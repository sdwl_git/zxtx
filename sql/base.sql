/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1@zxtx
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : 127.0.0.1:3306
 Source Schema         : mn01

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : 65001

 Date: 28/01/2023 02:05:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for crm_customer
-- ----------------------------
DROP TABLE IF EXISTS `crm_customer`;
CREATE TABLE `crm_customer`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户名称',
  `type` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户类别',
  `sales` bigint NULL DEFAULT NULL COMMENT '归属销售',
  `mobile` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '手机号',
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户来源',
  `dept_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '归属机构',
  `intention_product` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '意向产品',
  `remark` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `update_number` int NULL DEFAULT 0 COMMENT '更新次数',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `company_id`(`sales`, `mobile`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of crm_customer
-- ----------------------------
INSERT INTO `crm_customer` VALUES (6214, '方式公司11', '2', 1, '130888888888', NULL, NULL, NULL, NULL, 0, NULL, '2023-01-04 16:34:54');

-- ----------------------------
-- Table structure for crm_finance
-- ----------------------------
DROP TABLE IF EXISTS `crm_finance`;
CREATE TABLE `crm_finance`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `customer_id` bigint NULL DEFAULT NULL COMMENT '客户',
  `order_id` bigint NULL DEFAULT NULL COMMENT '订单',
  `get_account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收款账户',
  `sales` bigint NULL DEFAULT NULL COMMENT '销售',
  `money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '单价',
  `received_money` decimal(11, 2) NULL DEFAULT NULL COMMENT '已到金额',
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `company_id`(`customer_id`, `order_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of crm_finance
-- ----------------------------

-- ----------------------------
-- Table structure for crm_order
-- ----------------------------
DROP TABLE IF EXISTS `crm_order`;
CREATE TABLE `crm_order`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '订单ID',
  `customer_id` bigint NULL DEFAULT 0 COMMENT '客户ID',
  `quantity` decimal(11, 2) NULL DEFAULT NULL COMMENT '数量',
  `product_id` bigint NULL DEFAULT NULL COMMENT '产品',
  `money` decimal(11, 2) NULL DEFAULT NULL COMMENT '单价',
  `order_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '下单时间',
  `start_time` datetime NULL DEFAULT NULL COMMENT '订单开始时间',
  `end_time` datetime NULL DEFAULT NULL COMMENT '订单结束时间',
  `received_money` decimal(11, 2) NULL DEFAULT NULL COMMENT '已收金额',
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注',
  `state` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态',
  `sales` bigint NULL DEFAULT NULL COMMENT '销售',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新用户',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `company_id`(`customer_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of crm_order
-- ----------------------------

-- ----------------------------
-- Table structure for crm_order_supervisor
-- ----------------------------
DROP TABLE IF EXISTS `crm_order_supervisor`;
CREATE TABLE `crm_order_supervisor`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `customer_id` bigint NOT NULL COMMENT '客户ID',
  `intention_product_id` bigint NULL DEFAULT NULL COMMENT '意向产品',
  `state` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '状态',
  `next_time` datetime NULL DEFAULT NULL COMMENT '下次跟单时间',
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注',
  `sales` bigint NULL DEFAULT NULL COMMENT '销售',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `company_id`(`customer_id`, `intention_product_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of crm_order_supervisor
-- ----------------------------
INSERT INTO `crm_order_supervisor` VALUES (1, 6214, 12, '', NULL, '', NULL, NULL, '2023-01-04 16:34:11');

-- ----------------------------
-- Table structure for crm_product
-- ----------------------------
DROP TABLE IF EXISTS `crm_product`;
CREATE TABLE `crm_product`  (
  `id` bigint UNSIGNED NOT NULL COMMENT '产品id',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品类别',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品名称',
  `model` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '型号',
  `standard` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规格',
  `details` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '产品详情',
  `money` decimal(11, 2) NULL DEFAULT NULL COMMENT '金额',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of crm_product
-- ----------------------------

-- ----------------------------
-- Table structure for crm_resume
-- ----------------------------
DROP TABLE IF EXISTS `crm_resume`;
CREATE TABLE `crm_resume`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '简历ID',
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `birthday` datetime NULL DEFAULT NULL COMMENT '生日',
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别',
  `mobile` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `source` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '简历来源',
  `position_applied` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '应聘职位',
  `graduation_school` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '毕业院校',
  `graduation_time` datetime NULL DEFAULT NULL COMMENT '毕业时间',
  `major` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '专业',
  `education` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '学历',
  `work_experience` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '工作经历',
  `project_experience` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '项目经验',
  `resume_txt` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '文本简历',
  `remark` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `filename` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件路径',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `mobile`(`mobile`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of crm_resume
-- ----------------------------

-- ----------------------------
-- Table structure for fms_account
-- ----------------------------
DROP TABLE IF EXISTS `fms_account`;
CREATE TABLE `fms_account`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `serial_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编号',
  `current_amount` decimal(16, 2) NULL DEFAULT NULL COMMENT '当前余额',
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `enabled` bit(1) NULL DEFAULT NULL COMMENT '启用',
  `sort` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '排序',
  `is_default` bit(1) NULL DEFAULT NULL COMMENT '是否默认',
  `tenant_id` bigint NULL DEFAULT NULL COMMENT '租户id',
  `del_flag` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '账户信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fms_account
-- ----------------------------
INSERT INTO `fms_account` VALUES (17, '账户1', 'zzz111', 829.00, 'aabb', b'1', NULL, b'1', 63, '0', NULL, NULL);
INSERT INTO `fms_account` VALUES (18, '账户2', '1234131324', -1681.00, 'bbbb', b'1', NULL, b'0', 63, '0', NULL, NULL);

-- ----------------------------
-- Table structure for fms_account_head
-- ----------------------------
DROP TABLE IF EXISTS `fms_account_head`;
CREATE TABLE `fms_account_head`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型(支出/收入/收款/付款/转账)',
  `organ_id` bigint NULL DEFAULT NULL COMMENT '单位Id(收款/付款单位)',
  `hands_person_id` bigint NULL DEFAULT NULL COMMENT '经手人id',
  `user_id` bigint NULL DEFAULT NULL COMMENT '操作员',
  `change_amount` decimal(24, 6) NULL DEFAULT NULL COMMENT '变动金额(优惠/收款/付款/实付)',
  `discount_money` decimal(24, 6) NULL DEFAULT NULL COMMENT '优惠金额',
  `total_price` decimal(24, 6) NULL DEFAULT NULL COMMENT '合计金额',
  `account_id` bigint NULL DEFAULT NULL COMMENT '账户(收款/付款)',
  `bill_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据编号',
  `bill_time` datetime NULL DEFAULT NULL COMMENT '单据日期',
  `remark` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `file_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件名称',
  `status` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态，0未审核、1已审核',
  `tenant_id` bigint NULL DEFAULT NULL COMMENT '租户id',
  `del_flag` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK9F4C0D8DB610FC06`(`organ_id`) USING BTREE,
  INDEX `FK9F4C0D8DAAE50527`(`account_id`) USING BTREE,
  INDEX `FK9F4C0D8DC4170B37`(`hands_person_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 126 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '财务主表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fms_account_head
-- ----------------------------
INSERT INTO `fms_account_head` VALUES (118, '收入', 58, 16, 63, 55.000000, NULL, 55.000000, 17, 'SR00000000643', '2021-06-02 00:24:49', NULL, NULL, '1', 63, '0', NULL, '2023-01-28 01:33:19');
INSERT INTO `fms_account_head` VALUES (119, '支出', 68, 16, 63, -66.000000, NULL, -66.000000, 17, 'ZC00000000644', '2021-06-02 00:25:01', NULL, NULL, '0', 63, '0', NULL, '2023-01-28 01:33:19');
INSERT INTO `fms_account_head` VALUES (122, '转账', NULL, 17, 63, -11.000000, NULL, -11.000000, 17, 'ZZ00000000647', '2021-06-02 00:25:32', NULL, NULL, '0', 63, '0', NULL, '2023-01-28 01:33:19');
INSERT INTO `fms_account_head` VALUES (124, '收预付款', 60, 17, 63, 80.000000, 0.000000, 80.000000, NULL, 'SYF00000000649', '2021-07-06 23:43:48', NULL, NULL, '0', 63, '0', NULL, '2023-01-28 01:33:19');
INSERT INTO `fms_account_head` VALUES (125, '收款', 58, 17, 63, 10.000000, 0.000000, 10.000000, 17, 'SK00000000653', '2021-07-06 23:46:38', NULL, NULL, '0', 63, '0', NULL, '2023-01-28 01:33:19');
INSERT INTO `fms_account_head` VALUES (126, '付款', 57, 17, 63, -50.000000, 0.000000, -50.000000, 17, 'FK00000000654', '2021-07-06 23:47:23', NULL, NULL, '0', 63, '0', NULL, '2023-01-28 01:33:19');

-- ----------------------------
-- Table structure for fms_account_item
-- ----------------------------
DROP TABLE IF EXISTS `fms_account_item`;
CREATE TABLE `fms_account_item`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `header_id` bigint NOT NULL COMMENT '表头Id',
  `account_id` bigint NULL DEFAULT NULL COMMENT '账户Id',
  `in_out_item_id` bigint NULL DEFAULT NULL COMMENT '收支项目Id',
  `bill_id` bigint NULL DEFAULT NULL COMMENT '单据id',
  `need_debt` decimal(24, 6) NULL DEFAULT NULL COMMENT '应收欠款',
  `finish_debt` decimal(24, 6) NULL DEFAULT NULL COMMENT '已收欠款',
  `each_amount` decimal(24, 6) NULL DEFAULT NULL COMMENT '单项金额',
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据备注',
  `tenant_id` int NULL DEFAULT NULL COMMENT '租户id',
  `del_flag` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK9F4CBAC0AAE50527`(`account_id`) USING BTREE,
  INDEX `FK9F4CBAC0C5FE6007`(`header_id`) USING BTREE,
  INDEX `FK9F4CBAC0D203EDC5`(`in_out_item_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 151 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '财务子表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fms_account_item
-- ----------------------------
INSERT INTO `fms_account_item` VALUES (143, 118, NULL, 23, NULL, NULL, NULL, 55.000000, '', 63, '0', NULL, NULL);
INSERT INTO `fms_account_item` VALUES (144, 119, NULL, 21, NULL, NULL, NULL, 66.000000, '', 63, '0', NULL, NULL);
INSERT INTO `fms_account_item` VALUES (147, 122, 17, NULL, NULL, NULL, NULL, 11.000000, '', 63, '0', NULL, NULL);
INSERT INTO `fms_account_item` VALUES (149, 124, 17, NULL, NULL, NULL, NULL, 80.000000, '', 63, '0', NULL, NULL);
INSERT INTO `fms_account_item` VALUES (150, 125, NULL, NULL, 272, 20.000000, 0.000000, 10.000000, '', 63, '0', NULL, NULL);
INSERT INTO `fms_account_item` VALUES (151, 126, NULL, NULL, 271, 60.000000, 0.000000, -50.000000, '', 63, '0', NULL, NULL);

-- ----------------------------
-- Table structure for fms_item
-- ----------------------------
DROP TABLE IF EXISTS `fms_item`;
CREATE TABLE `fms_item`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `enabled` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '启用',
  `sort` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '排序',
  `tenant_id` int NULL DEFAULT NULL COMMENT '租户id',
  `del_flag` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '收支项目' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fms_item
-- ----------------------------
INSERT INTO `fms_item` VALUES (21, '快递费', '支出', '', '1', NULL, 63, '0', NULL, NULL);
INSERT INTO `fms_item` VALUES (22, '房租收入', '收入', '', '1', NULL, 63, '0', NULL, NULL);
INSERT INTO `fms_item` VALUES (23, '利息收入', '收入', '收入', '1', NULL, 63, '0', NULL, NULL);

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (39, 'wms_material', '产品表', NULL, NULL, 'Material', 'crud', 'com.mn.wms', 'wms', 'material', '产品', 'mn', '0', '/', '{\"parentMenuId\":2064}', 'admin', '2023-01-27 20:48:50', '', '2023-01-27 23:32:33', NULL);
INSERT INTO `gen_table` VALUES (40, 'wms_material_category', '产品类型表', '', '', 'MaterialCategory', 'tree', 'com.mn.wms', 'wms', 'category', '物料类型', 'mn', '0', '/', '{\"treeCode\":\"id\",\"treeName\":\"name\",\"treeParentCode\":\"parent_id\",\"parentMenuId\":\"2064\"}', 'admin', '2023-01-27 21:13:57', '', '2023-01-27 23:46:54', NULL);
INSERT INTO `gen_table` VALUES (41, 'wms_material_current_stock', '商品库存', NULL, NULL, 'MaterialStock', 'crud', 'com.mn.wms', 'wms', 'stock', '商品库存', 'mn', '0', '/', '{\"parentMenuId\":2064}', 'admin', '2023-01-27 21:13:57', '', '2023-01-27 23:42:20', NULL);
INSERT INTO `gen_table` VALUES (42, 'wms_material_extend', '产品价格扩展', NULL, NULL, 'MaterialExtend', 'crud', 'com.mn.wms', 'wms', 'materialExtend', '产品价格扩展', 'mn', '0', '/', '{\"parentMenuId\":2064}', 'admin', '2023-01-27 22:57:47', '', '2023-01-27 23:41:58', NULL);
INSERT INTO `gen_table` VALUES (43, 'wms_order_head', '单据主表', NULL, NULL, 'OrderHead', 'crud', 'com.mn.wms', 'wms', 'orderHead', '出入库单据', 'mn', '0', '/', '{\"parentMenuId\":\"2064\"}', 'admin', '2023-01-27 22:58:40', '', '2023-01-27 23:51:37', NULL);
INSERT INTO `gen_table` VALUES (44, 'wms_order_item', '单据子表', NULL, NULL, 'OrderItem', 'crud', 'com.mn.wms', 'wms', 'orderItem', '出入库单', 'mn', '0', '/', '{\"parentMenuId\":2064}', 'admin', '2023-01-27 22:59:00', '', '2023-01-27 23:51:14', NULL);
INSERT INTO `gen_table` VALUES (46, 'wms_order_type', '出入库类别表', NULL, NULL, 'OrderType', 'crud', 'com.mn.wms', 'wms', 'orderType', '出入库类别', 'mn', '0', '/', '{\"parentMenuId\":2064}', 'admin', '2023-01-27 23:26:17', '', '2023-01-27 23:53:54', NULL);
INSERT INTO `gen_table` VALUES (47, 'fms_account', '账户信息', NULL, NULL, 'Account', 'crud', 'com.mn.fms', 'fms', 'account', '账户信息', 'mn', '0', '/', '{\"parentMenuId\":\"2101\"}', 'admin', '2023-01-28 01:26:11', '', '2023-01-28 01:52:12', NULL);
INSERT INTO `gen_table` VALUES (48, 'fms_account_head', '财务主表', NULL, NULL, 'AccountHead', 'crud', 'com.mn.fms', 'fms', 'accountHead', '账单主表', 'mn', '0', '/', '{\"parentMenuId\":\"2101\"}', 'admin', '2023-01-28 01:26:11', '', '2023-01-28 01:52:27', NULL);
INSERT INTO `gen_table` VALUES (49, 'fms_account_item', '财务子表', NULL, NULL, 'AccountItem', 'crud', 'com.mn.fms', 'fms', 'accountItem', '财务子', 'mn', '0', '/', '{\"parentMenuId\":2101}', 'admin', '2023-01-28 01:26:11', '', '2023-01-28 01:53:29', NULL);
INSERT INTO `gen_table` VALUES (50, 'fms_item', '收支项目', NULL, NULL, 'FmsItem', 'crud', 'com.mn.fms', 'fms', 'item', '收支项目', 'mn', '0', '/', '{\"parentMenuId\":2101}', 'admin', '2023-01-28 01:26:11', '', '2023-01-28 01:53:43', NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `html_width` int NULL DEFAULT NULL COMMENT '在页面中的宽度',
  `visible` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否可见',
  `sort` int NULL DEFAULT NULL COMMENT '排序',
  `dept_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '归属机构',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 459 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (476, '39', 'id', '主键', 'bigint', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, NULL, NULL, 1, NULL, 'admin', '2023-01-27 20:48:50', NULL, '2023-01-27 23:32:33');
INSERT INTO `gen_table_column` VALUES (477, '39', 'category_id', '产品类型id', 'bigint', 'Long', 'categoryId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', NULL, NULL, 2, NULL, 'admin', '2023-01-27 20:48:50', NULL, '2023-01-27 23:32:33');
INSERT INTO `gen_table_column` VALUES (478, '39', 'name', '名称', 'varchar(100)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', NULL, NULL, 3, NULL, 'admin', '2023-01-27 20:48:50', NULL, '2023-01-27 23:32:33');
INSERT INTO `gen_table_column` VALUES (479, '39', 'mfrs', '制造商', 'varchar(50)', 'String', 'mfrs', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 4, NULL, 'admin', '2023-01-27 20:48:50', NULL, '2023-01-27 23:32:33');
INSERT INTO `gen_table_column` VALUES (480, '39', 'model', '型号', 'varchar(50)', 'String', 'model', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 5, NULL, 'admin', '2023-01-27 20:48:50', NULL, '2023-01-27 23:32:33');
INSERT INTO `gen_table_column` VALUES (481, '39', 'standard', '规格', 'varchar(50)', 'String', 'standard', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 6, NULL, 'admin', '2023-01-27 20:48:50', NULL, '2023-01-27 23:32:33');
INSERT INTO `gen_table_column` VALUES (482, '39', 'unit', '单位', 'varchar(50)', 'String', 'unit', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 7, NULL, 'admin', '2023-01-27 20:48:50', NULL, '2023-01-27 23:32:33');
INSERT INTO `gen_table_column` VALUES (483, '39', 'img_name', '图片名称', 'varchar(500)', 'String', 'imgName', '0', '0', NULL, '1', '1', '1', NULL, 'LIKE', 'textarea', '', NULL, NULL, 8, NULL, 'admin', '2023-01-27 20:48:50', NULL, '2023-01-27 23:32:33');
INSERT INTO `gen_table_column` VALUES (484, '39', 'unit_id', '计量单位Id', 'bigint', 'Long', 'unitId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 9, NULL, 'admin', '2023-01-27 20:48:50', NULL, '2023-01-27 23:32:33');
INSERT INTO `gen_table_column` VALUES (485, '39', 'expiry_num', '保质期天数', 'int', 'Long', 'expiryNum', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 10, NULL, 'admin', '2023-01-27 20:48:50', NULL, '2023-01-27 23:32:33');
INSERT INTO `gen_table_column` VALUES (486, '39', 'weight', '基础重量(kg)', 'decimal(24,6)', 'BigDecimal', 'weight', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 11, NULL, 'admin', '2023-01-27 20:48:50', NULL, '2023-01-27 23:32:33');
INSERT INTO `gen_table_column` VALUES (487, '39', 'enabled', '是否启用', 'char(1)', 'String', 'enabled', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', 'sys_normal_disable', NULL, NULL, 12, NULL, 'admin', '2023-01-27 20:48:50', NULL, '2023-01-27 23:32:33');
INSERT INTO `gen_table_column` VALUES (488, '39', 'enable_serial', '是否序列号', 'char(1)', 'String', 'enableSerial', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', 'sys_normal_disable', NULL, NULL, 13, NULL, 'admin', '2023-01-27 20:48:50', NULL, '2023-01-27 23:32:33');
INSERT INTO `gen_table_column` VALUES (489, '39', 'enable_batch', '是否批次号', 'char(1)', 'String', 'enableBatch', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', 'sys_normal_disable', NULL, NULL, 14, NULL, 'admin', '2023-01-27 20:48:50', NULL, '2023-01-27 23:32:33');
INSERT INTO `gen_table_column` VALUES (490, '39', 'tenant_id', '租户id', 'bigint', 'Long', 'tenantId', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', NULL, NULL, 15, NULL, 'admin', '2023-01-27 20:48:50', NULL, '2023-01-27 23:32:33');
INSERT INTO `gen_table_column` VALUES (491, '39', 'del_flag', '删除标记，0未删除，1删除', 'varchar(1)', 'String', 'delFlag', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, NULL, NULL, 16, NULL, 'admin', '2023-01-27 20:48:50', NULL, '2023-01-27 23:32:33');
INSERT INTO `gen_table_column` VALUES (492, '39', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', NULL, NULL, 17, NULL, 'admin', '2023-01-27 20:48:50', NULL, '2023-01-27 23:32:33');
INSERT INTO `gen_table_column` VALUES (493, '39', 'update_by', '更新人', 'varchar(32)', 'String', 'updateBy', '0', '0', NULL, '1', NULL, '1', NULL, 'EQ', 'input', NULL, NULL, NULL, 18, NULL, 'admin', '2023-01-27 20:48:50', NULL, '2023-01-27 23:32:33');
INSERT INTO `gen_table_column` VALUES (494, '39', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, NULL, NULL, '1', '1', 'BETWEEN', 'datetime', NULL, NULL, NULL, 19, NULL, 'admin', '2023-01-27 20:48:50', NULL, '2023-01-27 23:32:33');
INSERT INTO `gen_table_column` VALUES (495, '40', 'id', '主键', 'bigint', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, NULL, NULL, 1, NULL, 'admin', '2023-01-27 21:13:57', NULL, '2023-01-27 23:46:54');
INSERT INTO `gen_table_column` VALUES (496, '40', 'name', '名称', 'varchar(50)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', NULL, NULL, 2, NULL, 'admin', '2023-01-27 21:13:57', NULL, '2023-01-27 23:46:54');
INSERT INTO `gen_table_column` VALUES (498, '40', 'parent_id', '父类', 'bigint', 'Long', 'parentId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 4, NULL, 'admin', '2023-01-27 21:13:57', NULL, '2023-01-27 23:46:54');
INSERT INTO `gen_table_column` VALUES (499, '40', 'sort', '显示顺序', 'varchar(10)', 'String', 'sort', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 5, NULL, 'admin', '2023-01-27 21:13:57', NULL, '2023-01-27 23:46:54');
INSERT INTO `gen_table_column` VALUES (500, '40', 'serial_no', '编号', 'varchar(100)', 'String', 'serialNo', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 6, NULL, 'admin', '2023-01-27 21:13:57', NULL, '2023-01-27 23:46:54');
INSERT INTO `gen_table_column` VALUES (501, '40', 'remark', '备注', 'varchar(1024)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', NULL, NULL, 7, NULL, 'admin', '2023-01-27 21:13:57', NULL, '2023-01-27 23:46:54');
INSERT INTO `gen_table_column` VALUES (503, '40', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, NULL, NULL, '1', '1', 'BETWEEN', 'datetime', NULL, NULL, NULL, 9, NULL, 'admin', '2023-01-27 21:13:57', NULL, '2023-01-27 23:46:54');
INSERT INTO `gen_table_column` VALUES (504, '40', 'tenant_id', '租户id', 'bigint', 'Long', 'tenantId', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', NULL, NULL, 10, NULL, 'admin', '2023-01-27 21:13:57', NULL, '2023-01-27 23:46:54');
INSERT INTO `gen_table_column` VALUES (506, '41', 'id', '主键', 'bigint', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, NULL, NULL, 1, NULL, 'admin', '2023-01-27 21:13:57', NULL, '2023-01-27 23:42:20');
INSERT INTO `gen_table_column` VALUES (507, '41', 'material_id', '产品id', 'bigint', 'Long', 'materialId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', NULL, NULL, 3, NULL, 'admin', '2023-01-27 21:13:57', NULL, '2023-01-27 23:42:20');
INSERT INTO `gen_table_column` VALUES (509, '41', 'current_number', '当前库存数量', 'decimal(24,6)', 'BigDecimal', 'currentNumber', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 6, NULL, 'admin', '2023-01-27 21:13:57', NULL, '2023-01-27 23:42:20');
INSERT INTO `gen_table_column` VALUES (510, '41', 'tenant_id', '租户id', 'bigint', 'Long', 'tenantId', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', NULL, NULL, 7, NULL, 'admin', '2023-01-27 21:13:57', NULL, '2023-01-27 23:42:20');
INSERT INTO `gen_table_column` VALUES (512, '42', 'id', '主键', 'bigint', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, NULL, NULL, 1, NULL, 'admin', '2023-01-27 22:57:47', NULL, '2023-01-27 23:41:58');
INSERT INTO `gen_table_column` VALUES (513, '42', 'material_id', '商品id', 'bigint', 'Long', 'materialId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', NULL, NULL, 2, NULL, 'admin', '2023-01-27 22:57:47', NULL, '2023-01-27 23:41:58');
INSERT INTO `gen_table_column` VALUES (514, '42', 'bar_code', '商品条码', 'varchar(50)', 'String', 'barCode', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 3, NULL, 'admin', '2023-01-27 22:57:47', NULL, '2023-01-27 23:41:58');
INSERT INTO `gen_table_column` VALUES (515, '42', 'commodity_unit', '商品单位', 'varchar(50)', 'String', 'commodityUnit', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 4, NULL, 'admin', '2023-01-27 22:57:47', NULL, '2023-01-27 23:41:58');
INSERT INTO `gen_table_column` VALUES (516, '42', 'sku', '多属性', 'varchar(50)', 'String', 'sku', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 5, NULL, 'admin', '2023-01-27 22:57:47', NULL, '2023-01-27 23:41:58');
INSERT INTO `gen_table_column` VALUES (517, '42', 'purchase_decimal', '采购价格', 'decimal(16,2)', 'BigDecimal', 'purchaseDecimal', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 6, NULL, 'admin', '2023-01-27 22:57:47', NULL, '2023-01-27 23:41:58');
INSERT INTO `gen_table_column` VALUES (518, '42', 'commodity_decimal', '零售价格', 'decimal(16,2)', 'BigDecimal', 'commodityDecimal', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 7, NULL, 'admin', '2023-01-27 22:57:47', NULL, '2023-01-27 23:41:58');
INSERT INTO `gen_table_column` VALUES (519, '42', 'wholesale_decimal', '销售价格', 'decimal(16,2)', 'BigDecimal', 'wholesaleDecimal', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 8, NULL, 'admin', '2023-01-27 22:57:47', NULL, '2023-01-27 23:41:58');
INSERT INTO `gen_table_column` VALUES (520, '42', 'low_decimal', '最低售价', 'decimal(16,2)', 'BigDecimal', 'lowDecimal', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 9, NULL, 'admin', '2023-01-27 22:57:47', NULL, '2023-01-27 23:41:58');
INSERT INTO `gen_table_column` VALUES (521, '42', 'default_flag', '是否为默认单位，1是，0否', 'char(1)', 'String', 'defaultFlag', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'select', 'sys_yes_no', NULL, NULL, 10, NULL, 'admin', '2023-01-27 22:57:47', NULL, '2023-01-27 23:41:58');
INSERT INTO `gen_table_column` VALUES (522, '42', 'create_by', '创建人编码', 'varchar(50)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, NULL, NULL, 11, NULL, 'admin', '2023-01-27 22:57:47', NULL, '2023-01-27 23:41:58');
INSERT INTO `gen_table_column` VALUES (523, '42', 'create_time', '创建日期', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', NULL, NULL, NULL, 12, NULL, 'admin', '2023-01-27 22:57:47', NULL, '2023-01-27 23:41:58');
INSERT INTO `gen_table_column` VALUES (524, '42', 'update_by', '更新人编码', 'varchar(50)', 'String', 'updateBy', '0', '0', NULL, '1', NULL, '1', NULL, 'EQ', 'input', NULL, NULL, NULL, 13, NULL, 'admin', '2023-01-27 22:57:47', NULL, '2023-01-27 23:41:58');
INSERT INTO `gen_table_column` VALUES (525, '42', 'update_time', '更新时间戳', 'datetime', 'Date', 'updateTime', '0', '0', NULL, NULL, NULL, '1', NULL, 'EQ', 'datetime', NULL, NULL, NULL, 14, NULL, 'admin', '2023-01-27 22:57:47', NULL, '2023-01-27 23:41:58');
INSERT INTO `gen_table_column` VALUES (526, '42', 'tenant_id', '租户id', 'bigint', 'Long', 'tenantId', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', NULL, NULL, 15, NULL, 'admin', '2023-01-27 22:57:47', NULL, '2023-01-27 23:41:58');
INSERT INTO `gen_table_column` VALUES (528, '43', 'id', '主键', 'bigint', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, NULL, NULL, 1, NULL, 'admin', '2023-01-27 22:58:40', NULL, '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (529, '43', 'type', '出入库类别', 'varchar(50)', 'String', 'type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', NULL, NULL, 2, NULL, 'admin', '2023-01-27 22:58:40', NULL, '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (533, '43', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, '1', NULL, 'EQ', 'datetime', NULL, NULL, NULL, 7, NULL, 'admin', '2023-01-27 22:58:40', NULL, '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (534, '43', 'oper_time', '出入时间', 'datetime', 'Date', 'operTime', '0', '0', NULL, '1', NULL, '1', '1', 'BETWEEN', 'datetime', '', NULL, NULL, 8, NULL, 'admin', '2023-01-27 22:58:40', NULL, '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (535, '43', 'organ_id', '供应商id or 者客户id', 'bigint', 'Long', 'organId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 9, NULL, 'admin', '2023-01-27 22:58:40', NULL, '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (536, '43', 'user_id', '操作者', 'bigint', 'Long', 'userId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 10, NULL, 'admin', '2023-01-27 22:58:40', NULL, '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (537, '43', 'approver_id', '审批人', 'bigint', 'Long', 'approverId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 11, NULL, 'admin', '2023-01-27 22:58:40', NULL, '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (538, '43', 'operate_id', '出入库操作人', 'bigint', 'Long', 'operateId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 12, NULL, 'admin', '2023-01-27 22:58:40', NULL, '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (539, '43', 'fms_head_id', '账户id', 'bigint', 'Long', 'fmsHeadId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 13, NULL, 'admin', '2023-01-27 22:58:40', NULL, '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (540, '43', 'pay_type', '付款类型(现金、记账等)', 'varchar(50)', 'String', 'payType', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'select', '', NULL, NULL, 14, NULL, 'admin', '2023-01-27 22:58:40', NULL, '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (541, '43', 'bill_type', '单据类型', 'varchar(50)', 'String', 'billType', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'select', '', NULL, NULL, 15, NULL, 'admin', '2023-01-27 22:58:40', NULL, '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (542, '43', 'remark', '备注', 'varchar(1000)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', NULL, NULL, 16, NULL, 'admin', '2023-01-27 22:58:40', NULL, '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (543, '43', 'file_name', '附件名称', 'varchar(500)', 'String', 'fileName', '0', '0', NULL, '1', '1', '1', NULL, 'LIKE', 'textarea', '', NULL, NULL, 17, NULL, 'admin', '2023-01-27 22:58:40', NULL, '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (544, '43', 'sales_man', '业务员', 'varchar(50)', 'String', 'salesMan', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 18, NULL, 'admin', '2023-01-27 22:58:40', NULL, '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (545, '43', 'account_id_list', '多账户ID列表', 'varchar(50)', 'String', 'accountIdList', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 19, NULL, 'admin', '2023-01-27 22:58:40', NULL, '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (546, '43', 'account_money_list', '多账户金额列表', 'varchar(200)', 'String', 'accountMoneyList', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 20, NULL, 'admin', '2023-01-27 22:58:40', NULL, '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (547, '43', 'discount', '优惠率', 'decimal(24,6)', 'BigDecimal', 'discount', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 21, NULL, 'admin', '2023-01-27 22:58:40', NULL, '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (548, '43', 'discount_money', '优惠金额', 'decimal(24,6)', 'BigDecimal', 'discountMoney', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 22, NULL, 'admin', '2023-01-27 22:58:40', NULL, '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (549, '43', 'discount_last_money', '优惠后金额', 'decimal(24,6)', 'BigDecimal', 'discountLastMoney', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 23, NULL, 'admin', '2023-01-27 22:58:40', NULL, '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (550, '43', 'other_money', '销售或采购费用合计', 'decimal(24,6)', 'BigDecimal', 'otherMoney', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 24, NULL, 'admin', '2023-01-27 22:58:40', NULL, '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (551, '43', 'deposit', '订金', 'decimal(24,6)', 'BigDecimal', 'deposit', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 25, NULL, 'admin', '2023-01-27 22:58:40', NULL, '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (552, '43', 'status', '状态', 'varchar(1)', 'String', 'status', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'radio', '', NULL, NULL, 26, NULL, 'admin', '2023-01-27 22:58:40', NULL, '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (553, '43', 'purchase_status', '采购状态，0未采购、2完成采购、3部分采购', 'varchar(1)', 'String', 'purchaseStatus', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'radio', '', NULL, NULL, 27, NULL, 'admin', '2023-01-27 22:58:40', NULL, '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (554, '43', 'link_number', '关联订单号', 'varchar(50)', 'String', 'linkNumber', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 28, NULL, 'admin', '2023-01-27 22:58:40', NULL, '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (556, '43', 'del_flag', '删除标记，0未删除，1删除', 'varchar(1)', 'String', 'delFlag', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, NULL, NULL, 30, NULL, 'admin', '2023-01-27 22:58:40', NULL, '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (557, '44', 'id', '主键', 'bigint', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, NULL, NULL, 1, NULL, 'admin', '2023-01-27 22:59:00', NULL, '2023-01-27 23:51:14');
INSERT INTO `gen_table_column` VALUES (558, '44', 'header_id', '表头Id', 'bigint', 'Long', 'headerId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', NULL, NULL, 2, NULL, 'admin', '2023-01-27 22:59:00', NULL, '2023-01-27 23:51:14');
INSERT INTO `gen_table_column` VALUES (559, '44', 'material_id', '商品Id', 'bigint', 'Long', 'materialId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', NULL, NULL, 3, NULL, 'admin', '2023-01-27 22:59:00', NULL, '2023-01-27 23:51:14');
INSERT INTO `gen_table_column` VALUES (560, '44', 'material_extend_id', '商品扩展id', 'bigint', 'Long', 'materialExtendId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 4, NULL, 'admin', '2023-01-27 22:59:00', NULL, '2023-01-27 23:51:14');
INSERT INTO `gen_table_column` VALUES (561, '44', 'material_unit', '商品计量单位', 'varchar(20)', 'String', 'materialUnit', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 5, NULL, 'admin', '2023-01-27 22:59:00', NULL, '2023-01-27 23:51:14');
INSERT INTO `gen_table_column` VALUES (562, '44', 'sku', '多属性', 'varchar(50)', 'String', 'sku', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 6, NULL, 'admin', '2023-01-27 22:59:00', NULL, '2023-01-27 23:51:14');
INSERT INTO `gen_table_column` VALUES (563, '44', 'oper_number', '数量', 'decimal(24,6)', 'BigDecimal', 'operNumber', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 7, NULL, 'admin', '2023-01-27 22:59:00', NULL, '2023-01-27 23:51:14');
INSERT INTO `gen_table_column` VALUES (564, '44', 'basic_number', '基础数量，如kg、瓶', 'decimal(24,6)', 'BigDecimal', 'basicNumber', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 8, NULL, 'admin', '2023-01-27 22:59:00', NULL, '2023-01-27 23:51:14');
INSERT INTO `gen_table_column` VALUES (565, '44', 'unit_price', '单价', 'decimal(24,6)', 'BigDecimal', 'unitPrice', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 9, NULL, 'admin', '2023-01-27 22:59:00', NULL, '2023-01-27 23:51:14');
INSERT INTO `gen_table_column` VALUES (566, '44', 'purchase_unit_price', '采购单价', 'decimal(24,6)', 'BigDecimal', 'purchaseUnitPrice', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 10, NULL, 'admin', '2023-01-27 22:59:00', NULL, '2023-01-27 23:51:14');
INSERT INTO `gen_table_column` VALUES (567, '44', 'tax_unit_price', '含税单价', 'decimal(24,6)', 'BigDecimal', 'taxUnitPrice', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 11, NULL, 'admin', '2023-01-27 22:59:00', NULL, '2023-01-27 23:51:14');
INSERT INTO `gen_table_column` VALUES (568, '44', 'all_price', '金额', 'decimal(24,6)', 'BigDecimal', 'allPrice', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 12, NULL, 'admin', '2023-01-27 22:59:00', NULL, '2023-01-27 23:51:14');
INSERT INTO `gen_table_column` VALUES (569, '44', 'remark', '备注', 'varchar(200)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', NULL, NULL, 13, NULL, 'admin', '2023-01-27 22:59:00', NULL, '2023-01-27 23:51:14');
INSERT INTO `gen_table_column` VALUES (572, '44', 'tax_rate', '税率', 'decimal(24,6)', 'BigDecimal', 'taxRate', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 16, NULL, 'admin', '2023-01-27 22:59:00', NULL, '2023-01-27 23:51:14');
INSERT INTO `gen_table_column` VALUES (573, '44', 'tax_money', '税额', 'decimal(24,6)', 'BigDecimal', 'taxMoney', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 17, NULL, 'admin', '2023-01-27 22:59:00', NULL, '2023-01-27 23:51:14');
INSERT INTO `gen_table_column` VALUES (574, '44', 'tax_last_money', '价税合计', 'decimal(24,6)', 'BigDecimal', 'taxLastMoney', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 18, NULL, 'admin', '2023-01-27 22:59:00', NULL, '2023-01-27 23:51:14');
INSERT INTO `gen_table_column` VALUES (575, '44', 'material_type', '商品类型', 'varchar(20)', 'String', 'materialType', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'select', '', NULL, NULL, 19, NULL, 'admin', '2023-01-27 22:59:00', NULL, '2023-01-27 23:51:14');
INSERT INTO `gen_table_column` VALUES (576, '44', 'sn_list', '序列号列表', 'varchar(2000)', 'String', 'snList', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', NULL, NULL, 20, NULL, 'admin', '2023-01-27 22:59:00', NULL, '2023-01-27 23:51:14');
INSERT INTO `gen_table_column` VALUES (578, '44', 'expiration_date', '有效日期', 'datetime', 'Date', 'expirationDate', '0', '0', NULL, '1', '1', '1', '1', 'BETWEEN', 'datetime', '', NULL, NULL, 22, NULL, 'admin', '2023-01-27 22:59:00', NULL, '2023-01-27 23:51:14');
INSERT INTO `gen_table_column` VALUES (579, '44', 'link_id', '关联明细id', 'bigint', 'Long', 'linkId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 23, NULL, 'admin', '2023-01-27 22:59:00', NULL, '2023-01-27 23:51:14');
INSERT INTO `gen_table_column` VALUES (581, '44', 'del_flag', '删除标记，0未删除，1删除', 'varchar(1)', 'String', 'delFlag', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, NULL, NULL, 25, NULL, 'admin', '2023-01-27 22:59:00', NULL, '2023-01-27 23:51:14');
INSERT INTO `gen_table_column` VALUES (582, '40', 'ancestors', '祖级列表', 'varbinary(128)', 'String', 'ancestors', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', NULL, '', NULL, NULL, 3, NULL, '', '2023-01-27 23:17:00', '', '2023-01-27 23:46:54');
INSERT INTO `gen_table_column` VALUES (583, '40', 'update_by', '更新人', 'varchar(32)', 'String', 'updateBy', '0', '0', NULL, '1', NULL, '1', NULL, 'EQ', 'input', '', NULL, NULL, 8, NULL, '', '2023-01-27 23:17:00', '', '2023-01-27 23:46:54');
INSERT INTO `gen_table_column` VALUES (584, '40', 'del_flag', '删除标记，0未删除，1删除', 'varchar(1)', 'String', 'delFlag', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', NULL, NULL, 11, NULL, '', '2023-01-27 23:17:00', '', '2023-01-27 23:46:54');
INSERT INTO `gen_table_column` VALUES (585, '41', 'warehouse_id', '仓库id', 'bigint', 'Long', 'warehouseId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', NULL, NULL, 2, NULL, '', '2023-01-27 23:17:03', '', '2023-01-27 23:42:20');
INSERT INTO `gen_table_column` VALUES (586, '41', 'material_extend_id', '产品subid', 'bigint', 'Long', 'materialExtendId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', NULL, NULL, 4, NULL, '', '2023-01-27 23:17:03', '', '2023-01-27 23:42:20');
INSERT INTO `gen_table_column` VALUES (587, '41', 'batch', '批次号', 'varchar(64)', 'String', 'batch', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 5, NULL, '', '2023-01-27 23:17:03', '', '2023-01-27 23:42:20');
INSERT INTO `gen_table_column` VALUES (588, '41', 'update_by', '更新人', 'varchar(32)', 'String', 'updateBy', '0', '0', NULL, '1', NULL, '1', NULL, 'EQ', 'input', '', NULL, NULL, 8, NULL, '', '2023-01-27 23:17:03', '', '2023-01-27 23:42:20');
INSERT INTO `gen_table_column` VALUES (589, '41', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, NULL, NULL, '1', NULL, 'EQ', 'datetime', '', NULL, NULL, 9, NULL, '', '2023-01-27 23:17:03', '', '2023-01-27 23:42:20');
INSERT INTO `gen_table_column` VALUES (590, '41', 'del_flag', '删除标记，0未删除，1删除', 'varchar(1)', 'String', 'delFlag', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', NULL, NULL, 10, NULL, '', '2023-01-27 23:17:03', '', '2023-01-27 23:42:20');
INSERT INTO `gen_table_column` VALUES (591, '42', 'del_Flag', '删除标记，0未删除，1删除', 'varchar(1)', 'String', 'delFlag', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', NULL, NULL, 16, NULL, '', '2023-01-27 23:17:05', '', '2023-01-27 23:41:58');
INSERT INTO `gen_table_column` VALUES (592, '43', 'source_type', '来源类别', 'varchar(32)', 'String', 'sourceType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', NULL, NULL, 3, NULL, '', '2023-01-27 23:17:07', '', '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (593, '43', 'source_num', '来源单据号', 'varchar(64)', 'String', 'sourceNum', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 4, NULL, '', '2023-01-27 23:17:07', '', '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (594, '43', 'default_num', '初始票据号', 'varchar(50)', 'String', 'defaultNum', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 5, NULL, '', '2023-01-27 23:17:07', '', '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (595, '43', 'bill_num', '票据号', 'varchar(50)', 'String', 'billNum', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 6, NULL, '', '2023-01-27 23:17:07', '', '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (596, '43', 'tenant_id', '租户id', 'bigint', 'Long', 'tenantId', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', NULL, NULL, 29, NULL, '', '2023-01-27 23:17:07', '', '2023-01-27 23:51:37');
INSERT INTO `gen_table_column` VALUES (597, '44', 'warehouse_id', '仓库ID', 'bigint', 'Long', 'warehouseId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 14, NULL, '', '2023-01-27 23:17:11', NULL, '2023-01-27 23:51:14');
INSERT INTO `gen_table_column` VALUES (598, '44', 'batch', '批号', 'varchar(100)', 'String', 'batch', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 21, NULL, '', '2023-01-27 23:17:11', NULL, '2023-01-27 23:51:14');
INSERT INTO `gen_table_column` VALUES (599, '44', 'tenant_id', '租户id', 'varchar(64)', 'String', 'tenantId', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', NULL, NULL, 24, NULL, '', '2023-01-27 23:17:11', NULL, '2023-01-27 23:51:14');
INSERT INTO `gen_table_column` VALUES (608, '46', 'id', 'id', 'bigint', 'Long', 'id', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', NULL, NULL, 1, NULL, 'admin', '2023-01-27 23:26:17', '', '2023-01-27 23:53:54');
INSERT INTO `gen_table_column` VALUES (609, '46', 'inout', '出入库', 'char(1)', 'String', 'inout', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', NULL, NULL, 2, NULL, 'admin', '2023-01-27 23:26:17', '', '2023-01-27 23:53:54');
INSERT INTO `gen_table_column` VALUES (610, '46', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', NULL, NULL, 3, NULL, 'admin', '2023-01-27 23:26:17', '', '2023-01-27 23:53:54');
INSERT INTO `gen_table_column` VALUES (611, '46', 'enable', '启用', 'char(1)', 'String', 'enable', '0', '0', '1', '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 4, NULL, 'admin', '2023-01-27 23:26:17', '', '2023-01-27 23:53:54');
INSERT INTO `gen_table_column` VALUES (612, '46', 'tenant_id', '租户', 'bigint', 'Long', 'tenantId', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', NULL, NULL, 5, NULL, 'admin', '2023-01-27 23:26:17', '', '2023-01-27 23:53:54');
INSERT INTO `gen_table_column` VALUES (613, '46', 'update_by', '更新人', 'varchar(32)', 'String', 'updateBy', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 6, NULL, 'admin', '2023-01-27 23:26:17', '', '2023-01-27 23:53:54');
INSERT INTO `gen_table_column` VALUES (614, '46', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, NULL, NULL, '1', '1', 'BETWEEN', 'datetime', '', NULL, NULL, 7, NULL, 'admin', '2023-01-27 23:26:17', '', '2023-01-27 23:53:54');
INSERT INTO `gen_table_column` VALUES (615, '46', 'remark', '备注', 'text', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', NULL, NULL, 8, NULL, 'admin', '2023-01-27 23:26:17', '', '2023-01-27 23:53:54');
INSERT INTO `gen_table_column` VALUES (616, '44', 'another_warehouse_id', '调拨仓库', 'bigint', 'Long', 'anotherWarehouseId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 15, NULL, '', '2023-01-27 23:48:49', '', '2023-01-27 23:51:14');
INSERT INTO `gen_table_column` VALUES (617, '47', 'id', '主键', 'bigint', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, NULL, NULL, 1, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:52:12');
INSERT INTO `gen_table_column` VALUES (618, '47', 'name', '名称', 'varchar(50)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', NULL, NULL, 2, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:52:12');
INSERT INTO `gen_table_column` VALUES (619, '47', 'serial_no', '编号', 'varchar(50)', 'String', 'serialNo', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 3, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:52:12');
INSERT INTO `gen_table_column` VALUES (621, '47', 'current_amount', '当前余额', 'decimal(24,6)', 'BigDecimal', 'currentAmount', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 4, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:52:12');
INSERT INTO `gen_table_column` VALUES (622, '47', 'remark', '备注', 'varchar(100)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 5, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:52:12');
INSERT INTO `gen_table_column` VALUES (623, '47', 'enabled', '启用', 'bit(1)', 'Integer', 'enabled', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'select', 'sys_normal_disable', NULL, NULL, 6, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:52:12');
INSERT INTO `gen_table_column` VALUES (624, '47', 'sort', '排序', 'varchar(10)', 'String', 'sort', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 7, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:52:12');
INSERT INTO `gen_table_column` VALUES (625, '47', 'is_default', '是否默认', 'bit(1)', 'Integer', 'isDefault', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'select', 'sys_yes_no', NULL, NULL, 8, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:52:12');
INSERT INTO `gen_table_column` VALUES (626, '47', 'tenant_id', '租户id', 'bigint', 'Long', 'tenantId', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', NULL, NULL, 9, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:52:12');
INSERT INTO `gen_table_column` VALUES (627, '47', 'del_flag', '删除标记，0未删除，1删除', 'varchar(1)', 'String', 'delFlag', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, NULL, NULL, 10, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:52:12');
INSERT INTO `gen_table_column` VALUES (628, '48', 'id', '主键', 'bigint', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, NULL, NULL, 1, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:52:27');
INSERT INTO `gen_table_column` VALUES (629, '48', 'type', '类型', 'varchar(50)', 'String', 'type', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'select', '', NULL, NULL, 2, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:52:27');
INSERT INTO `gen_table_column` VALUES (630, '48', 'organ_id', '单位', 'bigint', 'Long', 'organId', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', NULL, NULL, 3, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:52:27');
INSERT INTO `gen_table_column` VALUES (631, '48', 'hands_person_id', '经手人', 'bigint', 'Long', 'handsPersonId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 4, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:52:27');
INSERT INTO `gen_table_column` VALUES (632, '48', 'user_id', '操作员', 'bigint', 'Long', 'userId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 5, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:52:27');
INSERT INTO `gen_table_column` VALUES (633, '48', 'change_amount', '变动金额(优惠/收款/付款/实付)', 'decimal(24,6)', 'BigDecimal', 'changeAmount', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 6, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:52:27');
INSERT INTO `gen_table_column` VALUES (634, '48', 'discount_money', '优惠金额', 'decimal(24,6)', 'BigDecimal', 'discountMoney', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 7, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:52:27');
INSERT INTO `gen_table_column` VALUES (635, '48', 'total_price', '合计金额', 'decimal(24,6)', 'BigDecimal', 'totalPrice', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 8, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:52:27');
INSERT INTO `gen_table_column` VALUES (636, '48', 'account_id', '账户', 'bigint', 'Long', 'accountId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', NULL, NULL, 9, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:52:27');
INSERT INTO `gen_table_column` VALUES (637, '48', 'bill_no', '单据编号', 'varchar(50)', 'String', 'billNo', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 10, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:52:27');
INSERT INTO `gen_table_column` VALUES (638, '48', 'bill_time', '单据日期', 'datetime', 'Date', 'billTime', '0', '0', NULL, '1', '1', '1', '1', 'BETWEEN', 'datetime', '', NULL, NULL, 11, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:52:27');
INSERT INTO `gen_table_column` VALUES (639, '48', 'remark', '备注', 'varchar(1000)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', NULL, NULL, 12, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:52:27');
INSERT INTO `gen_table_column` VALUES (640, '48', 'file_name', '附件名称', 'varchar(500)', 'String', 'fileName', '0', '0', NULL, '1', '1', '1', NULL, 'LIKE', 'textarea', '', NULL, NULL, 13, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:52:27');
INSERT INTO `gen_table_column` VALUES (641, '48', 'status', '状态', 'varchar(1)', 'String', 'status', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'select', 'sys_job_status', NULL, NULL, 14, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:52:27');
INSERT INTO `gen_table_column` VALUES (642, '48', 'tenant_id', '租户id', 'bigint', 'Long', 'tenantId', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', NULL, NULL, 15, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:52:27');
INSERT INTO `gen_table_column` VALUES (643, '48', 'del_flag', '删除标记，0未删除，1删除', 'varchar(1)', 'String', 'delFlag', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, NULL, NULL, 16, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:52:27');
INSERT INTO `gen_table_column` VALUES (644, '49', 'id', '主键', 'bigint', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, NULL, NULL, 1, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:53:29');
INSERT INTO `gen_table_column` VALUES (645, '49', 'header_id', '表头Id', 'bigint', 'Long', 'headerId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', NULL, NULL, 2, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:53:29');
INSERT INTO `gen_table_column` VALUES (646, '49', 'account_id', '账户Id', 'bigint', 'Long', 'accountId', '0', '0', '1', '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 3, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:53:29');
INSERT INTO `gen_table_column` VALUES (647, '49', 'in_out_item_id', '收支项目Id', 'bigint', 'Long', 'inOutItemId', '0', '0', '1', '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 4, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:53:29');
INSERT INTO `gen_table_column` VALUES (648, '49', 'bill_id', '单据id', 'bigint', 'Long', 'billId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 5, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:53:29');
INSERT INTO `gen_table_column` VALUES (649, '49', 'need_debt', '应收欠款', 'decimal(24,6)', 'BigDecimal', 'needDebt', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 6, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:53:29');
INSERT INTO `gen_table_column` VALUES (650, '49', 'finish_debt', '已收欠款', 'decimal(24,6)', 'BigDecimal', 'finishDebt', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 7, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:53:29');
INSERT INTO `gen_table_column` VALUES (651, '49', 'each_amount', '单项金额', 'decimal(24,6)', 'BigDecimal', 'eachAmount', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 8, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:53:29');
INSERT INTO `gen_table_column` VALUES (652, '49', 'remark', '单据备注', 'varchar(100)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 9, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:53:29');
INSERT INTO `gen_table_column` VALUES (654, '49', 'del_flag', '删除标记，0未删除，1删除', 'varchar(1)', 'String', 'delFlag', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, NULL, NULL, 11, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:53:29');
INSERT INTO `gen_table_column` VALUES (655, '50', 'id', '主键', 'bigint', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, NULL, NULL, 1, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:53:43');
INSERT INTO `gen_table_column` VALUES (656, '50', 'name', '名称', 'varchar(50)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', NULL, NULL, 2, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:53:43');
INSERT INTO `gen_table_column` VALUES (657, '50', 'type', '类型', 'varchar(20)', 'String', 'type', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', '', NULL, NULL, 3, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:53:43');
INSERT INTO `gen_table_column` VALUES (658, '50', 'remark', '备注', 'varchar(100)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 4, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:53:43');
INSERT INTO `gen_table_column` VALUES (659, '50', 'enabled', '启用', 'char(1)', 'String', 'enabled', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'select', 'sys_yes_no', NULL, NULL, 5, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:53:43');
INSERT INTO `gen_table_column` VALUES (660, '50', 'sort', '排序', 'varchar(10)', 'String', 'sort', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', NULL, NULL, 6, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:53:43');
INSERT INTO `gen_table_column` VALUES (661, '50', 'tenant_id', '租户id', 'bigint', 'Long', 'tenantId', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', NULL, NULL, 7, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:53:43');
INSERT INTO `gen_table_column` VALUES (662, '50', 'del_flag', '删除标记，0未删除，1删除', 'varchar(1)', 'String', 'delFlag', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', NULL, NULL, NULL, 8, NULL, 'admin', '2023-01-28 01:26:11', NULL, '2023-01-28 01:53:43');
INSERT INTO `gen_table_column` VALUES (663, '47', 'update_by', '更新人', 'varchar(32)', 'String', 'updateBy', '0', '0', NULL, '1', NULL, '1', NULL, 'EQ', 'input', '', NULL, NULL, 11, NULL, '', '2023-01-28 01:36:05', '', '2023-01-28 01:52:12');
INSERT INTO `gen_table_column` VALUES (664, '47', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, NULL, NULL, '1', '1', 'BETWEEN', 'datetime', '', NULL, NULL, 12, NULL, '', '2023-01-28 01:36:05', '', '2023-01-28 01:52:12');
INSERT INTO `gen_table_column` VALUES (665, '48', 'update_by', '更新人', 'varchar(32)', 'String', 'updateBy', '0', '0', NULL, '1', NULL, '1', NULL, 'EQ', 'input', '', NULL, NULL, 17, NULL, '', '2023-01-28 01:36:07', '', '2023-01-28 01:52:27');
INSERT INTO `gen_table_column` VALUES (666, '48', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, NULL, NULL, '1', '1', 'EQ', 'datetime', '', NULL, NULL, 18, NULL, '', '2023-01-28 01:36:07', '', '2023-01-28 01:52:27');
INSERT INTO `gen_table_column` VALUES (667, '49', 'tenant_id', '租户id', 'int', 'Long', 'tenantId', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', NULL, NULL, 10, NULL, '', '2023-01-28 01:36:09', NULL, '2023-01-28 01:53:29');
INSERT INTO `gen_table_column` VALUES (668, '49', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, NULL, NULL, '1', '1', 'BETWEEN', 'datetime', NULL, NULL, NULL, 12, NULL, '', '2023-01-28 01:36:09', NULL, '2023-01-28 01:53:29');
INSERT INTO `gen_table_column` VALUES (669, '49', 'update_by', '更新人', 'varchar(32)', 'String', 'updateBy', '0', '0', NULL, '1', NULL, '1', NULL, 'EQ', 'input', NULL, NULL, NULL, 13, NULL, '', '2023-01-28 01:36:09', NULL, '2023-01-28 01:53:29');
INSERT INTO `gen_table_column` VALUES (670, '50', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, NULL, NULL, '1', '1', 'BETWEEN', 'datetime', NULL, NULL, NULL, 9, NULL, '', '2023-01-28 01:36:13', NULL, '2023-01-28 01:53:43');
INSERT INTO `gen_table_column` VALUES (671, '50', 'update_by', '更新人', 'varchar(32)', 'String', 'updateBy', '0', '0', NULL, '1', NULL, '1', NULL, 'EQ', 'input', NULL, NULL, NULL, 10, NULL, '', '2023-01-28 01:36:13', NULL, '2023-01-28 01:53:43');

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `sched_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `blob_data` blob NULL COMMENT '存放持久化Trigger对象',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Blob类型的触发器表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `sched_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `calendar_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '日历名称',
  `calendar` blob NOT NULL COMMENT '存放持久化calendar对象',
  PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '日历信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `sched_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `cron_expression` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'cron表达式',
  `time_zone_id` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '时区',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Cron类型的触发器表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `sched_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `entry_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度器实例id',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度器实例名',
  `fired_time` bigint NOT NULL COMMENT '触发的时间',
  `sched_time` bigint NOT NULL COMMENT '定时器制定的时间',
  `priority` int NOT NULL COMMENT '优先级',
  `state` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态',
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务组名',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否并发',
  `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否接受恢复执行',
  PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '已触发的触发器表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `sched_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `job_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务组名',
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '相关介绍',
  `job_class_name` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '执行任务类名称',
  `is_durable` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否持久化',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否并发',
  `is_update_data` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否更新数据',
  `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否接受恢复执行',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '任务详细信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `sched_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `lock_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '悲观锁名称',
  PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '存储的悲观锁信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '暂停的触发器表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `sched_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `instance_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '实例名称',
  `last_checkin_time` bigint NOT NULL COMMENT '上次检查时间',
  `checkin_interval` bigint NOT NULL COMMENT '检查间隔时间',
  PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '调度器状态表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `sched_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `repeat_count` bigint NOT NULL COMMENT '重复的次数统计',
  `repeat_interval` bigint NOT NULL COMMENT '重复的间隔时间',
  `times_triggered` bigint NOT NULL COMMENT '已经触发的次数',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '简单触发器的信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `sched_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `str_prop_1` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第一个参数',
  `str_prop_2` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第二个参数',
  `str_prop_3` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第三个参数',
  `int_prop_1` int NULL DEFAULT NULL COMMENT 'int类型的trigger的第一个参数',
  `int_prop_2` int NULL DEFAULT NULL COMMENT 'int类型的trigger的第二个参数',
  `long_prop_1` bigint NULL DEFAULT NULL COMMENT 'long类型的trigger的第一个参数',
  `long_prop_2` bigint NULL DEFAULT NULL COMMENT 'long类型的trigger的第二个参数',
  `dec_prop_1` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第一个参数',
  `dec_prop_2` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第二个参数',
  `bool_prop_1` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第一个参数',
  `bool_prop_2` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第二个参数',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '同步机制的行锁表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `sched_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '触发器的名字',
  `trigger_group` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '触发器所属组的名字',
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_job_details表job_name的外键',
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_job_details表job_group的外键',
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '相关介绍',
  `next_fire_time` bigint NULL DEFAULT NULL COMMENT '上一次触发时间（毫秒）',
  `prev_fire_time` bigint NULL DEFAULT NULL COMMENT '下一次触发时间（默认为-1表示不触发）',
  `priority` int NULL DEFAULT NULL COMMENT '优先级',
  `trigger_state` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '触发器状态',
  `trigger_type` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '触发器的类型',
  `start_time` bigint NOT NULL COMMENT '开始时间',
  `end_time` bigint NULL DEFAULT NULL COMMENT '结束时间',
  `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日程表名称',
  `misfire_instr` smallint NULL DEFAULT NULL COMMENT '补偿执行的策略',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '触发器详细信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` bigint NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `tenant_id` int NULL DEFAULT NULL COMMENT '租户',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '参数配置表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2022-04-03 16:23:22', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow', 1);
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2022-04-03 16:23:22', '', NULL, '初始化密码 123456', 1);
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2022-04-03 16:23:22', '', NULL, '深色主题theme-dark，浅色主题theme-light', 1);
INSERT INTO `sys_config` VALUES (4, '账号自助-验证码开关', 'sys.account.captchaOnOff', 'true', 'Y', 'admin', '2022-04-03 16:23:22', '', NULL, '是否开启验证码功能（true开启，false关闭）', 1);
INSERT INTO `sys_config` VALUES (5, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'true', 'Y', 'admin', '2022-04-03 16:23:22', '', NULL, '是否开启注册用户功能（true开启，false关闭）', 1);

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint NULL DEFAULT NULL COMMENT '父部门id',
  `ancestors` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `tenant_id` int NULL DEFAULT NULL COMMENT '租户',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (1, 0, '0', '机构1', 1, NULL, NULL, NULL, '0', '0', 'admin', '2023-01-10 00:02:54', 'admin', '2023-01-10 00:05:25', 1);
INSERT INTO `sys_dept` VALUES (4, 1, '0,1', '12', 12, NULL, NULL, NULL, '0', '0', 'admin', '2023-01-27 22:39:25', '', NULL, 1);
INSERT INTO `sys_dept` VALUES (5, 4, '0,1,4', '13', 13, NULL, NULL, NULL, '0', '0', 'admin', '2023-01-27 22:56:56', '', NULL, 1);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 129 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典数据表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', '1', 'sys_job_group', '', '', 'Y', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', '2', 'sys_job_group', '', '', 'N', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', '1', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', '0', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2022-04-03 16:23:22', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (100, 1, '智联招聘', '1', 'customer_source', NULL, 'default', 'N', '0', 'admin', '2022-04-11 22:21:09', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (101, 2, '五八同城', '2', 'customer_source', NULL, 'default', 'N', '0', 'admin', '2022-04-11 22:21:25', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (102, 3, '前程无忧', '3', 'customer_source', NULL, 'default', 'N', '0', 'admin', '2022-04-11 22:21:38', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (103, 4, 'BOSS直聘', '4', 'customer_source', NULL, 'default', 'N', '1', 'admin', '2022-04-11 22:22:26', 'admin', '2022-09-07 14:40:23', NULL);
INSERT INTO `sys_dict_data` VALUES (105, 0, '转介绍', '0', 'customer_source', NULL, 'default', 'N', '0', 'admin', '2022-04-11 22:23:23', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (106, 0, '有意向', '0', 'customer_state', NULL, 'default', 'N', '0', 'admin', '2022-04-13 15:49:09', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (107, 9, '已拒绝', '1', 'customer_state', '2', 'default', 'N', '0', 'admin', '2022-04-13 15:49:44', 'wr', '2022-06-30 11:36:41', NULL);
INSERT INTO `sys_dict_data` VALUES (108, 2, '初次面试', '2', 'customer_state', NULL, 'default', 'N', '0', 'admin', '2022-04-13 15:50:07', 'wr', '2022-06-30 11:36:55', NULL);
INSERT INTO `sys_dict_data` VALUES (109, 3, '复试', '3', 'customer_state', NULL, 'default', 'N', '0', 'admin', '2022-04-13 15:50:15', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (110, 4, '成交', '4', 'customer_state', NULL, 'default', 'N', '0', 'admin', '2022-04-13 15:51:19', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (112, 0, '建行对公账户', '0', 'account_credited', NULL, 'default', 'N', '0', 'admin', '2022-04-15 15:06:37', 'admin', '2022-04-15 15:07:40', NULL);
INSERT INTO `sys_dict_data` VALUES (113, 1, '微信账户', '1', 'account_credited', NULL, 'default', 'N', '0', 'admin', '2022-04-15 15:06:49', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (114, 2, '支付宝账户', '2', 'account_credited', NULL, 'default', 'N', '0', 'admin', '2022-04-15 15:07:21', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (115, 1, '客服岗位类', '1', 'productType', NULL, 'default', 'N', '0', 'admin', '2022-05-13 20:47:36', 'admin', '2022-05-13 20:48:16', NULL);
INSERT INTO `sys_dict_data` VALUES (116, 2, '开发岗位类', '2', 'productType', NULL, 'default', 'N', '0', 'admin', '2022-05-13 20:48:04', 'admin', '2022-05-13 20:48:22', NULL);
INSERT INTO `sys_dict_data` VALUES (117, 3, '技能咨询类', '3', 'productType', NULL, 'default', 'N', '0', 'admin', '2022-05-13 20:50:51', 'admin', '2022-05-13 20:56:34', NULL);
INSERT INTO `sys_dict_data` VALUES (118, 9, '网络数据', '9', 'customer_source', NULL, 'default', 'N', '0', 'admin', '2022-05-15 10:59:02', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (119, 0, '待审核', '0', 'customer_order_state', NULL, 'info', 'N', '0', 'admin', '2022-05-15 11:55:57', 'wr', '2022-06-30 11:37:45', NULL);
INSERT INTO `sys_dict_data` VALUES (120, 1, '订单有效', '1', 'customer_order_state', NULL, 'success', 'N', '0', 'admin', '2022-05-15 11:56:34', 'admin', '2022-05-15 15:35:09', NULL);
INSERT INTO `sys_dict_data` VALUES (121, 2, '订单无效', '2', 'customer_order_state', NULL, 'danger', 'N', '0', 'admin', '2022-05-15 11:57:02', 'admin', '2022-05-15 15:35:00', NULL);
INSERT INTO `sys_dict_data` VALUES (122, 3, '已完成', '3', 'customer_order_state', NULL, 'primary', 'N', '0', 'admin', '2022-05-15 15:35:35', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (123, 5, '合肥招聘网', '5', 'customer_source', NULL, 'default', 'N', '0', 'admin', '2022-09-07 14:32:35', '', NULL, 'https://www.hfzp.cc/');
INSERT INTO `sys_dict_data` VALUES (124, 9, '新安人才', '9', 'customer_source', NULL, 'default', 'N', '0', 'wr', '2022-12-17 13:08:36', 'wr', '2022-12-17 13:08:51', '新安人才网');
INSERT INTO `sys_dict_data` VALUES (125, 0, '系统字典', '0', 'dict_sys_type', NULL, 'primary', 'N', '0', 'wr', '2022-12-17 13:08:36', 'wr', NULL, '系统字典类别');
INSERT INTO `sys_dict_data` VALUES (126, 1, '动态字典', '1', 'dict_sys_type', NULL, 'warning', 'N', '0', 'wr', '2022-12-17 13:08:36', '', NULL, '动态系统字典');
INSERT INTO `sys_dict_data` VALUES (127, 0, '个人客户', '0', 'crm_customer_type', NULL, NULL, 'N', '0', '', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (128, 0, '公司客户', '1', 'crm_customer_type', NULL, NULL, 'N', '0', '', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (129, 0, '团体客户', '2', 'crm_customer_type', NULL, NULL, 'N', '0', '', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (130, 1, '类别1', '1', 'wms_supplier_type', NULL, 'default', 'N', '0', 'admin', '2023-01-27 17:01:44', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (131, 1, '类别1', '1', 'wms_warehouse_type', NULL, 'default', 'N', '0', 'admin', '2023-01-27 18:02:51', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类名',
  `dict_sys_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统字典类别',
  `data_sql` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '动态sql',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 113 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典类型表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', NULL, '0', 'admin', '2022-04-03 16:23:21', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', NULL, '0', 'admin', '2022-04-03 16:23:21', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', NULL, '0', 'admin', '2022-04-03 16:23:21', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', NULL, '0', 'admin', '2022-04-03 16:23:21', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', NULL, '0', 'admin', '2022-04-03 16:23:21', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', NULL, '0', 'admin', '2022-04-03 16:23:21', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', NULL, '0', 'admin', '2022-04-03 16:23:21', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', NULL, '0', 'admin', '2022-04-03 16:23:22', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', NULL, '0', 'admin', '2022-04-03 16:23:22', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', NULL, '0', 'admin', '2022-04-03 16:23:22', '', NULL, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (100, '客户来源', 'customer_source', '0', NULL, '0', 'admin', '2022-04-11 22:18:16', '', NULL, 'crm系统');
INSERT INTO `sys_dict_type` VALUES (101, '跟单状态', 'customer_state', '0', NULL, '0', 'admin', '2022-04-13 15:47:47', 'admin', '2022-04-14 12:20:32', 'crm用户跟单状态');
INSERT INTO `sys_dict_type` VALUES (102, '订单状态', 'customer_order_state', '0', NULL, '0', 'admin', '2022-04-14 12:21:14', 'admin', '2022-04-14 12:21:34', '用户订单状态');
INSERT INTO `sys_dict_type` VALUES (103, '收款账户', 'account_credited', '0', NULL, '0', 'admin', '2022-04-15 15:06:12', 'admin', '2022-04-15 15:08:06', 'crm收款账户');
INSERT INTO `sys_dict_type` VALUES (104, '产品类别', 'productType', '0', NULL, '0', 'admin', '2022-05-13 20:44:57', '', '2022-05-13 20:44:57', 'crm产品类别');
INSERT INTO `sys_dict_type` VALUES (105, '销售', 'crm_sales', '1', '{\"dict_label\":\"a.nick_name\", \"dict_value\":\"a.user_id\", \"dict_type\":\"crm_sales\", \"table_name\":\"sys_user a\", \"and\":[\"a.user_id IN (SELECT b.user_id from sys_user_role b WHERE b.role_id = 100 )\"]}', '0', 'admin', '2022-05-13 20:44:57', 'admin', '2022-06-26 16:09:17', 'crm系统的销售');
INSERT INTO `sys_dict_type` VALUES (108, '产品列表', 'crm_product', '1', '{\"dict_label\":\"a.name\", \"dict_value\":\"a.id\", \"dict_type\":\"crm_product\", \"table_name\":\"crm_product a\", \"and\":[\"1 = 1\"]}', '0', 'admin', '2022-07-20 22:52:54', 'admin', '2022-07-20 23:17:08', 'crm产品列表');
INSERT INTO `sys_dict_type` VALUES (110, '客户类别', 'crm_customer_type', '0', '', '0', '', NULL, '', '2023-01-01 18:59:16', NULL);
INSERT INTO `sys_dict_type` VALUES (111, '系统字典类别', 'dict_sys_type', '0', '0', '0', '', NULL, '', '2023-01-01 19:03:59', NULL);
INSERT INTO `sys_dict_type` VALUES (113, '供应商类别', 'wms_supplier_type', '0', '', '0', 'admin', '2023-01-25 20:45:33', 'admin', '2023-01-27 17:01:06', '仓储模块-供应商类别');
INSERT INTO `sys_dict_type` VALUES (114, '仓库类别', 'wms_warehouse_type', '0', '0', '0', 'admin', '2023-01-27 17:59:26', '', '2023-01-27 17:59:26', '仓储模块，仓库类别');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注信息',
  `tenant_id` int NULL DEFAULT NULL COMMENT '租户',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'mnTask.mnNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2022-04-03 16:23:22', 'wr', '2023-01-24 18:42:31', '', 1);
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'mnTask.mnParams(\'mn\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2022-04-03 16:23:22', 'wr', '2022-05-27 20:54:12', '', 1);
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'mnTask.mnMultipleParams(\'mn\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2022-04-03 16:23:22', 'wr', '2022-05-27 20:53:50', '', 1);
INSERT INTO `sys_job` VALUES (100, '清除客户归属销售', 'DEFAULT', 'crmTask.customerClearSales()', '0 0 0/12 * * ?', '1', '1', '1', 'wr', '2022-05-30 15:04:41', 'wr', '2023-01-24 18:42:37', '', 1);

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------
INSERT INTO `sys_job_log` VALUES (1, '清除客户归属销售', 'DEFAULT', 'crmTask.customerClearSales()', '清除客户归属销售 总共耗时：43毫秒', '0', '', '2023-01-06 23:42:57');

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统访问记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (1, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-24 19:28:08');
INSERT INTO `sys_logininfor` VALUES (2, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2023-01-24 19:34:05');
INSERT INTO `sys_logininfor` VALUES (3, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-24 19:34:11');
INSERT INTO `sys_logininfor` VALUES (4, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2023-01-24 19:42:15');
INSERT INTO `sys_logininfor` VALUES (5, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-24 19:42:27');
INSERT INTO `sys_logininfor` VALUES (6, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2023-01-24 20:05:29');
INSERT INTO `sys_logininfor` VALUES (7, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-24 20:05:34');
INSERT INTO `sys_logininfor` VALUES (8, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2023-01-24 20:05:38');
INSERT INTO `sys_logininfor` VALUES (9, 'wr', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-24 20:05:46');
INSERT INTO `sys_logininfor` VALUES (10, 'wr', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-25 10:03:50');
INSERT INTO `sys_logininfor` VALUES (11, 'wr', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2023-01-25 10:04:01');
INSERT INTO `sys_logininfor` VALUES (12, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-25 10:04:07');
INSERT INTO `sys_logininfor` VALUES (13, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2023-01-25 10:25:12');
INSERT INTO `sys_logininfor` VALUES (14, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-25 10:25:25');
INSERT INTO `sys_logininfor` VALUES (15, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-25 13:33:48');
INSERT INTO `sys_logininfor` VALUES (16, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2023-01-25 13:36:10');
INSERT INTO `sys_logininfor` VALUES (17, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-25 13:36:14');
INSERT INTO `sys_logininfor` VALUES (18, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2023-01-25 13:37:36');
INSERT INTO `sys_logininfor` VALUES (19, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-25 13:37:41');
INSERT INTO `sys_logininfor` VALUES (20, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-25 14:26:29');
INSERT INTO `sys_logininfor` VALUES (21, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-25 15:19:57');
INSERT INTO `sys_logininfor` VALUES (22, 'admin', '127.0.0.1', '内网IP', 'Chrome Mobile', 'Android 6.x', '0', '登录成功', '2023-01-25 15:45:16');
INSERT INTO `sys_logininfor` VALUES (23, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-25 15:46:43');
INSERT INTO `sys_logininfor` VALUES (24, 'admin', '192.168.1.27', '内网IP', 'Chrome Mobile', 'Android 1.x', '0', '登录成功', '2023-01-25 15:48:09');
INSERT INTO `sys_logininfor` VALUES (25, 'admin', '127.0.0.1', '内网IP', 'Chrome Mobile', 'Android 6.x', '0', '退出成功', '2023-01-25 16:00:02');
INSERT INTO `sys_logininfor` VALUES (26, 'wr', '127.0.0.1', '内网IP', 'Chrome Mobile', 'Android 6.x', '1', '验证码错误', '2023-01-25 16:00:16');
INSERT INTO `sys_logininfor` VALUES (27, 'wr', '127.0.0.1', '内网IP', 'Chrome Mobile', 'Android 6.x', '1', '用户不存在/密码错误', '2023-01-25 16:00:21');
INSERT INTO `sys_logininfor` VALUES (28, 'wr', '127.0.0.1', '内网IP', 'Chrome Mobile', 'Android 6.x', '0', '登录成功', '2023-01-25 16:00:32');
INSERT INTO `sys_logininfor` VALUES (29, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-25 16:06:51');
INSERT INTO `sys_logininfor` VALUES (30, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2023-01-25 16:35:28');
INSERT INTO `sys_logininfor` VALUES (31, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2023-01-25 16:35:28');
INSERT INTO `sys_logininfor` VALUES (32, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-25 16:36:39');
INSERT INTO `sys_logininfor` VALUES (33, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2023-01-25 16:36:57');
INSERT INTO `sys_logininfor` VALUES (34, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-25 16:37:15');
INSERT INTO `sys_logininfor` VALUES (35, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-25 18:41:32');
INSERT INTO `sys_logininfor` VALUES (36, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-25 20:18:53');
INSERT INTO `sys_logininfor` VALUES (37, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-27 16:46:17');
INSERT INTO `sys_logininfor` VALUES (38, 'admin', '192.168.1.17', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-27 17:48:27');
INSERT INTO `sys_logininfor` VALUES (39, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-27 19:01:36');
INSERT INTO `sys_logininfor` VALUES (40, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2023-01-27 19:45:27');
INSERT INTO `sys_logininfor` VALUES (41, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-27 19:45:32');
INSERT INTO `sys_logininfor` VALUES (42, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-27 20:47:35');
INSERT INTO `sys_logininfor` VALUES (43, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-27 21:44:30');
INSERT INTO `sys_logininfor` VALUES (44, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-27 22:37:48');
INSERT INTO `sys_logininfor` VALUES (45, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2023-01-28 01:25:45');
INSERT INTO `sys_logininfor` VALUES (46, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2023-01-28 01:25:49');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路由参数',
  `is_frame` int NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2064 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 9, 'system', NULL, '', 1, 0, 'M', '0', '0', '', 'system', 'admin', '2022-04-03 16:23:21', 'admin', '2023-01-25 21:11:56', '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 9, 'monitor', NULL, '', 1, 0, 'M', '0', '0', '', 'monitor', 'admin', '2022-04-03 16:23:21', 'admin', '2023-01-25 21:12:14', '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 9, 'tool', NULL, '', 1, 0, 'M', '0', '0', '', 'tool', 'admin', '2022-04-03 16:23:21', 'admin', '2023-01-25 21:12:03', '系统工具目录');
INSERT INTO `sys_menu` VALUES (4, '公司官网', 0, 100, 'https://www.baidu.com/', NULL, '', 0, 0, 'M', '1', '1', '', 'guide', 'admin', '2022-04-03 16:23:21', 'admin', '2023-01-25 21:12:36', '若依官网地址');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', '', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2022-04-03 16:23:21', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', '', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2022-04-03 16:23:21', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', '', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2022-04-03 16:23:21', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', '', 1, 0, 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2022-04-03 16:23:21', '', NULL, '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', '', 1, 0, 'C', '0', '0', 'system:post:list', 'post', 'admin', '2022-04-03 16:23:21', '', NULL, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', '', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2022-04-03 16:23:21', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', '', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2022-04-03 16:23:21', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', '', 1, 0, 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2022-04-03 16:23:21', '', NULL, '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, 'log', '', '', 1, 0, 'M', '0', '0', '', 'log', 'admin', '2022-04-03 16:23:21', '', NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', '', 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2022-04-03 16:23:21', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', '', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2022-04-03 16:23:21', '', NULL, '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, 'druid', 'monitor/druid/index', '', 1, 0, 'C', '0', '0', 'monitor:druid:list', 'druid', 'admin', '2022-04-03 16:23:21', '', NULL, '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 4, 'server', 'monitor/server/index', '', 1, 0, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2022-04-03 16:23:21', '', NULL, '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '缓存监控', 2, 5, 'cache', 'monitor/cache/index', '', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis', 'admin', '2022-04-03 16:23:21', '', NULL, '缓存监控菜单');
INSERT INTO `sys_menu` VALUES (114, '表单构建', 3, 1, 'build', 'tool/build/index', '', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2022-04-03 16:23:21', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu` VALUES (115, '代码生成', 3, 2, 'gen', 'tool/gen/index', '', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2022-04-03 16:23:21', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (116, '系统接口', 3, 3, 'swagger', 'tool/swagger/index', '', 1, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2022-04-03 16:23:21', '', NULL, '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, 'operlog', 'monitor/operlog/index', '', 1, 0, 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', '2022-04-03 16:23:21', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, 'logininfor', 'monitor/logininfor/index', '', 1, 0, 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2022-04-03 16:23:21', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1001, '用户查询', 100, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户新增', 100, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户修改', 100, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户删除', 100, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导出', 100, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '用户导入', 100, 6, '', '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '重置密码', 100, 7, '', '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色查询', 101, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色新增', 101, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色修改', 101, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色删除', 101, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '角色导出', 101, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单查询', 102, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单新增', 102, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单修改', 102, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '菜单删除', 102, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门查询', 103, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门新增', 103, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门修改', 103, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '部门删除', 103, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位查询', 104, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位新增', 104, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位修改', 104, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位删除', 104, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '岗位导出', 104, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典查询', 105, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典新增', 105, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典修改', 105, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典删除', 105, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '字典导出', 105, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数查询', 106, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数新增', 106, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数修改', 106, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数删除', 106, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '参数导出', 106, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告查询', 107, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告新增', 107, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告修改', 107, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '公告删除', 107, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '操作查询', 500, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:query', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '操作删除', 500, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '日志导出', 500, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '登录查询', 501, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:query', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '登录删除', 501, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '日志导出', 501, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '在线查询', 109, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '批量强退', 109, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '单条强退', 109, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '任务查询', 110, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '任务新增', 110, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '任务修改', 110, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '任务删除', 110, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '状态修改', 110, 5, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '任务导出', 110, 7, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '生成查询', 115, 1, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '生成修改', 115, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '生成删除', 115, 3, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '导入代码', 115, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '预览代码', 115, 4, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '生成代码', 115, 5, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2022-04-03 16:23:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2000, '客户管理', 0, 1, 'crm', NULL, NULL, 1, 0, 'M', '0', '0', '', 'peoples', 'admin', '2022-04-03 18:34:30', 'admin', '2023-01-25 21:12:56', '');
INSERT INTO `sys_menu` VALUES (2014, '财务收款', 2000, 5, 'finance', 'crm/finance/index', NULL, 1, 0, 'C', '0', '0', 'crm:finance:list', 'money', 'admin', '2022-04-09 14:22:35', 'admin', '2022-04-09 15:04:53', '财务收款菜单');
INSERT INTO `sys_menu` VALUES (2015, '财务收款查询', 2014, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:finance:query', '#', 'admin', '2022-04-09 14:22:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2016, '财务收款新增', 2014, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:finance:add', '#', 'admin', '2022-04-09 14:22:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2017, '财务收款修改', 2014, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:finance:edit', '#', 'admin', '2022-04-09 14:22:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2018, '财务收款删除', 2014, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:finance:remove', '#', 'admin', '2022-04-09 14:22:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2019, '财务收款导出', 2014, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:finance:export', '#', 'admin', '2022-04-09 14:22:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2020, '客户列表', 2000, 1, 'customer', 'crm/customer/index', NULL, 1, 0, 'C', '0', '0', 'crm:customer:list', 'people', 'admin', '2022-04-09 14:22:39', 'wr', '2023-01-06 23:49:07', '客户联系人菜单');
INSERT INTO `sys_menu` VALUES (2021, '客户联系人查询', 2020, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:customer:query', '#', 'admin', '2022-04-09 14:22:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2022, '客户联系人新增', 2020, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:customer:add', '#', 'admin', '2022-04-09 14:22:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2023, '客户联系人修改', 2020, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:customer:edit', '#', 'admin', '2022-04-09 14:22:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2024, '客户联系人删除', 2020, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:customer:remove', '#', 'admin', '2022-04-09 14:22:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2025, '客户联系人导出', 2020, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:customer:export', '#', 'admin', '2022-04-09 14:22:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2026, '客户订单', 2000, 3, 'order', 'crm/order/index', NULL, 1, 0, 'C', '0', '0', 'crm:order:list', 'edit', 'admin', '2022-04-09 14:22:42', 'admin', '2022-04-09 15:05:31', '客户订单菜单');
INSERT INTO `sys_menu` VALUES (2027, '客户订单查询', 2026, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:order:query', '#', 'admin', '2022-04-09 14:22:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2028, '客户订单新增', 2026, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:order:add', '#', 'admin', '2022-04-09 14:22:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2029, '客户订单修改', 2026, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:order:edit', '#', 'admin', '2022-04-09 14:22:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2030, '客户订单删除', 2026, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:order:remove', '#', 'admin', '2022-04-09 14:22:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2031, '客户订单导出', 2026, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:order:export', '#', 'admin', '2022-04-09 14:22:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2032, '订单跟踪', 2000, 2, 'orderSupervisor', 'crm/orderSupervisor/index', NULL, 1, 0, 'C', '0', '0', 'crm:orderSupervisor:list', 'shopping', 'admin', '2022-04-09 14:22:46', 'admin', '2022-04-09 15:06:00', '订单跟踪菜单');
INSERT INTO `sys_menu` VALUES (2033, '订单跟踪查询', 2032, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:orderSupervisor:query', '#', 'admin', '2022-04-09 14:22:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2034, '订单跟踪新增', 2032, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:orderSupervisor:add', '#', 'admin', '2022-04-09 14:22:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2035, '订单跟踪修改', 2032, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:orderSupervisor:edit', '#', 'admin', '2022-04-09 14:22:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2036, '订单跟踪删除', 2032, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:orderSupervisor:remove', '#', 'admin', '2022-04-09 14:22:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2037, '订单跟踪导出', 2032, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:orderSupervisor:export', '#', 'admin', '2022-04-09 14:22:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2038, '产品信息', 2000, 6, 'product', 'crm/product/index', NULL, 1, 0, 'C', '0', '0', 'crm:product:list', 'validCode', 'admin', '2022-04-09 14:22:49', 'admin', '2022-04-09 15:07:55', '产品信息菜单');
INSERT INTO `sys_menu` VALUES (2039, '产品信息查询', 2038, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:product:query', '#', 'admin', '2022-04-09 14:22:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2040, '产品信息新增', 2038, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:product:add', '#', 'admin', '2022-04-09 14:22:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2041, '产品信息修改', 2038, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:product:edit', '#', 'admin', '2022-04-09 14:22:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2042, '产品信息删除', 2038, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:product:remove', '#', 'admin', '2022-04-09 14:22:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2043, '产品信息导出', 2038, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:product:export', '#', 'admin', '2022-04-09 14:22:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2044, '简历信息', 2000, 4, 'resume', 'crm/resume/index', NULL, 1, 0, 'C', '0', '0', 'crm:resume:list', 'rate', 'admin', '2022-04-09 14:22:54', 'admin', '2022-04-09 15:06:38', '简历信息菜单');
INSERT INTO `sys_menu` VALUES (2045, '简历信息查询', 2044, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:resume:query', '#', 'admin', '2022-04-09 14:22:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2046, '简历信息新增', 2044, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:resume:add', '#', 'admin', '2022-04-09 14:22:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2047, '简历信息修改', 2044, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:resume:edit', '#', 'admin', '2022-04-09 14:22:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2048, '简历信息删除', 2044, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:resume:remove', '#', 'admin', '2022-04-09 14:22:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2049, '简历信息导出', 2044, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'crm:resume:export', '#', 'admin', '2022-04-09 14:22:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2051, '查询所有', 2020, 2, '', NULL, NULL, 1, 0, 'F', '0', '0', 'crm:customer:queryAll', '#', 'admin', '2022-05-12 16:28:49', 'wr', '2023-01-01 16:34:52', '');
INSERT INTO `sys_menu` VALUES (2053, '详情', 2020, 7, '', NULL, NULL, 1, 0, 'F', '0', '0', 'crm:customer:show', '#', 'admin', '2022-05-15 14:24:20', 'wr', '2023-01-01 16:35:04', '');
INSERT INTO `sys_menu` VALUES (2054, '取得客户归属', 2020, 8, '', NULL, NULL, 1, 0, 'F', '0', '0', 'crm:customer:take', '#', 'wr', '2022-05-27 20:19:52', 'wr', '2023-01-01 16:35:14', '');
INSERT INTO `sys_menu` VALUES (2055, '系统租户', 1, 1, 'tenant', 'system/tenant/index', NULL, 1, 0, 'C', '0', '0', 'system:tenant:list', 'list', 'admin', '2023-01-21 12:25:09', 'admin', '2023-01-25 10:07:42', '租户菜单');
INSERT INTO `sys_menu` VALUES (2056, '租户查询', 2055, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:tenant:query', '#', 'admin', '2023-01-21 12:25:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2057, '租户新增', 2055, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:tenant:add', '#', 'admin', '2023-01-21 12:25:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2058, '租户修改', 2055, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:tenant:edit', '#', 'admin', '2023-01-21 12:25:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2059, '租户删除', 2055, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:tenant:remove', '#', 'admin', '2023-01-21 12:25:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2060, '租户导出', 2055, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:tenant:export', '#', 'admin', '2023-01-21 12:25:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2064, '仓储管理', 0, 2, 'wms', NULL, NULL, 1, 0, 'M', '0', '0', '', 'textarea', 'admin', '2023-01-25 21:11:01', 'admin', '2023-01-25 21:13:02', '');
INSERT INTO `sys_menu` VALUES (2065, '供应商信息', 2064, 1, 'supplier', 'wms/supplier/index', NULL, 1, 0, 'C', '0', '0', 'wms:supplier:list', 'peoples', 'admin', '2023-01-27 16:51:57', 'admin', '2023-01-27 19:46:07', '供应商信息菜单');
INSERT INTO `sys_menu` VALUES (2066, '供应商信息查询', 2065, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:supplier:query', '#', 'admin', '2023-01-27 16:51:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2067, '供应商信息新增', 2065, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:supplier:add', '#', 'admin', '2023-01-27 16:51:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2068, '供应商信息修改', 2065, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:supplier:edit', '#', 'admin', '2023-01-27 16:51:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2069, '供应商信息删除', 2065, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:supplier:remove', '#', 'admin', '2023-01-27 16:51:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2070, '供应商信息导出', 2065, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:supplier:export', '#', 'admin', '2023-01-27 16:51:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2071, '仓库信息', 2064, 1, 'warehouse', 'wms/warehouse/index', NULL, 1, 0, 'C', '0', '0', 'wms:warehouse:list', 'form', 'admin', '2023-01-27 18:47:41', 'admin', '2023-01-27 19:47:32', '仓库信息菜单');
INSERT INTO `sys_menu` VALUES (2072, '仓库信息查询', 2071, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:warehouse:query', '#', 'admin', '2023-01-27 18:47:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2073, '仓库信息新增', 2071, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:warehouse:add', '#', 'admin', '2023-01-27 18:47:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2074, '仓库信息修改', 2071, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:warehouse:edit', '#', 'admin', '2023-01-27 18:47:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2075, '仓库信息删除', 2071, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:warehouse:remove', '#', 'admin', '2023-01-27 18:47:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2076, '仓库信息导出', 2071, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:warehouse:export', '#', 'admin', '2023-01-27 18:47:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2077, '产品', 2064, 1, 'material', 'wms/material/index', NULL, 1, 0, 'C', '0', '0', 'wms:material:list', '#', 'admin', '2023-01-27 23:56:18', '', NULL, '产品菜单');
INSERT INTO `sys_menu` VALUES (2078, '产品查询', 2077, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:material:query', '#', 'admin', '2023-01-27 23:56:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2079, '产品新增', 2077, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:material:add', '#', 'admin', '2023-01-27 23:56:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2080, '产品修改', 2077, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:material:edit', '#', 'admin', '2023-01-27 23:56:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2081, '产品删除', 2077, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:material:remove', '#', 'admin', '2023-01-27 23:56:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2082, '产品导出', 2077, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:material:export', '#', 'admin', '2023-01-27 23:56:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2083, '商品库存', 2064, 1, 'stock', 'wms/stock/index', NULL, 1, 0, 'C', '0', '0', 'wms:stock:list', '#', 'admin', '2023-01-27 23:57:13', '', NULL, '商品库存菜单');
INSERT INTO `sys_menu` VALUES (2084, '商品库存查询', 2083, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:stock:query', '#', 'admin', '2023-01-27 23:57:13', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2085, '商品库存新增', 2083, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:stock:add', '#', 'admin', '2023-01-27 23:57:13', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2086, '商品库存修改', 2083, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:stock:edit', '#', 'admin', '2023-01-27 23:57:13', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2087, '商品库存删除', 2083, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:stock:remove', '#', 'admin', '2023-01-27 23:57:13', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2088, '商品库存导出', 2083, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:stock:export', '#', 'admin', '2023-01-27 23:57:13', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2089, '商品类型', 2064, 1, 'category', 'wms/category/index', NULL, 1, 0, 'C', '0', '0', 'wms:category:list', '#', 'admin', '2023-01-27 23:57:57', '', NULL, '商品类型菜单');
INSERT INTO `sys_menu` VALUES (2090, '商品类型查询', 2089, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:category:query', '#', 'admin', '2023-01-27 23:57:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2091, '商品类型新增', 2089, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:category:add', '#', 'admin', '2023-01-27 23:57:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2092, '商品类型修改', 2089, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:category:edit', '#', 'admin', '2023-01-27 23:57:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2093, '商品类型删除', 2089, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:category:remove', '#', 'admin', '2023-01-27 23:57:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2094, '商品类型导出', 2089, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:category:export', '#', 'admin', '2023-01-27 23:57:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2095, '出入库单据', 2064, 1, 'orderHead', 'wms/orderHead/index', NULL, 1, 0, 'C', '0', '0', 'wms:orderHead:list', '#', 'admin', '2023-01-27 23:58:13', '', NULL, '出入库单据菜单');
INSERT INTO `sys_menu` VALUES (2096, '出入库单据查询', 2095, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:orderHead:query', '#', 'admin', '2023-01-27 23:58:13', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2097, '出入库单据新增', 2095, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:orderHead:add', '#', 'admin', '2023-01-27 23:58:13', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2098, '出入库单据修改', 2095, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:orderHead:edit', '#', 'admin', '2023-01-27 23:58:13', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2099, '出入库单据删除', 2095, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:orderHead:remove', '#', 'admin', '2023-01-27 23:58:13', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2100, '出入库单据导出', 2095, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'wms:orderHead:export', '#', 'admin', '2023-01-27 23:58:13', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2101, '财务管理', 0, 3, 'fms', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'money', 'admin', '2023-01-28 01:27:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2120, '账户信息', 2101, 1, 'account', 'fms/account/index', NULL, 1, 0, 'C', '0', '0', 'fms:account:list', '#', 'admin', '2023-01-28 01:56:15', '', NULL, '账户信息菜单');
INSERT INTO `sys_menu` VALUES (2121, '账户信息查询', 2120, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'fms:account:query', '#', 'admin', '2023-01-28 01:56:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2122, '账户信息新增', 2120, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'fms:account:add', '#', 'admin', '2023-01-28 01:56:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2123, '账户信息修改', 2120, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'fms:account:edit', '#', 'admin', '2023-01-28 01:56:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2124, '账户信息删除', 2120, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'fms:account:remove', '#', 'admin', '2023-01-28 01:56:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2125, '账户信息导出', 2120, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'fms:account:export', '#', 'admin', '2023-01-28 01:56:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2126, '账单主表', 2101, 1, 'accountHead', 'fms/accountHead/index', NULL, 1, 0, 'C', '0', '0', 'fms:accountHead:list', '#', 'admin', '2023-01-28 01:56:36', '', NULL, '账单主表菜单');
INSERT INTO `sys_menu` VALUES (2127, '账单主表查询', 2126, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'fms:accountHead:query', '#', 'admin', '2023-01-28 01:56:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2128, '账单主表新增', 2126, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'fms:accountHead:add', '#', 'admin', '2023-01-28 01:56:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2129, '账单主表修改', 2126, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'fms:accountHead:edit', '#', 'admin', '2023-01-28 01:56:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2130, '账单主表删除', 2126, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'fms:accountHead:remove', '#', 'admin', '2023-01-28 01:56:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2131, '账单主表导出', 2126, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'fms:accountHead:export', '#', 'admin', '2023-01-28 01:56:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2132, '收支项目', 2101, 1, 'item', 'fms/item/index', NULL, 1, 0, 'C', '0', '0', 'fms:item:list', '#', 'admin', '2023-01-28 01:56:44', '', NULL, '收支项目菜单');
INSERT INTO `sys_menu` VALUES (2133, '收支项目查询', 2132, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'fms:item:query', '#', 'admin', '2023-01-28 01:56:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2134, '收支项目新增', 2132, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'fms:item:add', '#', 'admin', '2023-01-28 01:56:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2135, '收支项目修改', 2132, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'fms:item:edit', '#', 'admin', '2023-01-28 01:56:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2136, '收支项目删除', 2132, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'fms:item:remove', '#', 'admin', '2023-01-28 01:56:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2137, '收支项目导出', 2132, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'fms:item:export', '#', 'admin', '2023-01-28 01:56:44', '', NULL, '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `tenant_id` int NULL DEFAULT NULL COMMENT '租户',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '通知公告表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (3, 'arg', '2', 0x3C703E666173647765637665726765663C2F703E, '0', 'admin', '2022-05-14 20:09:32', '', NULL, NULL, 1);
INSERT INTO `sys_notice` VALUES (4, '13', '1', 0x3C703EE8AFB4E6B395E7A88DE7AD89E58F91E58F91E58F91E58F91E58F91E58F91E4BB98E4BB98E4BB98E4BB98E4BB98E4BB98E4BB98E4BB98E4BB98E4BB98E4BB98E4BB98E4BB98E4BB983C2F703E, '0', 'admin', '2023-01-24 18:40:09', '', NULL, NULL, 1);
INSERT INTO `sys_notice` VALUES (5, 'fdd', '1', 0x3C703E666173646671773C2F703E, '0', 'admin', '2023-01-25 18:41:57', '', NULL, NULL, 2);

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 45 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作日志记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (1, '操作日志', 9, 'com.mn.monitor.controller.SysOperlogController.clean()', 'DELETE', 1, 'admin', NULL, '/monitor/operlog/clean', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-24 18:41:15');
INSERT INTO `sys_oper_log` VALUES (2, '定时任务', 2, 'com.mn.quartz.controller.SysJobController.changeStatus()', 'PUT', 1, 'admin', NULL, '/monitor/job/changeStatus', '127.0.0.1', '内网IP', '{\"params\":{},\"jobId\":1,\"misfirePolicy\":\"0\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-24 18:42:29');
INSERT INTO `sys_oper_log` VALUES (3, '定时任务', 2, 'com.mn.quartz.controller.SysJobController.changeStatus()', 'PUT', 1, 'admin', NULL, '/monitor/job/changeStatus', '127.0.0.1', '内网IP', '{\"params\":{},\"jobId\":1,\"misfirePolicy\":\"0\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-24 18:42:31');
INSERT INTO `sys_oper_log` VALUES (4, '定时任务', 2, 'com.mn.quartz.controller.SysJobController.changeStatus()', 'PUT', 1, 'admin', NULL, '/monitor/job/changeStatus', '127.0.0.1', '内网IP', '{\"params\":{},\"jobId\":100,\"misfirePolicy\":\"0\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-24 18:42:35');
INSERT INTO `sys_oper_log` VALUES (5, '定时任务', 2, 'com.mn.quartz.controller.SysJobController.changeStatus()', 'PUT', 1, 'admin', NULL, '/monitor/job/changeStatus', '127.0.0.1', '内网IP', '{\"params\":{},\"jobId\":100,\"misfirePolicy\":\"0\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-24 18:42:38');
INSERT INTO `sys_oper_log` VALUES (6, '菜单管理', 1, 'com.mn.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"button\",\"orderNum\":1,\"menuName\":\"测试\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"cs\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"tenantId\":1,\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-24 19:44:05');
INSERT INTO `sys_oper_log` VALUES (7, '菜单管理', 1, 'com.mn.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"download\",\"orderNum\":2,\"menuName\":\"cs1\",\"params\":{},\"parentId\":2061,\"isCache\":\"0\",\"path\":\"1\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"tenantId\":1,\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-24 19:46:00');
INSERT INTO `sys_oper_log` VALUES (8, '角色管理', 2, 'com.mn.system.controller.SysRoleController.edit()', 'PUT', 1, 'wr', NULL, '/system/role', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":3,\"admin\":false,\"remark\":\"普通管理员\",\"dataScope\":\"1\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"1\",\"deptCheckStrictly\":true,\"createTime\":1652664894000,\"updateBy\":\"wr\",\"menuCheckStrictly\":true,\"roleKey\":\"gly\",\"roleName\":\"管理员\",\"menuIds\":[2000,2020,2021,2022,2051,2023,2024,2025,2053,2054,2032,2033,2034,2035,2036,2037,2026,2027,2028,2029,2030,2031,2038,2039,2040,2041,2042,2043,1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1036,1037,1038,1039,108,500,1040,1041,1042,501,1043,1044,1045,2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-24 20:06:35');
INSERT INTO `sys_oper_log` VALUES (9, '角色管理', 2, 'com.mn.system.controller.SysRoleController.edit()', 'PUT', 1, 'wr', NULL, '/system/role', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":3,\"admin\":false,\"remark\":\"普通管理员\",\"dataScope\":\"1\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"1\",\"deptCheckStrictly\":true,\"createTime\":1652664894000,\"updateBy\":\"wr\",\"menuCheckStrictly\":true,\"roleKey\":\"gly\",\"roleName\":\"管理员\",\"menuIds\":[2000,2020,2021,2022,2051,2023,2024,2025,2053,2054,2032,2033,2034,2035,2036,2037,2026,2027,2028,2029,2030,2031,2038,2039,2040,2041,2042,2043,1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1036,1037,1038,1039,108,500,1040,1041,1042,501,1043,1044,1045],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-24 20:06:54');
INSERT INTO `sys_oper_log` VALUES (10, '菜单管理', 2, 'com.mn.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":0,\"menuName\":\"系统租户\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"tenant\",\"component\":\"system/tenant/index\",\"children\":[],\"createTime\":1674275109000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2055,\"menuType\":\"C\",\"perms\":\"system:tenant:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 10:07:42');
INSERT INTO `sys_oper_log` VALUES (11, '租户', 2, 'com.mn.system.controller.SysTenantController.edit()', 'PUT', 1, 'admin', NULL, '/system/tenant', '127.0.0.1', '内网IP', '{\"expireTime\":1675440000000,\"createTime\":1674279521000,\"userNumLimit\":111,\"name\":\"测试\",\"tenantId\":2,\"remark\":\"1111\",\"params\":{},\"enabled\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 10:08:09');
INSERT INTO `sys_oper_log` VALUES (12, '租户', 2, 'com.mn.system.controller.SysTenantController.edit()', 'PUT', 1, 'admin', NULL, '/system/tenant', '127.0.0.1', '内网IP', '{\"expireTime\":1675440000000,\"createTime\":1674279521000,\"userNumLimit\":111,\"name\":\"测试\",\"tenantId\":2,\"remark\":\"1111\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 10:08:37');
INSERT INTO `sys_oper_log` VALUES (13, '角色管理', 1, 'com.mn.system.controller.SysRoleController.add()', 'POST', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{\"flag\":false,\"admin\":false,\"params\":{},\"roleSort\":\"0\",\"deptCheckStrictly\":true,\"menuCheckStrictly\":true,\"roleKey\":\"gly\",\"roleName\":\"gly\",\"tenantId\":2,\"deptIds\":[],\"menuIds\":[2000,2020,2021,2022,2051,2023,2024,2025,2053,2054,2032,2033,2034,2035,2036,2037,2026,2027,2028,2029,2030,2031,2044,2045,2046,2047,2048,2049,2014,2015,2016,2017,2018,2019,2038,2039,2040,2041,2042,2043],\"status\":\"0\"}', NULL, 1, '\r\n### Error querying database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'where r.role_key=\'gly\' limit 1\' at line 10\r\n### The error may exist in file [D:\\work\\srcWeb\\zxtx01\\mn-system\\target\\classes\\mapper\\system\\SysRoleMapper.xml]\r\n### The error may involve com.mn.system.mapper.SysRoleMapper.checkRoleKeyUnique-Inline\r\n### The error occurred while setting parameters\r\n### SQL: select distinct r.role_id, r.role_name, r.role_key, r.role_sort, r.data_scope, r.menu_check_strictly, r.dept_check_strictly,             r.status, r.del_flag, r.create_time, r.remark         from sys_role r          left join sys_user_role ur on ur.role_id = r.role_id          left join sys_user u on u.user_id = ur.user_id          left join sys_dept d on u.dept_id = d.dept_id         where r.role_name=?       where r.role_key=? limit 1\r\n### Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'where r.role_key=\'gly\' limit 1\' at line 10\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'where r.role_key=\'gly\' limit 1\' at line 10', '2023-01-25 11:01:22');
INSERT INTO `sys_oper_log` VALUES (14, '角色管理', 1, 'com.mn.system.controller.SysRoleController.add()', 'POST', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{\"flag\":false,\"admin\":false,\"params\":{},\"roleSort\":\"0\",\"deptCheckStrictly\":true,\"menuCheckStrictly\":true,\"roleKey\":\"gly\",\"roleName\":\"gly\",\"tenantId\":2,\"deptIds\":[],\"menuIds\":[2000,2020,2021,2022,2051,2023,2024,2025,2053,2054,2032,2033,2034,2035,2036,2037,2026,2027,2028,2029,2030,2031,2044,2045,2046,2047,2048,2049,2014,2015,2016,2017,2018,2019,2038,2039,2040,2041,2042,2043],\"status\":\"0\"}', NULL, 1, '\r\n### Error querying database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'where r.role_key=\'gly\' limit 1\' at line 10\r\n### The error may exist in file [D:\\work\\srcWeb\\zxtx01\\mn-system\\target\\classes\\mapper\\system\\SysRoleMapper.xml]\r\n### The error may involve com.mn.system.mapper.SysRoleMapper.checkRoleKeyUnique-Inline\r\n### The error occurred while setting parameters\r\n### SQL: select distinct r.role_id, r.role_name, r.role_key, r.role_sort, r.data_scope, r.menu_check_strictly, r.dept_check_strictly,             r.status, r.del_flag, r.create_time, r.remark         from sys_role r          left join sys_user_role ur on ur.role_id = r.role_id          left join sys_user u on u.user_id = ur.user_id          left join sys_dept d on u.dept_id = d.dept_id         where r.role_name=?       where r.role_key=? limit 1\r\n### Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'where r.role_key=\'gly\' limit 1\' at line 10\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'where r.role_key=\'gly\' limit 1\' at line 10', '2023-01-25 11:01:53');
INSERT INTO `sys_oper_log` VALUES (15, '角色管理', 1, 'com.mn.system.controller.SysRoleController.add()', 'POST', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{\"flag\":false,\"admin\":false,\"params\":{},\"roleSort\":\"0\",\"deptCheckStrictly\":true,\"menuCheckStrictly\":true,\"roleKey\":\"gly\",\"roleName\":\"gly\",\"tenantId\":2,\"deptIds\":[],\"menuIds\":[2000,2020,2021,2022,2051,2023,2024,2025,2053,2054,2032,2033,2034,2035,2036,2037,2026,2027,2028,2029,2030,2031,2044,2045,2046,2047,2048,2049,2014,2015,2016,2017,2018,2019,2038,2039,2040,2041,2042,2043],\"status\":\"0\"}', NULL, 1, '\r\n### Error querying database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'where r.role_key=\'gly\' limit 1\' at line 10\r\n### The error may exist in file [D:\\work\\srcWeb\\zxtx01\\mn-system\\target\\classes\\mapper\\system\\SysRoleMapper.xml]\r\n### The error may involve com.mn.system.mapper.SysRoleMapper.checkRoleKeyUnique-Inline\r\n### The error occurred while setting parameters\r\n### SQL: select distinct r.role_id, r.role_name, r.role_key, r.role_sort, r.data_scope, r.menu_check_strictly, r.dept_check_strictly,             r.status, r.del_flag, r.create_time, r.remark         from sys_role r          left join sys_user_role ur on ur.role_id = r.role_id          left join sys_user u on u.user_id = ur.user_id          left join sys_dept d on u.dept_id = d.dept_id         where r.role_name=?       where r.role_key=? limit 1\r\n### Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'where r.role_key=\'gly\' limit 1\' at line 10\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'where r.role_key=\'gly\' limit 1\' at line 10', '2023-01-25 11:03:02');
INSERT INTO `sys_oper_log` VALUES (16, '角色管理', 1, 'com.mn.system.controller.SysRoleController.add()', 'POST', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{\"flag\":false,\"admin\":false,\"params\":{},\"roleSort\":\"0\",\"deptCheckStrictly\":true,\"menuCheckStrictly\":true,\"roleKey\":\"gly\",\"roleName\":\"gly\",\"tenantId\":2,\"deptIds\":[],\"menuIds\":[2000,2020,2021,2022,2051,2023,2024,2025,2053,2054,2032,2033,2034,2035,2036,2037,2026,2027,2028,2029,2030,2031,2044,2045,2046,2047,2048,2049,2014,2015,2016,2017,2018,2019,2038,2039,2040,2041,2042,2043],\"status\":\"0\"}', NULL, 1, '\r\n### Error querying database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'where r.role_key=\'gly\' limit 1\' at line 10\r\n### The error may exist in file [D:\\work\\srcWeb\\zxtx01\\mn-system\\target\\classes\\mapper\\system\\SysRoleMapper.xml]\r\n### The error may involve com.mn.system.mapper.SysRoleMapper.checkRoleKeyUnique-Inline\r\n### The error occurred while setting parameters\r\n### SQL: select distinct r.role_id, r.role_name, r.role_key, r.role_sort, r.data_scope, r.menu_check_strictly, r.dept_check_strictly,             r.status, r.del_flag, r.create_time, r.remark         from sys_role r          left join sys_user_role ur on ur.role_id = r.role_id          left join sys_user u on u.user_id = ur.user_id          left join sys_dept d on u.dept_id = d.dept_id         where r.role_name=?       where r.role_key=? limit 1\r\n### Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'where r.role_key=\'gly\' limit 1\' at line 10\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'where r.role_key=\'gly\' limit 1\' at line 10', '2023-01-25 11:03:11');
INSERT INTO `sys_oper_log` VALUES (17, '角色管理', 1, 'com.mn.system.controller.SysRoleController.add()', 'POST', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":103,\"admin\":false,\"params\":{},\"roleSort\":\"0\",\"deptCheckStrictly\":true,\"createBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"gly\",\"roleName\":\"gly\",\"tenantId\":2,\"deptIds\":[],\"menuIds\":[2000,2020,2021,2022,2051,2023,2024,2025,2053,2054,2032,2033,2034,2035,2036,2037,2026,2027,2028,2029,2030,2031,2044,2045,2046,2047,2048,2049,2014,2015,2016,2017,2018,2019,2038,2039,2040,2041,2042,2043],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 11:04:42');
INSERT INTO `sys_oper_log` VALUES (18, '角色管理', 1, 'com.mn.system.controller.SysRoleController.add()', 'POST', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{\"flag\":false,\"admin\":false,\"params\":{},\"roleSort\":\"0\",\"deptCheckStrictly\":true,\"menuCheckStrictly\":true,\"roleKey\":\"gly\",\"roleName\":\"管理员\",\"tenantId\":2,\"deptIds\":[],\"menuIds\":[2000,2020,2021,2022,2051,2023,2024,2025,2053,2054,2032,2033,2034,2035,2036,2037,2026,2027,2028,2029,2030,2031,2044,2045,2046,2047,2048,2049,2014,2015,2016,2017,2018,2019,2038,2039,2040,2041,2042,2043,2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,3,114,115,1055,1056,1058,1057,1059,1060,116],\"status\":\"0\"}', '{\"msg\":\"新增角色\'管理员\'失败，角色权限已存在\",\"code\":500}', 0, NULL, '2023-01-25 11:05:02');
INSERT INTO `sys_oper_log` VALUES (19, '个人信息', 2, 'com.mn.system.controller.SysProfileController.updateProfile()', 'PUT', 1, 'admin', NULL, '/system/user/profile', '127.0.0.1', '内网IP', '{\"roles\":[{\"flag\":false,\"roleId\":1,\"admin\":true,\"dataScope\":\"1\",\"params\":{},\"roleSort\":\"0\",\"deptCheckStrictly\":false,\"menuCheckStrictly\":false,\"roleKey\":\"admin\",\"roleName\":\"超级管理员\",\"status\":\"0\"}],\"admin\":true,\"loginDate\":1674631197000,\"remark\":\"管理员\",\"delFlag\":\"0\",\"loginIp\":\"127.0.0.1\",\"email\":\"mn@163.com\",\"nickName\":\"曼宁\",\"sex\":\"0\",\"deptId\":\"00010001\",\"mobile\":\"15888888888\",\"avatar\":\"\",\"dept\":{\"deptName\":\"市场部\",\"deptId\":\"00010001\",\"orderNum\":\"2\",\"params\":{},\"parentId\":\"0001\",\"children\":[],\"status\":\"0\"},\"params\":{},\"userName\":\"admin\",\"userId\":1,\"createBy\":\"admin\",\"createTime\":1648974200000,\"tenantId\":1,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 15:45:49');
INSERT INTO `sys_oper_log` VALUES (20, '用户管理', 2, 'com.mn.system.controller.SysUserController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/user/changeStatus', '192.168.1.27', '内网IP', '{\"admin\":false,\"updateBy\":\"admin\",\"params\":{},\"userId\":105,\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 15:48:58');
INSERT INTO `sys_oper_log` VALUES (21, '用户管理', 2, 'com.mn.system.controller.SysUserController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/user/changeStatus', '192.168.1.27', '内网IP', '{\"admin\":false,\"updateBy\":\"admin\",\"params\":{},\"userId\":105,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 15:49:03');
INSERT INTO `sys_oper_log` VALUES (22, '个人信息', 2, 'com.mn.system.controller.SysProfileController.updateProfile()', 'PUT', 1, 'admin', NULL, '/system/user/profile', '127.0.0.1', '内网IP', '{\"roles\":[{\"flag\":false,\"roleId\":1,\"admin\":true,\"dataScope\":\"1\",\"params\":{},\"roleSort\":\"0\",\"deptCheckStrictly\":false,\"menuCheckStrictly\":false,\"roleKey\":\"admin\",\"roleName\":\"超级管理员\",\"status\":\"0\"}],\"admin\":true,\"loginDate\":1674631197000,\"remark\":\"管理员\",\"delFlag\":\"0\",\"loginIp\":\"127.0.0.1\",\"email\":\"mn@163.com\",\"nickName\":\"超管\",\"sex\":\"0\",\"deptId\":\"00010001\",\"mobile\":\"15888888888\",\"avatar\":\"\",\"dept\":{\"deptName\":\"市场部\",\"deptId\":\"00010001\",\"orderNum\":\"2\",\"params\":{},\"parentId\":\"0001\",\"children\":[],\"status\":\"0\"},\"params\":{},\"userName\":\"admin\",\"userId\":1,\"createBy\":\"admin\",\"createTime\":1648974200000,\"tenantId\":1,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 15:58:01');
INSERT INTO `sys_oper_log` VALUES (23, '岗位管理', 1, 'com.mn.system.controller.SysPostController.add()', 'POST', 1, 'admin', NULL, '/system/post', '127.0.0.1', '内网IP', '{\"postSort\":\"0\",\"flag\":false,\"remark\":\"7\",\"postId\":8,\"params\":{},\"createBy\":\"admin\",\"postName\":\"7\",\"tenantId\":2,\"postCode\":\"7\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 16:51:14');
INSERT INTO `sys_oper_log` VALUES (24, '岗位管理', 1, 'com.mn.system.controller.SysPostController.add()', 'POST', 1, 'admin', NULL, '/system/post', '127.0.0.1', '内网IP', '{\"postSort\":\"0\",\"flag\":false,\"remark\":\"7\",\"params\":{},\"postName\":\"7\",\"tenantId\":2,\"postCode\":\"7\",\"status\":\"0\"}', '{\"msg\":\"新增岗位\'7\'失败，岗位名称已存在\",\"code\":500}', 0, NULL, '2023-01-25 16:51:26');
INSERT INTO `sys_oper_log` VALUES (25, '岗位管理', 1, 'com.mn.system.controller.SysPostController.add()', 'POST', 1, 'admin', NULL, '/system/post', '127.0.0.1', '内网IP', '{\"postSort\":\"0\",\"flag\":false,\"remark\":\"ceo\",\"postId\":9,\"params\":{},\"createBy\":\"admin\",\"postName\":\"ceo\",\"tenantId\":2,\"postCode\":\"ceo\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 16:51:50');
INSERT INTO `sys_oper_log` VALUES (26, '通知公告', 1, 'com.mn.system.controller.SysNoticeController.add()', 'POST', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{\"noticeType\":\"1\",\"params\":{},\"noticeTitle\":\"fdd\",\"noticeContent\":\"<p>fasdfqw</p>\",\"createBy\":\"admin\",\"tenantId\":2,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 18:41:57');
INSERT INTO `sys_oper_log` VALUES (27, '参数管理', 9, 'com.mn.system.controller.SysConfigController.refreshCache()', 'DELETE', 1, 'admin', NULL, '/system/config/refreshCache', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 18:42:29');
INSERT INTO `sys_oper_log` VALUES (28, '代码生成', 6, 'com.mn.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'wms_supplier', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 20:25:06');
INSERT INTO `sys_oper_log` VALUES (29, '字典类型', 1, 'com.mn.system.controller.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"dictSysType\":\"0\",\"remark\":\"仓储模块-供应商类别\",\"params\":{},\"dictType\":\"wms_supplier_type\",\"createBy\":\"admin\",\"dictName\":\"供应商类别\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 20:45:33');
INSERT INTO `sys_oper_log` VALUES (30, '代码生成', 2, 'com.mn.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/wms_supplier', '127.0.0.1', '内网IP', '{tableName=wms_supplier}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 21:00:32');
INSERT INTO `sys_oper_log` VALUES (31, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":433,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674651631000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674649505000,\"tableId\":37,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Type\",\"usableColumn\":false,\"columnId\":434,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"wms_supplier_type\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"type\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"类型\",\"isQuery\":\"1\",\"updateTime\":1674651631000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674649505000,\"isEdit\":\"1\",\"tableId\":37,\"pk\":false,\"columnName\":\"type\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":435,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"名称\",\"isQuery\":\"1\",\"updateTime\":1674651631000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674649505000,\"isEdit\":\"1\",\"tableId\":37,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Contacts\",\"usableColumn\":false,\"columnId\":436,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"contacts\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"联系人\",\"updateTime\":1674651631000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(100)\",\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 21:04:27');
INSERT INTO `sys_oper_log` VALUES (32, '代码生成', 2, 'com.mn.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/wms_supplier', '127.0.0.1', '内网IP', '{tableName=wms_supplier}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 21:04:33');
INSERT INTO `sys_oper_log` VALUES (33, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":433,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674651873000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674649505000,\"tableId\":37,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Type\",\"usableColumn\":false,\"columnId\":434,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"wms_supplier_type\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"type\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"类型\",\"isQuery\":\"1\",\"updateTime\":1674651873000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674649505000,\"isEdit\":\"1\",\"tableId\":37,\"pk\":false,\"columnName\":\"type\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":435,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"名称\",\"isQuery\":\"1\",\"updateTime\":1674651873000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674649505000,\"isEdit\":\"1\",\"tableId\":37,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Contacts\",\"usableColumn\":false,\"columnId\":436,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"contacts\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"联系人\",\"updateTime\":1674651873000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(100)\",\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 21:07:22');
INSERT INTO `sys_oper_log` VALUES (34, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":433,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674652042000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674649505000,\"tableId\":37,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Type\",\"usableColumn\":false,\"columnId\":434,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"wms_supplier_type\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"type\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"类型\",\"isQuery\":\"1\",\"updateTime\":1674652042000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674649505000,\"isEdit\":\"1\",\"tableId\":37,\"pk\":false,\"columnName\":\"type\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":435,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"名称\",\"isQuery\":\"1\",\"updateTime\":1674652042000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674649505000,\"isEdit\":\"1\",\"tableId\":37,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Contacts\",\"usableColumn\":false,\"columnId\":436,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"contacts\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"联系人\",\"updateTime\":1674652042000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(100)\",\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 21:08:57');
INSERT INTO `sys_oper_log` VALUES (35, '菜单管理', 1, 'com.mn.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"textarea\",\"orderNum\":1,\"menuName\":\"仓储管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"wms\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"tenantId\":2,\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 21:11:01');
INSERT INTO `sys_oper_log` VALUES (36, '菜单管理', 2, 'com.mn.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"tool\",\"orderNum\":90,\"menuName\":\"系统工具\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"tool\",\"children\":[],\"createTime\":1648974201000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":3,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 21:11:29');
INSERT INTO `sys_oper_log` VALUES (37, '菜单管理', 2, 'com.mn.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"system\",\"orderNum\":9,\"menuName\":\"系统管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"system\",\"children\":[],\"createTime\":1648974201000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":1,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 21:11:56');
INSERT INTO `sys_oper_log` VALUES (38, '菜单管理', 2, 'com.mn.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"tool\",\"orderNum\":9,\"menuName\":\"系统工具\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"tool\",\"children\":[],\"createTime\":1648974201000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":3,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 21:12:03');
INSERT INTO `sys_oper_log` VALUES (39, '菜单管理', 2, 'com.mn.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"monitor\",\"orderNum\":9,\"menuName\":\"系统监控\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"monitor\",\"children\":[],\"createTime\":1648974201000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 21:12:14');
INSERT INTO `sys_oper_log` VALUES (40, '菜单管理', 2, 'com.mn.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"query\":\"\",\"icon\":\"guide\",\"orderNum\":4,\"menuName\":\"公司官网\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"https://www.baidu.com/\",\"children\":[],\"createTime\":1648974201000,\"updateBy\":\"admin\",\"isFrame\":\"0\",\"menuId\":4,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 21:12:28');
INSERT INTO `sys_oper_log` VALUES (41, '菜单管理', 2, 'com.mn.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"query\":\"\",\"icon\":\"guide\",\"orderNum\":100,\"menuName\":\"公司官网\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"https://www.baidu.com/\",\"children\":[],\"createTime\":1648974201000,\"updateBy\":\"admin\",\"isFrame\":\"0\",\"menuId\":4,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 21:12:36');
INSERT INTO `sys_oper_log` VALUES (42, '菜单管理', 2, 'com.mn.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"peoples\",\"orderNum\":1,\"menuName\":\"客户管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"crm\",\"children\":[],\"createTime\":1648982070000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2000,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 21:12:56');
INSERT INTO `sys_oper_log` VALUES (43, '菜单管理', 2, 'com.mn.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"textarea\",\"orderNum\":2,\"menuName\":\"仓储管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"wms\",\"children\":[],\"createTime\":1674652261000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2064,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 21:13:02');
INSERT INTO `sys_oper_log` VALUES (44, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"wr\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":433,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674652136000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674649505000,\"tableId\":37,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Type\",\"usableColumn\":false,\"columnId\":434,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"wms_supplier_type\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"type\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"类型\",\"isQuery\":\"1\",\"updateTime\":1674652136000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674649505000,\"isEdit\":\"1\",\"tableId\":37,\"pk\":false,\"columnName\":\"type\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":435,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"名称\",\"isQuery\":\"1\",\"updateTime\":1674652136000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674649505000,\"isEdit\":\"1\",\"tableId\":37,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Contacts\",\"usableColumn\":false,\"columnId\":436,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"contacts\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"联系人\",\"updateTime\":1674652136000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(100)\",\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-25 21:14:08');
INSERT INTO `sys_oper_log` VALUES (45, '代码生成', 8, 'com.mn.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', NULL, 0, NULL, '2023-01-25 21:14:14');
INSERT INTO `sys_oper_log` VALUES (46, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"wr\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":433,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674652448000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674649505000,\"tableId\":37,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Type\",\"usableColumn\":false,\"columnId\":434,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"wms_supplier_type\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"type\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"类型\",\"isQuery\":\"1\",\"updateTime\":1674652448000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674649505000,\"isEdit\":\"1\",\"tableId\":37,\"pk\":false,\"columnName\":\"type\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":435,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"名称\",\"isQuery\":\"1\",\"updateTime\":1674652448000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674649505000,\"isEdit\":\"1\",\"tableId\":37,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Contacts\",\"usableColumn\":false,\"columnId\":436,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"contacts\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"联系人\",\"updateTime\":1674652448000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(100)\",\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 16:47:10');
INSERT INTO `sys_oper_log` VALUES (47, '代码生成', 8, 'com.mn.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', NULL, 0, NULL, '2023-01-27 16:47:26');
INSERT INTO `sys_oper_log` VALUES (48, '字典类型', 2, 'com.mn.system.controller.SysDictTypeController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"dataSql\":\"0\",\"dictSysType\":\"0\",\"remark\":\"仓储模块-供应商类别\",\"dictId\":113,\"params\":{},\"dictType\":\"wms_supplier_type\",\"createBy\":\"admin\",\"createTime\":1674650733000,\"updateBy\":\"admin\",\"dictName\":\"供应商类别\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 16:55:16');
INSERT INTO `sys_oper_log` VALUES (49, '字典类型', 2, 'com.mn.system.controller.SysDictTypeController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"dataSql\":\"\",\"dictSysType\":\"0\",\"remark\":\"仓储模块-供应商类别\",\"dictId\":113,\"params\":{},\"dictType\":\"wms_supplier_type\",\"createBy\":\"admin\",\"createTime\":1674650733000,\"updateBy\":\"admin\",\"dictName\":\"供应商类别\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 16:55:27');
INSERT INTO `sys_oper_log` VALUES (50, '字典类型', 9, 'com.mn.system.controller.SysDictTypeController.refreshCache()', 'DELETE', 1, 'admin', NULL, '/system/dict/type/refreshCache', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 16:55:35');
INSERT INTO `sys_oper_log` VALUES (51, '字典类型', 2, 'com.mn.system.controller.SysDictTypeController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"dataSql\":\"\",\"dictSysType\":\"0\",\"remark\":\"仓储模块-供应商类别\",\"dictId\":113,\"params\":{},\"dictType\":\"wms_supplier_type\",\"createBy\":\"admin\",\"createTime\":1674650733000,\"updateBy\":\"admin\",\"dictName\":\"供应商类别\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 16:55:47');
INSERT INTO `sys_oper_log` VALUES (52, '字典类型', 2, 'com.mn.system.controller.SysDictTypeController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"dataSql\":\"\",\"dictSysType\":\"0\",\"remark\":\"仓储模块-供应商类别\",\"dictId\":113,\"params\":{},\"dictType\":\"wms_supplier_type\",\"createBy\":\"admin\",\"createTime\":1674650733000,\"updateBy\":\"admin\",\"dictName\":\"供应商类别\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 16:56:13');
INSERT INTO `sys_oper_log` VALUES (53, '字典类型', 2, 'com.mn.system.controller.SysDictTypeController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"dataSql\":\"\",\"dictSysType\":\"0\",\"remark\":\"仓储模块-供应商类别\",\"dictId\":113,\"params\":{},\"dictType\":\"wms_supplier_type\",\"createBy\":\"admin\",\"createTime\":1674650733000,\"updateBy\":\"admin\",\"dictName\":\"供应商类别\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 17:01:06');
INSERT INTO `sys_oper_log` VALUES (54, '字典数据', 1, 'com.mn.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"1\",\"listClass\":\"default\",\"dictSort\":1,\"params\":{},\"dictType\":\"wms_supplier_type\",\"dictLabel\":\"类别1\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 17:01:44');
INSERT INTO `sys_oper_log` VALUES (55, '供应商信息', 2, 'com.mn.wms.controller.SupplierController.edit()', 'PUT', 1, 'admin', NULL, '/wms/supplier', '127.0.0.1', '内网IP', '{\"advanceIn\":0,\"bankName\":\"\",\"remark\":\"\",\"delFlag\":\"0\",\"type\":\"供应商\",\"enabled\":\"Y\",\"allNeedGet\":0,\"beginNeedPay\":0,\"id\":57,\"taxNum\":\"\",\"fax\":\"\",\"email\":\"\",\"address\":\"地址1\",\"mobile\":\"12345678\",\"updateTime\":1674810155924,\"accountNumber\":\"\",\"params\":{},\"beginNeedGet\":0,\"taxRate\":12,\"name\":\"供应商1\",\"tenantId\":63,\"allNeedPay\":4,\"contacts\":\"小军\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 17:02:35');
INSERT INTO `sys_oper_log` VALUES (56, '供应商信息', 2, 'com.mn.wms.controller.SupplierController.edit()', 'PUT', 1, 'admin', NULL, '/wms/supplier', '127.0.0.1', '内网IP', '{\"advanceIn\":0,\"bankName\":\"\",\"remark\":\"\",\"delFlag\":\"0\",\"type\":\"1\",\"enabled\":\"Y\",\"allNeedGet\":0,\"beginNeedPay\":0,\"id\":57,\"taxNum\":\"\",\"fax\":\"\",\"email\":\"\",\"address\":\"地址1\",\"mobile\":\"12345678\",\"updateTime\":1674810641221,\"accountNumber\":\"\",\"params\":{},\"beginNeedGet\":0,\"taxRate\":12,\"name\":\"供应商1\",\"tenantId\":63,\"allNeedPay\":4,\"contacts\":\"小军\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 17:10:41');
INSERT INTO `sys_oper_log` VALUES (57, '供应商信息', 2, 'com.mn.wms.controller.SupplierController.edit()', 'PUT', 1, 'admin', NULL, '/wms/supplier', '127.0.0.1', '内网IP', '{\"advanceIn\":0,\"bankName\":\"\",\"remark\":\"\",\"delFlag\":\"0\",\"type\":\"1\",\"enabled\":\"1\",\"allNeedGet\":-100,\"beginNeedPay\":0,\"id\":58,\"taxNum\":\"\",\"fax\":\"\",\"email\":\"\",\"address\":\"\",\"mobile\":\"12345678\",\"updateTime\":1674810653287,\"accountNumber\":\"\",\"params\":{},\"beginNeedGet\":0,\"taxRate\":12,\"name\":\"客户1\",\"tenantId\":63,\"contacts\":\"小李\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 17:10:53');
INSERT INTO `sys_oper_log` VALUES (58, '代码生成', 6, 'com.mn.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '192.168.1.17', '内网IP', 'wms_warehouse', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 17:49:51');
INSERT INTO `sys_oper_log` VALUES (59, '代码生成', 2, 'com.mn.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/wms_warehouse', '192.168.1.17', '内网IP', '{tableName=wms_warehouse}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 17:58:18');
INSERT INTO `sys_oper_log` VALUES (60, '字典类型', 1, 'com.mn.system.controller.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '192.168.1.17', '内网IP', '{\"dictSysType\":\"0\",\"remark\":\"仓储模块，仓库类别\",\"params\":{},\"dictType\":\"wms_warehouse_type\",\"createBy\":\"admin\",\"dictName\":\"仓库类别\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 17:59:26');
INSERT INTO `sys_oper_log` VALUES (61, '字典数据', 1, 'com.mn.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '192.168.1.17', '内网IP', '{\"dictValue\":\"1\",\"listClass\":\"default\",\"dictSort\":1,\"params\":{},\"dictType\":\"wms_warehouse_type\",\"dictLabel\":\"类别1\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 18:02:51');
INSERT INTO `sys_oper_log` VALUES (62, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '192.168.1.17', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":460,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674813498000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674812990000,\"tableId\":38,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":461,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"仓库名称\",\"isQuery\":\"1\",\"updateTime\":1674813498000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674812990000,\"isEdit\":\"1\",\"tableId\":38,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Address\",\"usableColumn\":false,\"columnId\":462,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"address\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"仓库地址\",\"updateTime\":1674813498000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674812990000,\"isEdit\":\"1\",\"tableId\":38,\"pk\":false,\"columnName\":\"address\"},{\"capJavaField\":\"Cost\",\"usableColumn\":false,\"columnId\":473,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"cost\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"仓储费\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"BigDecimal\",\"queryType\":\"EQ\",\"columnType\":\"decimal(16,2)\",\"createBy\":\"\",\"isPk\":\"0\",\"createTime', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 18:05:41');
INSERT INTO `sys_oper_log` VALUES (63, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '192.168.1.17', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":460,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674813941000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674812990000,\"tableId\":38,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":461,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"仓库名称\",\"isQuery\":\"1\",\"updateTime\":1674813941000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674812990000,\"isEdit\":\"1\",\"tableId\":38,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Address\",\"usableColumn\":false,\"columnId\":462,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"address\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"仓库地址\",\"updateTime\":1674813941000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674812990000,\"isEdit\":\"1\",\"tableId\":38,\"pk\":false,\"columnName\":\"address\"},{\"capJavaField\":\"Cost\",\"usableColumn\":false,\"columnId\":473,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"cost\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"仓储费\",\"updateTime\":1674813941000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"BigDecimal\",\"queryType\":\"EQ\",\"columnType\":\"decimal(16,2)\",\"createBy', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 18:06:53');
INSERT INTO `sys_oper_log` VALUES (64, '代码生成', 2, 'com.mn.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/wms_warehouse', '192.168.1.17', '内网IP', '{tableName=wms_warehouse}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 18:19:16');
INSERT INTO `sys_oper_log` VALUES (65, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '192.168.1.17', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":460,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674814756000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674812990000,\"tableId\":38,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":461,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"仓库名称\",\"isQuery\":\"1\",\"updateTime\":1674814756000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674812990000,\"isEdit\":\"1\",\"tableId\":38,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Address\",\"usableColumn\":false,\"columnId\":462,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"address\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"仓库地址\",\"isQuery\":\"1\",\"updateTime\":1674814756000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674812990000,\"isEdit\":\"1\",\"tableId\":38,\"pk\":false,\"columnName\":\"address\"},{\"capJavaField\":\"Cost\",\"usableColumn\":false,\"columnId\":473,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"cost\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"仓储费\",\"updateTime\":1674814756000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"BigDecimal\",\"queryType\":\"EQ\",\"columnType\":\"decimal(16,2)\",\"createBy\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 18:21:21');
INSERT INTO `sys_oper_log` VALUES (66, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '192.168.1.17', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":460,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674814881000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674812990000,\"tableId\":38,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":461,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"仓库名称\",\"isQuery\":\"1\",\"updateTime\":1674814881000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674812990000,\"isEdit\":\"1\",\"tableId\":38,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Address\",\"usableColumn\":false,\"columnId\":462,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"address\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"仓库地址\",\"isQuery\":\"1\",\"updateTime\":1674814881000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674812990000,\"isEdit\":\"1\",\"tableId\":38,\"pk\":false,\"columnName\":\"address\"},{\"capJavaField\":\"Cost\",\"usableColumn\":false,\"columnId\":473,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"cost\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"仓储费\",\"updateTime\":1674814881000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"BigDecimal\",\"queryType\":\"EQ\",\"columnType\":\"decimal(16,2)\",\"createBy\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 18:25:51');
INSERT INTO `sys_oper_log` VALUES (67, '代码生成', 8, 'com.mn.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '192.168.1.17', '内网IP', '{}', NULL, 0, NULL, '2023-01-27 18:40:00');
INSERT INTO `sys_oper_log` VALUES (68, '仓库信息', 2, 'com.mn.wms.controller.WarehouseController.edit()', 'PUT', 1, 'admin', NULL, '/wms/warehouse', '127.0.0.1', '内网IP', '{\"address\":\"dizhi\",\"cost\":12,\"remark\":\"描述\",\"updateTime\":1674817326611,\"sort\":\"1\",\"delFlag\":\"0\",\"params\":{},\"type\":\"1\",\"userId\":131,\"enabled\":\"0\",\"isDefault\":\"1\",\"name\":\"仓库1\",\"tenantId\":63,\"truckage\":12,\"id\":14}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 19:02:06');
INSERT INTO `sys_oper_log` VALUES (69, '菜单管理', 2, 'com.mn.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"peoples\",\"orderNum\":1,\"menuName\":\"供应商信息\",\"params\":{},\"parentId\":2064,\"isCache\":\"0\",\"path\":\"supplier\",\"component\":\"wms/supplier/index\",\"children\":[],\"createTime\":1674809517000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2065,\"menuType\":\"C\",\"perms\":\"wms:supplier:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 19:46:07');
INSERT INTO `sys_oper_log` VALUES (70, '菜单管理', 2, 'com.mn.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"form\",\"orderNum\":1,\"menuName\":\"仓库信息\",\"params\":{},\"parentId\":2064,\"isCache\":\"0\",\"path\":\"warehouse\",\"component\":\"wms/warehouse/index\",\"children\":[],\"createTime\":1674816461000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2071,\"menuType\":\"C\",\"perms\":\"wms:warehouse:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 19:47:32');
INSERT INTO `sys_oper_log` VALUES (71, '代码生成', 6, 'com.mn.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'wms_material', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 20:48:50');
INSERT INTO `sys_oper_log` VALUES (72, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":476,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674823730000,\"tableId\":39,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"CategoryId\",\"usableColumn\":false,\"columnId\":477,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"categoryId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"产品类型id\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674823730000,\"isEdit\":\"1\",\"tableId\":39,\"pk\":false,\"columnName\":\"category_id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":478,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"名称\",\"isQuery\":\"1\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(100)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674823730000,\"isEdit\":\"1\",\"tableId\":39,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Mfrs\",\"usableColumn\":false,\"columnId\":479,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"mfrs\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"制造商\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674823', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 21:08:36');
INSERT INTO `sys_oper_log` VALUES (73, '代码生成', 6, 'com.mn.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'wms_material_category,wms_material_current_stock', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 21:13:57');
INSERT INTO `sys_oper_log` VALUES (74, '代码生成', 3, 'com.mn.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/32,33,34,35', '127.0.0.1', '内网IP', '{tableIds=32,33,34,35}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 21:14:09');
INSERT INTO `sys_oper_log` VALUES (75, '代码生成', 3, 'com.mn.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/36', '127.0.0.1', '内网IP', '{tableIds=36}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 21:14:14');
INSERT INTO `sys_oper_log` VALUES (76, '部门管理', 1, 'com.mn.system.controller.SysDeptController.add()', 'POST', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{\"deptName\":\"12\",\"leader\":\"12\",\"orderNum\":\"12\",\"params\":{},\"parentId\":1,\"createBy\":\"admin\",\"children\":[],\"ancestors\":\"null,1\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 22:38:09');
INSERT INTO `sys_oper_log` VALUES (77, '部门管理', 1, 'com.mn.system.controller.SysDeptController.add()', 'POST', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{\"deptName\":\"13\",\"orderNum\":\"13\",\"params\":{},\"parentId\":2,\"createBy\":\"admin\",\"children\":[],\"ancestors\":\"null,1,2\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 22:38:16');
INSERT INTO `sys_oper_log` VALUES (78, '部门管理', 1, 'com.mn.system.controller.SysDeptController.add()', 'POST', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{\"deptName\":\"12\",\"orderNum\":\"12\",\"params\":{},\"parentId\":1,\"createBy\":\"admin\",\"children\":[],\"ancestors\":\"0,1\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 22:39:25');
INSERT INTO `sys_oper_log` VALUES (79, '部门管理', 1, 'com.mn.system.controller.SysDeptController.add()', 'POST', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{\"deptName\":\"13\",\"orderNum\":\"13\",\"params\":{},\"parentId\":4,\"createBy\":\"admin\",\"children\":[],\"tenantId\":2,\"ancestors\":\"0,1,4\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 22:56:56');
INSERT INTO `sys_oper_log` VALUES (80, '代码生成', 6, 'com.mn.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'wms_material_extend', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 22:57:47');
INSERT INTO `sys_oper_log` VALUES (81, '代码生成', 6, 'com.mn.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'wms_order_head', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 22:58:40');
INSERT INTO `sys_oper_log` VALUES (82, '代码生成', 6, 'com.mn.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'wms_order_item', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 22:59:00');
INSERT INTO `sys_oper_log` VALUES (83, '代码生成', 2, 'com.mn.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/wms_supplier', '127.0.0.1', '内网IP', '{tableName=wms_supplier}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:16:49');
INSERT INTO `sys_oper_log` VALUES (84, '代码生成', 2, 'com.mn.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/wms_warehouse', '127.0.0.1', '内网IP', '{tableName=wms_warehouse}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:16:55');
INSERT INTO `sys_oper_log` VALUES (85, '代码生成', 2, 'com.mn.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/wms_material', '127.0.0.1', '内网IP', '{tableName=wms_material}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:16:58');
INSERT INTO `sys_oper_log` VALUES (86, '代码生成', 2, 'com.mn.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/wms_material_category', '127.0.0.1', '内网IP', '{tableName=wms_material_category}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:17:00');
INSERT INTO `sys_oper_log` VALUES (87, '代码生成', 2, 'com.mn.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/wms_material_current_stock', '127.0.0.1', '内网IP', '{tableName=wms_material_current_stock}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:17:03');
INSERT INTO `sys_oper_log` VALUES (88, '代码生成', 2, 'com.mn.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/wms_material_extend', '127.0.0.1', '内网IP', '{tableName=wms_material_extend}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:17:05');
INSERT INTO `sys_oper_log` VALUES (89, '代码生成', 2, 'com.mn.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/wms_order_head', '127.0.0.1', '内网IP', '{tableName=wms_order_head}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:17:07');
INSERT INTO `sys_oper_log` VALUES (90, '代码生成', 2, 'com.mn.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/wms_order_item', '127.0.0.1', '内网IP', '{tableName=wms_order_item}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:17:11');
INSERT INTO `sys_oper_log` VALUES (91, '代码生成', 6, 'com.mn.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'wms_order_type', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:25:13');
INSERT INTO `sys_oper_log` VALUES (92, '代码生成', 2, 'com.mn.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/wms_order_type', '127.0.0.1', '内网IP', '{tableName=wms_order_type}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:26:03');
INSERT INTO `sys_oper_log` VALUES (93, '代码生成', 3, 'com.mn.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/45', '127.0.0.1', '内网IP', '{tableIds=45}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:26:12');
INSERT INTO `sys_oper_log` VALUES (94, '代码生成', 6, 'com.mn.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'wms_order_type', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:26:17');
INSERT INTO `sys_oper_log` VALUES (95, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"wr\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":433,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674832609000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674649505000,\"tableId\":37,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Type\",\"usableColumn\":false,\"columnId\":434,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"type\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"类型\",\"updateTime\":1674832609000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674649505000,\"isEdit\":\"1\",\"tableId\":37,\"pk\":false,\"columnName\":\"type\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":435,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"名称\",\"isQuery\":\"1\",\"updateTime\":1674832609000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674649505000,\"isEdit\":\"1\",\"tableId\":37,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Contacts\",\"usableColumn\":false,\"columnId\":436,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"contacts\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"联系人\",\"updateTime\":1674832609000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(100)\",\"createBy\":\"admi', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:29:35');
INSERT INTO `sys_oper_log` VALUES (96, '代码生成', 3, 'com.mn.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/37', '127.0.0.1', '内网IP', '{tableIds=37}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:29:47');
INSERT INTO `sys_oper_log` VALUES (97, '代码生成', 3, 'com.mn.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/38', '127.0.0.1', '内网IP', '{tableIds=38}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:29:56');
INSERT INTO `sys_oper_log` VALUES (98, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":476,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674832618000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674823730000,\"tableId\":39,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"CategoryId\",\"usableColumn\":false,\"columnId\":477,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"categoryId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"产品类型id\",\"isQuery\":\"1\",\"updateTime\":1674832618000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674823730000,\"isEdit\":\"1\",\"tableId\":39,\"pk\":false,\"columnName\":\"category_id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":478,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"名称\",\"isQuery\":\"1\",\"updateTime\":1674832618000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(100)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674823730000,\"isEdit\":\"1\",\"tableId\":39,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Mfrs\",\"usableColumn\":false,\"columnId\":479,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"mfrs\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"制造商\",\"updateTime\":1674832618000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(50)\",\"createBy\":\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:31:05');
INSERT INTO `sys_oper_log` VALUES (99, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":476,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674833465000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674823730000,\"tableId\":39,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"CategoryId\",\"usableColumn\":false,\"columnId\":477,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"categoryId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"产品类型id\",\"isQuery\":\"1\",\"updateTime\":1674833465000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674823730000,\"isEdit\":\"1\",\"tableId\":39,\"pk\":false,\"columnName\":\"category_id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":478,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"名称\",\"isQuery\":\"1\",\"updateTime\":1674833465000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(100)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674823730000,\"isEdit\":\"1\",\"tableId\":39,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Mfrs\",\"usableColumn\":false,\"columnId\":479,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"mfrs\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"制造商\",\"updateTime\":1674833465000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(50)\",\"createBy\":\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:32:33');
INSERT INTO `sys_oper_log` VALUES (100, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":495,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674832620000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674825237000,\"tableId\":40,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":496,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"名称\",\"isQuery\":\"1\",\"updateTime\":1674832620000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674825237000,\"isEdit\":\"1\",\"tableId\":40,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Ancestors\",\"usableColumn\":false,\"columnId\":582,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"ancestors\",\"edit\":false,\"query\":false,\"columnComment\":\"祖级列表\",\"sort\":3,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varbinary(128)\",\"createBy\":\"\",\"isPk\":\"0\",\"createTime\":1674832620000,\"tableId\":40,\"pk\":false,\"columnName\":\"ancestors\"},{\"capJavaField\":\"ParentId\",\"usableColumn\":true,\"columnId\":498,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"isInsert\":\"1\",\"javaField\":\"parentId\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"父类\",\"updateTime\":1674832620000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674825237000,\"isEdit\":\"1\",\"tableId\":40,\"pk\":false,\"columnN', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:36:17');
INSERT INTO `sys_oper_log` VALUES (101, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":506,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674832623000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674825237000,\"tableId\":41,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"WarehouseId\",\"usableColumn\":false,\"columnId\":585,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"warehouseId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"仓库id\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"\",\"isPk\":\"0\",\"createTime\":1674832623000,\"isEdit\":\"1\",\"tableId\":41,\"pk\":false,\"columnName\":\"warehouse_id\"},{\"capJavaField\":\"MaterialId\",\"usableColumn\":false,\"columnId\":507,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"materialId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"产品id\",\"isQuery\":\"1\",\"updateTime\":1674832623000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674825237000,\"isEdit\":\"1\",\"tableId\":41,\"pk\":false,\"columnName\":\"material_id\"},{\"capJavaField\":\"MaterialExtendId\",\"usableColumn\":false,\"columnId\":586,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"materialExtendId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"产品subid\",\"isQuery\":\"1\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:39:12');
INSERT INTO `sys_oper_log` VALUES (102, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":512,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674832625000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674831467000,\"tableId\":42,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"MaterialId\",\"usableColumn\":false,\"columnId\":513,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"materialId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"商品id\",\"isQuery\":\"1\",\"updateTime\":1674832625000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674831467000,\"isEdit\":\"1\",\"tableId\":42,\"pk\":false,\"columnName\":\"material_id\"},{\"capJavaField\":\"BarCode\",\"usableColumn\":false,\"columnId\":514,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"barCode\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"商品条码\",\"updateTime\":1674832625000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674831467000,\"isEdit\":\"1\",\"tableId\":42,\"pk\":false,\"columnName\":\"bar_code\"},{\"capJavaField\":\"CommodityUnit\",\"usableColumn\":false,\"columnId\":515,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"commodityUnit\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"商品单位\",\"updateTime\":1674832625000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(50)\",\"createBy\":\"adm', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:41:58');
INSERT INTO `sys_oper_log` VALUES (103, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":506,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674833952000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674825237000,\"tableId\":41,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"WarehouseId\",\"usableColumn\":false,\"columnId\":585,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"warehouseId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"仓库id\",\"isQuery\":\"1\",\"updateTime\":1674833952000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"\",\"isPk\":\"0\",\"createTime\":1674832623000,\"isEdit\":\"1\",\"tableId\":41,\"pk\":false,\"columnName\":\"warehouse_id\"},{\"capJavaField\":\"MaterialId\",\"usableColumn\":false,\"columnId\":507,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"materialId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"产品id\",\"isQuery\":\"1\",\"updateTime\":1674833952000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674825237000,\"isEdit\":\"1\",\"tableId\":41,\"pk\":false,\"columnName\":\"material_id\"},{\"capJavaField\":\"MaterialExtendId\",\"usableColumn\":false,\"columnId\":586,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"materialExtendId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"产品subid\",\"isQuery\":\"1\",\"updateTime\":1674833952000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Long', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:42:20');
INSERT INTO `sys_oper_log` VALUES (104, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":528,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674832627000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674831520000,\"tableId\":43,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Type\",\"usableColumn\":false,\"columnId\":529,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"type\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"出入库类别\",\"isQuery\":\"1\",\"updateTime\":1674832627000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674831520000,\"isEdit\":\"1\",\"tableId\":43,\"pk\":false,\"columnName\":\"type\"},{\"capJavaField\":\"SourceType\",\"usableColumn\":false,\"columnId\":592,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"sourceType\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"来源类别\",\"isQuery\":\"1\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(32)\",\"createBy\":\"\",\"isPk\":\"0\",\"createTime\":1674832627000,\"isEdit\":\"1\",\"tableId\":43,\"pk\":false,\"columnName\":\"source_type\"},{\"capJavaField\":\"SourceNum\",\"usableColumn\":false,\"columnId\":593,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"sourceNum\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"来源单据号\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(64)\",\"createBy\":\"\",\"isPk\":\"0\",\"createTime\":16', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:46:20');
INSERT INTO `sys_oper_log` VALUES (105, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"subTableName\":\"\",\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":495,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674833777000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674825237000,\"tableId\":40,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":496,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"名称\",\"isQuery\":\"1\",\"updateTime\":1674833777000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674825237000,\"isEdit\":\"1\",\"tableId\":40,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Ancestors\",\"usableColumn\":false,\"columnId\":582,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"ancestors\",\"edit\":false,\"query\":false,\"columnComment\":\"祖级列表\",\"updateTime\":1674833777000,\"sort\":3,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varbinary(128)\",\"createBy\":\"\",\"isPk\":\"0\",\"createTime\":1674832620000,\"tableId\":40,\"pk\":false,\"columnName\":\"ancestors\"},{\"capJavaField\":\"ParentId\",\"usableColumn\":true,\"columnId\":498,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"isInsert\":\"1\",\"javaField\":\"parentId\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"父类\",\"updateTime\":1674833777000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674825237000,', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:46:54');
INSERT INTO `sys_oper_log` VALUES (106, '代码生成', 2, 'com.mn.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/wms_order_item', '127.0.0.1', '内网IP', '{tableName=wms_order_item}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:48:49');
INSERT INTO `sys_oper_log` VALUES (107, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":557,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674834529000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674831540000,\"tableId\":44,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"HeaderId\",\"usableColumn\":false,\"columnId\":558,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"headerId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"表头Id\",\"isQuery\":\"1\",\"updateTime\":1674834529000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674831540000,\"isEdit\":\"1\",\"tableId\":44,\"pk\":false,\"columnName\":\"header_id\"},{\"capJavaField\":\"MaterialId\",\"usableColumn\":false,\"columnId\":559,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"materialId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"商品Id\",\"isQuery\":\"1\",\"updateTime\":1674834529000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674831540000,\"isEdit\":\"1\",\"tableId\":44,\"pk\":false,\"columnName\":\"material_id\"},{\"capJavaField\":\"MaterialExtendId\",\"usableColumn\":false,\"columnId\":560,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"materialExtendId\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"商品扩展id\",\"updateTime\":1674834529000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:51:14');
INSERT INTO `sys_oper_log` VALUES (108, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":528,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674834380000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674831520000,\"tableId\":43,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Type\",\"usableColumn\":false,\"columnId\":529,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"type\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"出入库类别\",\"isQuery\":\"1\",\"updateTime\":1674834380000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674831520000,\"isEdit\":\"1\",\"tableId\":43,\"pk\":false,\"columnName\":\"type\"},{\"capJavaField\":\"SourceType\",\"usableColumn\":false,\"columnId\":592,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"sourceType\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"来源类别\",\"isQuery\":\"1\",\"updateTime\":1674834380000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(32)\",\"createBy\":\"\",\"isPk\":\"0\",\"createTime\":1674832627000,\"isEdit\":\"1\",\"tableId\":43,\"pk\":false,\"columnName\":\"source_type\"},{\"capJavaField\":\"SourceNum\",\"usableColumn\":false,\"columnId\":593,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"sourceNum\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"来源单据号\",\"updateTime\":1674834380000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:51:37');
INSERT INTO `sys_oper_log` VALUES (109, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":608,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"id\",\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674833177000,\"tableId\":46,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Inout\",\"usableColumn\":false,\"columnId\":609,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"inout\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"出入库\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"char(1)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674833177000,\"isEdit\":\"1\",\"tableId\":46,\"pk\":false,\"columnName\":\"inout\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":610,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"名称\",\"isQuery\":\"1\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674833177000,\"isEdit\":\"1\",\"tableId\":46,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Enable\",\"usableColumn\":false,\"columnId\":611,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"enable\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"启用\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"char(1)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"cre', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-27 23:53:54');
INSERT INTO `sys_oper_log` VALUES (110, '代码生成', 8, 'com.mn.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', NULL, 0, NULL, '2023-01-27 23:54:04');
INSERT INTO `sys_oper_log` VALUES (111, '代码生成', 6, 'com.mn.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'fms_item,fms_account_head,fms_account,fms_account_item', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-28 01:26:11');
INSERT INTO `sys_oper_log` VALUES (112, '菜单管理', 1, 'com.mn.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"money\",\"orderNum\":3,\"menuName\":\"财务管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"fms\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"tenantId\":2,\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-28 01:27:17');
INSERT INTO `sys_oper_log` VALUES (113, '代码生成', 2, 'com.mn.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/fms_account', '127.0.0.1', '内网IP', '{tableName=fms_account}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-28 01:29:58');
INSERT INTO `sys_oper_log` VALUES (114, '代码生成', 2, 'com.mn.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/fms_account', '127.0.0.1', '内网IP', '{tableName=fms_account}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-28 01:36:05');
INSERT INTO `sys_oper_log` VALUES (115, '代码生成', 2, 'com.mn.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/fms_account_head', '127.0.0.1', '内网IP', '{tableName=fms_account_head}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-28 01:36:07');
INSERT INTO `sys_oper_log` VALUES (116, '代码生成', 2, 'com.mn.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/fms_account_item', '127.0.0.1', '内网IP', '{tableName=fms_account_item}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-28 01:36:09');
INSERT INTO `sys_oper_log` VALUES (117, '代码生成', 2, 'com.mn.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/fms_item', '127.0.0.1', '内网IP', '{tableName=fms_item}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-28 01:36:13');
INSERT INTO `sys_oper_log` VALUES (118, '代码生成', 2, 'com.mn.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/fms_item', '127.0.0.1', '内网IP', '{tableName=fms_item}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-28 01:36:42');
INSERT INTO `sys_oper_log` VALUES (119, '代码生成', 2, 'com.mn.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/fms_account_item', '127.0.0.1', '内网IP', '{tableName=fms_account_item}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-28 01:36:44');
INSERT INTO `sys_oper_log` VALUES (120, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":617,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674840965000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674840371000,\"tableId\":47,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":618,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"名称\",\"isQuery\":\"1\",\"updateTime\":1674840965000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674840371000,\"isEdit\":\"1\",\"tableId\":47,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"SerialNo\",\"usableColumn\":false,\"columnId\":619,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"serialNo\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"编号\",\"updateTime\":1674840965000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674840371000,\"isEdit\":\"1\",\"tableId\":47,\"pk\":false,\"columnName\":\"serial_no\"},{\"capJavaField\":\"CurrentAmount\",\"usableColumn\":false,\"columnId\":621,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"currentAmount\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"当前余额\",\"updateTime\":1674840965000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"BigDecimal\",\"queryType\":\"EQ\",\"columnType\":\"decimal(24,6)\",\"cre', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-28 01:38:41');
INSERT INTO `sys_oper_log` VALUES (121, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":628,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674840966000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674840371000,\"tableId\":48,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Type\",\"usableColumn\":false,\"columnId\":629,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"type\",\"htmlType\":\"select\",\"edit\":true,\"query\":false,\"columnComment\":\"类型\",\"updateTime\":1674840966000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674840371000,\"isEdit\":\"1\",\"tableId\":48,\"pk\":false,\"columnName\":\"type\"},{\"capJavaField\":\"OrganId\",\"usableColumn\":false,\"columnId\":630,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"organId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"单位\",\"isQuery\":\"1\",\"updateTime\":1674840966000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"LIKE\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674840371000,\"isEdit\":\"1\",\"tableId\":48,\"pk\":false,\"columnName\":\"organ_id\"},{\"capJavaField\":\"HandsPersonId\",\"usableColumn\":false,\"columnId\":631,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"handsPersonId\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"经手人\",\"updateTime\":1674840966000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-28 01:43:15');
INSERT INTO `sys_oper_log` VALUES (122, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":644,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674841004000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674840371000,\"tableId\":49,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"HeaderId\",\"usableColumn\":false,\"columnId\":645,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"headerId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"表头Id\",\"isQuery\":\"1\",\"updateTime\":1674841004000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674840371000,\"isEdit\":\"1\",\"tableId\":49,\"pk\":false,\"columnName\":\"header_id\"},{\"capJavaField\":\"AccountId\",\"usableColumn\":false,\"columnId\":646,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"accountId\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"账户Id\",\"updateTime\":1674841004000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674840371000,\"isEdit\":\"1\",\"tableId\":49,\"pk\":false,\"columnName\":\"account_id\"},{\"capJavaField\":\"InOutItemId\",\"usableColumn\":false,\"columnId\":647,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"inOutItemId\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"收支项目Id\",\"updateTime\":1674841004000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"colu', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-28 01:44:56');
INSERT INTO `sys_oper_log` VALUES (123, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":655,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674841002000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674840371000,\"tableId\":50,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":656,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"名称\",\"isQuery\":\"1\",\"updateTime\":1674841002000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674840371000,\"isEdit\":\"1\",\"tableId\":50,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Type\",\"usableColumn\":false,\"columnId\":657,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"type\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"类型\",\"isQuery\":\"1\",\"updateTime\":1674841002000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674840371000,\"isEdit\":\"1\",\"tableId\":50,\"pk\":false,\"columnName\":\"type\"},{\"capJavaField\":\"Remark\",\"usableColumn\":true,\"columnId\":658,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"isInsert\":\"1\",\"javaField\":\"remark\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"备注\",\"updateTime\":1674841002000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(100)\",\"createBy\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-28 01:46:39');
INSERT INTO `sys_oper_log` VALUES (124, '代码生成', 8, 'com.mn.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', NULL, 0, NULL, '2023-01-28 01:46:45');
INSERT INTO `sys_oper_log` VALUES (125, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":617,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674841121000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674840371000,\"tableId\":47,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":618,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"名称\",\"isQuery\":\"1\",\"updateTime\":1674841121000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674840371000,\"isEdit\":\"1\",\"tableId\":47,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"SerialNo\",\"usableColumn\":false,\"columnId\":619,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"serialNo\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"编号\",\"updateTime\":1674841121000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674840371000,\"isEdit\":\"1\",\"tableId\":47,\"pk\":false,\"columnName\":\"serial_no\"},{\"capJavaField\":\"CurrentAmount\",\"usableColumn\":false,\"columnId\":621,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"currentAmount\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"当前余额\",\"updateTime\":1674841121000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"BigDecimal\",\"queryType\":\"EQ\",\"columnType\":\"decimal(24,6)\",\"cre', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-28 01:52:12');
INSERT INTO `sys_oper_log` VALUES (126, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":628,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674841395000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674840371000,\"tableId\":48,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Type\",\"usableColumn\":false,\"columnId\":629,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"type\",\"htmlType\":\"select\",\"edit\":true,\"query\":false,\"columnComment\":\"类型\",\"updateTime\":1674841395000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674840371000,\"isEdit\":\"1\",\"tableId\":48,\"pk\":false,\"columnName\":\"type\"},{\"capJavaField\":\"OrganId\",\"usableColumn\":false,\"columnId\":630,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"organId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"单位\",\"isQuery\":\"1\",\"updateTime\":1674841395000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"LIKE\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674840371000,\"isEdit\":\"1\",\"tableId\":48,\"pk\":false,\"columnName\":\"organ_id\"},{\"capJavaField\":\"HandsPersonId\",\"usableColumn\":false,\"columnId\":631,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"handsPersonId\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"经手人\",\"updateTime\":1674841395000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-28 01:52:27');
INSERT INTO `sys_oper_log` VALUES (127, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":644,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674841496000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674840371000,\"tableId\":49,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"HeaderId\",\"usableColumn\":false,\"columnId\":645,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"headerId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"表头Id\",\"isQuery\":\"1\",\"updateTime\":1674841496000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674840371000,\"isEdit\":\"1\",\"tableId\":49,\"pk\":false,\"columnName\":\"header_id\"},{\"capJavaField\":\"AccountId\",\"usableColumn\":false,\"columnId\":646,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"accountId\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"账户Id\",\"updateTime\":1674841496000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674840371000,\"isEdit\":\"1\",\"tableId\":49,\"pk\":false,\"columnName\":\"account_id\"},{\"capJavaField\":\"InOutItemId\",\"usableColumn\":false,\"columnId\":647,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"inOutItemId\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"收支项目Id\",\"updateTime\":1674841496000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"colu', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-28 01:52:38');
INSERT INTO `sys_oper_log` VALUES (128, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":644,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674841958000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674840371000,\"tableId\":49,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"HeaderId\",\"usableColumn\":false,\"columnId\":645,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"headerId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"表头Id\",\"isQuery\":\"1\",\"updateTime\":1674841958000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674840371000,\"isEdit\":\"1\",\"tableId\":49,\"pk\":false,\"columnName\":\"header_id\"},{\"capJavaField\":\"AccountId\",\"usableColumn\":false,\"columnId\":646,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"accountId\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"账户Id\",\"updateTime\":1674841958000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674840371000,\"isEdit\":\"1\",\"tableId\":49,\"pk\":false,\"columnName\":\"account_id\"},{\"capJavaField\":\"InOutItemId\",\"usableColumn\":false,\"columnId\":647,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"inOutItemId\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"收支项目Id\",\"updateTime\":1674841958000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"colu', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-28 01:53:29');
INSERT INTO `sys_oper_log` VALUES (129, '代码生成', 2, 'com.mn.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"mn\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":655,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1674841599000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1674840371000,\"tableId\":50,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":656,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"名称\",\"isQuery\":\"1\",\"updateTime\":1674841599000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674840371000,\"isEdit\":\"1\",\"tableId\":50,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Type\",\"usableColumn\":false,\"columnId\":657,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"type\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"类型\",\"isQuery\":\"1\",\"updateTime\":1674841599000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1674840371000,\"isEdit\":\"1\",\"tableId\":50,\"pk\":false,\"columnName\":\"type\"},{\"capJavaField\":\"Remark\",\"usableColumn\":true,\"columnId\":658,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"isInsert\":\"1\",\"javaField\":\"remark\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"备注\",\"updateTime\":1674841599000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(100)\",\"createBy\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2023-01-28 01:53:43');
INSERT INTO `sys_oper_log` VALUES (130, '代码生成', 8, 'com.mn.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', NULL, 0, NULL, '2023-01-28 01:53:52');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `tenant_id` int NULL DEFAULT NULL COMMENT '租户',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '岗位信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2022-04-03 16:23:21', '', NULL, '', 1);
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2022-04-03 16:23:21', '', NULL, '', 1);
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2022-04-03 16:23:21', '', NULL, '', 1);
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2022-04-03 16:23:21', '', NULL, '', 1);
INSERT INTO `sys_post` VALUES (5, '12', '12', 0, '0', 'admin', '2023-01-24 18:13:00', '', NULL, NULL, 1);
INSERT INTO `sys_post` VALUES (6, '13', '13', 0, '0', 'admin', '2023-01-24 18:17:01', '', NULL, NULL, 1);
INSERT INTO `sys_post` VALUES (7, '14', '14', 0, '0', 'admin', '2023-01-24 18:38:27', '', NULL, '14', 2);
INSERT INTO `sys_post` VALUES (8, '7', '7', 0, '0', 'admin', '2023-01-25 16:51:14', '', NULL, '7', 2);
INSERT INTO `sys_post` VALUES (9, 'ceo', 'ceo', 0, '0', 'admin', '2023-01-25 16:51:50', '', NULL, 'ceo', 2);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `tenant_id` int NULL DEFAULT NULL COMMENT '租户',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 103 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 0, '1', 1, 1, '0', '0', 'admin', '2022-04-03 16:23:21', '', NULL, '超级管理员', 1);
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 99, '1', 0, 0, '1', '0', 'admin', '2022-04-03 16:23:21', 'wr', '2022-05-19 16:52:37', '普通角色', 1);
INSERT INTO `sys_role` VALUES (3, '管理员', 'gly', 1, '1', 1, 1, '0', '0', 'admin', '2022-05-16 09:34:54', 'wr', '2023-01-24 20:06:54', '普通管理员', 1);
INSERT INTO `sys_role` VALUES (100, '销售人员', 'crmSales', 2, '1', 1, 1, '0', '0', 'admin', '2022-04-10 16:44:52', 'wr', '2023-01-10 21:33:28', '客户关系管理系统销售人员', 1);
INSERT INTO `sys_role` VALUES (102, '测试使用', 'test1', 4, '1', 1, 1, '0', '0', 'wr', '2022-05-24 09:21:33', 'wr', '2022-05-24 09:21:50', NULL, 1);
INSERT INTO `sys_role` VALUES (103, 'gly', 'gly', 0, '1', 1, 1, '0', '0', 'admin', '2023-01-25 11:04:42', '', NULL, NULL, 2);

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `dept_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, '00010001');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `menu_id` bigint NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 4);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 103);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 107);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 113);
INSERT INTO `sys_role_menu` VALUES (2, 114);
INSERT INTO `sys_role_menu` VALUES (2, 115);
INSERT INTO `sys_role_menu` VALUES (2, 116);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1002);
INSERT INTO `sys_role_menu` VALUES (2, 1003);
INSERT INTO `sys_role_menu` VALUES (2, 1004);
INSERT INTO `sys_role_menu` VALUES (2, 1005);
INSERT INTO `sys_role_menu` VALUES (2, 1006);
INSERT INTO `sys_role_menu` VALUES (2, 1007);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1009);
INSERT INTO `sys_role_menu` VALUES (2, 1010);
INSERT INTO `sys_role_menu` VALUES (2, 1011);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1013);
INSERT INTO `sys_role_menu` VALUES (2, 1014);
INSERT INTO `sys_role_menu` VALUES (2, 1015);
INSERT INTO `sys_role_menu` VALUES (2, 1016);
INSERT INTO `sys_role_menu` VALUES (2, 1017);
INSERT INTO `sys_role_menu` VALUES (2, 1018);
INSERT INTO `sys_role_menu` VALUES (2, 1019);
INSERT INTO `sys_role_menu` VALUES (2, 1020);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1022);
INSERT INTO `sys_role_menu` VALUES (2, 1023);
INSERT INTO `sys_role_menu` VALUES (2, 1024);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1027);
INSERT INTO `sys_role_menu` VALUES (2, 1028);
INSERT INTO `sys_role_menu` VALUES (2, 1029);
INSERT INTO `sys_role_menu` VALUES (2, 1030);
INSERT INTO `sys_role_menu` VALUES (2, 1031);
INSERT INTO `sys_role_menu` VALUES (2, 1032);
INSERT INTO `sys_role_menu` VALUES (2, 1033);
INSERT INTO `sys_role_menu` VALUES (2, 1034);
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 1036);
INSERT INTO `sys_role_menu` VALUES (2, 1037);
INSERT INTO `sys_role_menu` VALUES (2, 1038);
INSERT INTO `sys_role_menu` VALUES (2, 1039);
INSERT INTO `sys_role_menu` VALUES (2, 1040);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1043);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 1055);
INSERT INTO `sys_role_menu` VALUES (2, 1056);
INSERT INTO `sys_role_menu` VALUES (2, 1057);
INSERT INTO `sys_role_menu` VALUES (2, 1058);
INSERT INTO `sys_role_menu` VALUES (2, 1059);
INSERT INTO `sys_role_menu` VALUES (2, 1060);
INSERT INTO `sys_role_menu` VALUES (3, 1);
INSERT INTO `sys_role_menu` VALUES (3, 100);
INSERT INTO `sys_role_menu` VALUES (3, 101);
INSERT INTO `sys_role_menu` VALUES (3, 102);
INSERT INTO `sys_role_menu` VALUES (3, 103);
INSERT INTO `sys_role_menu` VALUES (3, 104);
INSERT INTO `sys_role_menu` VALUES (3, 105);
INSERT INTO `sys_role_menu` VALUES (3, 106);
INSERT INTO `sys_role_menu` VALUES (3, 107);
INSERT INTO `sys_role_menu` VALUES (3, 108);
INSERT INTO `sys_role_menu` VALUES (3, 500);
INSERT INTO `sys_role_menu` VALUES (3, 501);
INSERT INTO `sys_role_menu` VALUES (3, 1001);
INSERT INTO `sys_role_menu` VALUES (3, 1002);
INSERT INTO `sys_role_menu` VALUES (3, 1003);
INSERT INTO `sys_role_menu` VALUES (3, 1004);
INSERT INTO `sys_role_menu` VALUES (3, 1005);
INSERT INTO `sys_role_menu` VALUES (3, 1006);
INSERT INTO `sys_role_menu` VALUES (3, 1007);
INSERT INTO `sys_role_menu` VALUES (3, 1008);
INSERT INTO `sys_role_menu` VALUES (3, 1009);
INSERT INTO `sys_role_menu` VALUES (3, 1010);
INSERT INTO `sys_role_menu` VALUES (3, 1011);
INSERT INTO `sys_role_menu` VALUES (3, 1012);
INSERT INTO `sys_role_menu` VALUES (3, 1013);
INSERT INTO `sys_role_menu` VALUES (3, 1014);
INSERT INTO `sys_role_menu` VALUES (3, 1015);
INSERT INTO `sys_role_menu` VALUES (3, 1016);
INSERT INTO `sys_role_menu` VALUES (3, 1017);
INSERT INTO `sys_role_menu` VALUES (3, 1018);
INSERT INTO `sys_role_menu` VALUES (3, 1019);
INSERT INTO `sys_role_menu` VALUES (3, 1020);
INSERT INTO `sys_role_menu` VALUES (3, 1021);
INSERT INTO `sys_role_menu` VALUES (3, 1022);
INSERT INTO `sys_role_menu` VALUES (3, 1023);
INSERT INTO `sys_role_menu` VALUES (3, 1024);
INSERT INTO `sys_role_menu` VALUES (3, 1025);
INSERT INTO `sys_role_menu` VALUES (3, 1026);
INSERT INTO `sys_role_menu` VALUES (3, 1027);
INSERT INTO `sys_role_menu` VALUES (3, 1028);
INSERT INTO `sys_role_menu` VALUES (3, 1029);
INSERT INTO `sys_role_menu` VALUES (3, 1030);
INSERT INTO `sys_role_menu` VALUES (3, 1031);
INSERT INTO `sys_role_menu` VALUES (3, 1032);
INSERT INTO `sys_role_menu` VALUES (3, 1033);
INSERT INTO `sys_role_menu` VALUES (3, 1034);
INSERT INTO `sys_role_menu` VALUES (3, 1035);
INSERT INTO `sys_role_menu` VALUES (3, 1036);
INSERT INTO `sys_role_menu` VALUES (3, 1037);
INSERT INTO `sys_role_menu` VALUES (3, 1038);
INSERT INTO `sys_role_menu` VALUES (3, 1039);
INSERT INTO `sys_role_menu` VALUES (3, 1040);
INSERT INTO `sys_role_menu` VALUES (3, 1041);
INSERT INTO `sys_role_menu` VALUES (3, 1042);
INSERT INTO `sys_role_menu` VALUES (3, 1043);
INSERT INTO `sys_role_menu` VALUES (3, 1044);
INSERT INTO `sys_role_menu` VALUES (3, 1045);
INSERT INTO `sys_role_menu` VALUES (3, 2000);
INSERT INTO `sys_role_menu` VALUES (3, 2020);
INSERT INTO `sys_role_menu` VALUES (3, 2021);
INSERT INTO `sys_role_menu` VALUES (3, 2022);
INSERT INTO `sys_role_menu` VALUES (3, 2023);
INSERT INTO `sys_role_menu` VALUES (3, 2024);
INSERT INTO `sys_role_menu` VALUES (3, 2025);
INSERT INTO `sys_role_menu` VALUES (3, 2026);
INSERT INTO `sys_role_menu` VALUES (3, 2027);
INSERT INTO `sys_role_menu` VALUES (3, 2028);
INSERT INTO `sys_role_menu` VALUES (3, 2029);
INSERT INTO `sys_role_menu` VALUES (3, 2030);
INSERT INTO `sys_role_menu` VALUES (3, 2031);
INSERT INTO `sys_role_menu` VALUES (3, 2032);
INSERT INTO `sys_role_menu` VALUES (3, 2033);
INSERT INTO `sys_role_menu` VALUES (3, 2034);
INSERT INTO `sys_role_menu` VALUES (3, 2035);
INSERT INTO `sys_role_menu` VALUES (3, 2036);
INSERT INTO `sys_role_menu` VALUES (3, 2037);
INSERT INTO `sys_role_menu` VALUES (3, 2038);
INSERT INTO `sys_role_menu` VALUES (3, 2039);
INSERT INTO `sys_role_menu` VALUES (3, 2040);
INSERT INTO `sys_role_menu` VALUES (3, 2041);
INSERT INTO `sys_role_menu` VALUES (3, 2042);
INSERT INTO `sys_role_menu` VALUES (3, 2043);
INSERT INTO `sys_role_menu` VALUES (3, 2051);
INSERT INTO `sys_role_menu` VALUES (3, 2053);
INSERT INTO `sys_role_menu` VALUES (3, 2054);
INSERT INTO `sys_role_menu` VALUES (100, 2000);
INSERT INTO `sys_role_menu` VALUES (100, 2020);
INSERT INTO `sys_role_menu` VALUES (100, 2021);
INSERT INTO `sys_role_menu` VALUES (100, 2022);
INSERT INTO `sys_role_menu` VALUES (100, 2023);
INSERT INTO `sys_role_menu` VALUES (100, 2024);
INSERT INTO `sys_role_menu` VALUES (100, 2025);
INSERT INTO `sys_role_menu` VALUES (100, 2026);
INSERT INTO `sys_role_menu` VALUES (100, 2027);
INSERT INTO `sys_role_menu` VALUES (100, 2028);
INSERT INTO `sys_role_menu` VALUES (100, 2029);
INSERT INTO `sys_role_menu` VALUES (100, 2030);
INSERT INTO `sys_role_menu` VALUES (100, 2031);
INSERT INTO `sys_role_menu` VALUES (100, 2032);
INSERT INTO `sys_role_menu` VALUES (100, 2033);
INSERT INTO `sys_role_menu` VALUES (100, 2034);
INSERT INTO `sys_role_menu` VALUES (100, 2035);
INSERT INTO `sys_role_menu` VALUES (100, 2036);
INSERT INTO `sys_role_menu` VALUES (100, 2037);
INSERT INTO `sys_role_menu` VALUES (100, 2038);
INSERT INTO `sys_role_menu` VALUES (100, 2039);
INSERT INTO `sys_role_menu` VALUES (100, 2040);
INSERT INTO `sys_role_menu` VALUES (100, 2041);
INSERT INTO `sys_role_menu` VALUES (100, 2042);
INSERT INTO `sys_role_menu` VALUES (100, 2043);
INSERT INTO `sys_role_menu` VALUES (100, 2053);
INSERT INTO `sys_role_menu` VALUES (100, 2054);
INSERT INTO `sys_role_menu` VALUES (102, 2000);
INSERT INTO `sys_role_menu` VALUES (102, 2008);
INSERT INTO `sys_role_menu` VALUES (102, 2009);
INSERT INTO `sys_role_menu` VALUES (102, 2010);
INSERT INTO `sys_role_menu` VALUES (102, 2011);
INSERT INTO `sys_role_menu` VALUES (102, 2012);
INSERT INTO `sys_role_menu` VALUES (102, 2013);
INSERT INTO `sys_role_menu` VALUES (102, 2014);
INSERT INTO `sys_role_menu` VALUES (102, 2015);
INSERT INTO `sys_role_menu` VALUES (102, 2016);
INSERT INTO `sys_role_menu` VALUES (102, 2017);
INSERT INTO `sys_role_menu` VALUES (102, 2018);
INSERT INTO `sys_role_menu` VALUES (102, 2019);
INSERT INTO `sys_role_menu` VALUES (102, 2020);
INSERT INTO `sys_role_menu` VALUES (102, 2021);
INSERT INTO `sys_role_menu` VALUES (102, 2022);
INSERT INTO `sys_role_menu` VALUES (102, 2026);
INSERT INTO `sys_role_menu` VALUES (102, 2027);
INSERT INTO `sys_role_menu` VALUES (102, 2028);
INSERT INTO `sys_role_menu` VALUES (102, 2029);
INSERT INTO `sys_role_menu` VALUES (102, 2030);
INSERT INTO `sys_role_menu` VALUES (102, 2031);
INSERT INTO `sys_role_menu` VALUES (102, 2032);
INSERT INTO `sys_role_menu` VALUES (102, 2033);
INSERT INTO `sys_role_menu` VALUES (102, 2034);
INSERT INTO `sys_role_menu` VALUES (102, 2035);
INSERT INTO `sys_role_menu` VALUES (102, 2036);
INSERT INTO `sys_role_menu` VALUES (102, 2037);
INSERT INTO `sys_role_menu` VALUES (102, 2038);
INSERT INTO `sys_role_menu` VALUES (102, 2039);
INSERT INTO `sys_role_menu` VALUES (102, 2040);
INSERT INTO `sys_role_menu` VALUES (102, 2041);
INSERT INTO `sys_role_menu` VALUES (102, 2042);
INSERT INTO `sys_role_menu` VALUES (102, 2043);
INSERT INTO `sys_role_menu` VALUES (102, 2044);
INSERT INTO `sys_role_menu` VALUES (102, 2045);
INSERT INTO `sys_role_menu` VALUES (102, 2046);
INSERT INTO `sys_role_menu` VALUES (102, 2047);
INSERT INTO `sys_role_menu` VALUES (102, 2048);
INSERT INTO `sys_role_menu` VALUES (102, 2049);
INSERT INTO `sys_role_menu` VALUES (102, 2051);
INSERT INTO `sys_role_menu` VALUES (102, 2053);
INSERT INTO `sys_role_menu` VALUES (103, 2000);
INSERT INTO `sys_role_menu` VALUES (103, 2014);
INSERT INTO `sys_role_menu` VALUES (103, 2015);
INSERT INTO `sys_role_menu` VALUES (103, 2016);
INSERT INTO `sys_role_menu` VALUES (103, 2017);
INSERT INTO `sys_role_menu` VALUES (103, 2018);
INSERT INTO `sys_role_menu` VALUES (103, 2019);
INSERT INTO `sys_role_menu` VALUES (103, 2020);
INSERT INTO `sys_role_menu` VALUES (103, 2021);
INSERT INTO `sys_role_menu` VALUES (103, 2022);
INSERT INTO `sys_role_menu` VALUES (103, 2023);
INSERT INTO `sys_role_menu` VALUES (103, 2024);
INSERT INTO `sys_role_menu` VALUES (103, 2025);
INSERT INTO `sys_role_menu` VALUES (103, 2026);
INSERT INTO `sys_role_menu` VALUES (103, 2027);
INSERT INTO `sys_role_menu` VALUES (103, 2028);
INSERT INTO `sys_role_menu` VALUES (103, 2029);
INSERT INTO `sys_role_menu` VALUES (103, 2030);
INSERT INTO `sys_role_menu` VALUES (103, 2031);
INSERT INTO `sys_role_menu` VALUES (103, 2032);
INSERT INTO `sys_role_menu` VALUES (103, 2033);
INSERT INTO `sys_role_menu` VALUES (103, 2034);
INSERT INTO `sys_role_menu` VALUES (103, 2035);
INSERT INTO `sys_role_menu` VALUES (103, 2036);
INSERT INTO `sys_role_menu` VALUES (103, 2037);
INSERT INTO `sys_role_menu` VALUES (103, 2038);
INSERT INTO `sys_role_menu` VALUES (103, 2039);
INSERT INTO `sys_role_menu` VALUES (103, 2040);
INSERT INTO `sys_role_menu` VALUES (103, 2041);
INSERT INTO `sys_role_menu` VALUES (103, 2042);
INSERT INTO `sys_role_menu` VALUES (103, 2043);
INSERT INTO `sys_role_menu` VALUES (103, 2044);
INSERT INTO `sys_role_menu` VALUES (103, 2045);
INSERT INTO `sys_role_menu` VALUES (103, 2046);
INSERT INTO `sys_role_menu` VALUES (103, 2047);
INSERT INTO `sys_role_menu` VALUES (103, 2048);
INSERT INTO `sys_role_menu` VALUES (103, 2049);
INSERT INTO `sys_role_menu` VALUES (103, 2051);
INSERT INTO `sys_role_menu` VALUES (103, 2053);
INSERT INTO `sys_role_menu` VALUES (103, 2054);

-- ----------------------------
-- Table structure for sys_tenant
-- ----------------------------
DROP TABLE IF EXISTS `sys_tenant`;
CREATE TABLE `sys_tenant`  (
  `tenant_id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录名',
  `user_num_limit` int NULL DEFAULT NULL COMMENT '用户数量限制',
  `enabled` bit(1) NULL DEFAULT b'1' COMMENT '启用 0-禁用  1-启用',
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `expire_time` datetime NULL DEFAULT NULL COMMENT '到期时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '租户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_tenant
-- ----------------------------
INSERT INTO `sys_tenant` VALUES (1, '系统管理', 9999, b'1', 'admin', '2023-01-21 12:27:52', '2090-04-01 12:27:58', NULL);
INSERT INTO `sys_tenant` VALUES (2, '测试', 111, b'1', NULL, '2023-01-21 13:38:41', '2023-02-04 00:00:00', '1111');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf16 COLLATE utf16_general_ci NULL DEFAULT '' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `mobile` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `tenant_id` int NULL DEFAULT NULL COMMENT '租户id',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `inx_sys_user_name`(`user_name`) USING BTREE COMMENT '不可重复'
) ENGINE = InnoDB AUTO_INCREMENT = 107 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 1, 'admin', '超管', '00', 'mn@163.com', '15888888888', '0', '', '$2a$10$oERlGFoOViUDsZ.t5nGo7effrQiCz/1FoWwUlqgViG.NaT3j7eaRe', '0', '0', '127.0.0.1', '2023-01-28 01:25:49', 'admin', '2022-04-03 16:23:20', '', '2023-01-28 01:25:49', '管理员', 2);
INSERT INTO `sys_user` VALUES (2, 1, 'wr', '文瑞', '00', '13085001100@qq.com', '13085001100', '0', '', '$2a$10$/17z0KHrZTgskBV9k0j1JOZcNfXyqFqdM3B7Kxbr/N6qrm6BEjDbO', '0', '0', '127.0.0.1', '2023-01-25 16:00:32', 'admin', '2022-04-03 16:23:20', 'admin', '2023-01-25 16:00:32', '销售管理员', 1);
INSERT INTO `sys_user` VALUES (3, 1, 'test', 'test', '00', '', '', '0', '', '$2a$10$vNVw4HbY84Yn3PE8FlnUHuPzyUs.1CWFivFzzK0tgDGua5C03xGS.', '0', '0', '36.32.14.128', '2022-05-24 14:28:13', 'wr', '2022-05-19 21:07:22', 'admin', '2023-01-10 00:35:33', NULL, 1);
INSERT INTO `sys_user` VALUES (100, 1, '18119630457', '传云', '00', '', '', '1', '', '$2a$10$/eADUSil6JyXfvfQbyC4TeouueOVJZCyfd65b3Ii7YMpnPWcTk2Ky', '0', '0', '36.32.15.248', '2022-08-07 16:15:44', 'admin', '2022-04-10 17:01:00', 'admin', '2023-01-10 00:35:34', NULL, 1);
INSERT INTO `sys_user` VALUES (101, 1, '13085062757', '婉燕', '00', '', '13085062757', '1', '', '$2a$10$K7QlMHefkJp0JwuizMjQa.O7sXEOnYvWEjqZ8goIYiDqrtUk/VRHi', '0', '0', '36.32.15.115', '2022-12-30 13:57:45', 'admin', '2022-04-14 11:24:06', 'admin', '2022-12-30 13:57:45', NULL, 1);
INSERT INTO `sys_user` VALUES (103, 1, '17705691657', '龙恒', '00', '', '17705691657', '0', '', '$2a$10$gErM0PgtVay5RXFP2xyluuUIZgKBK.GLC8E2tArpyQX6sxxW1ai..', '0', '0', '36.32.14.248', '2022-08-29 11:41:19', 'wr', '2022-06-20 09:58:24', 'admin', '2023-01-10 13:22:33', NULL, 1);
INSERT INTO `sys_user` VALUES (104, 1, 'sunhui', '孙慧', '00', '', '', '1', '', '$2a$10$7Y3MXvFlEkIXeskH1PiZ7.yupotdwgSVcEvxqL6TNIBxfulzu45ei', '1', '2', '36.32.15.248', '2022-08-04 17:07:39', 'wr', '2022-08-03 09:54:00', 'wr', '2022-08-11 15:15:16', NULL, 1);
INSERT INTO `sys_user` VALUES (105, 1, 'liran', '丽冉', '00', '1668581997@qq.com', '15077939981', '1', '/profile/avatar/2022/10/10/blob_20221010110510A104.jpeg', '$2a$10$5sGalIDnk7Z80QyOGt4lBOnNfedulZRJlRNWinULc.gvs/4BpcsQW', '0', '0', '36.32.15.115', '2022-12-30 13:58:23', 'wr', '2022-08-15 14:14:56', 'admin', '2023-01-25 15:49:03', NULL, 1);
INSERT INTO `sys_user` VALUES (106, 1, '130', '130', '00', '', '', '0', '', '$2a$10$gkE12OBzN4YrMcQN6ra/8.0rezm6NuJdmau4LA27UmWmOcaOUBx2S', '0', '0', '127.0.0.1', '2023-01-03 16:15:27', '', '2023-01-03 16:15:10', '', '2023-01-03 16:15:26', NULL, 2);
INSERT INTO `sys_user` VALUES (107, 1, 'cs1', '测试2', '', '', '', '0', '', '$2a$10$46GBH6N2Z4zjZQsdzwFqpuCUcdAd4YyDGKZVryU59RgPyMLMEmmHC', '0', '0', '', NULL, 'admin', '2023-01-24 09:42:04', '', NULL, NULL, 2);

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `post_id` bigint NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 3);
INSERT INTO `sys_user_post` VALUES (3, 3);
INSERT INTO `sys_user_post` VALUES (100, 3);
INSERT INTO `sys_user_post` VALUES (101, 3);
INSERT INTO `sys_user_post` VALUES (103, 3);
INSERT INTO `sys_user_post` VALUES (105, 3);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 3);
INSERT INTO `sys_user_role` VALUES (2, 100);
INSERT INTO `sys_user_role` VALUES (101, 100);
INSERT INTO `sys_user_role` VALUES (105, 100);

-- ----------------------------
-- Table structure for wms_material
-- ----------------------------
DROP TABLE IF EXISTS `wms_material`;
CREATE TABLE `wms_material`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `category_id` bigint NULL DEFAULT NULL COMMENT '产品类型id',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `mfrs` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '制造商',
  `model` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '型号',
  `standard` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规格',
  `unit` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单位-单个',
  `img_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片名称',
  `unit_id` bigint NULL DEFAULT NULL COMMENT '计量单位Id',
  `expiry_num` int NULL DEFAULT NULL COMMENT '保质期天数',
  `weight` decimal(24, 6) NULL DEFAULT NULL COMMENT '基础重量(kg)',
  `enabled` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否启用',
  `enable_serial` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '是否序列号',
  `enable_batch` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '是否批次号',
  `tenant_id` bigint NULL DEFAULT NULL COMMENT '租户id',
  `del_flag` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK675951272AB6672C`(`category_id`) USING BTREE,
  INDEX `UnitId`(`unit_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 619 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '产品表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wms_material
-- ----------------------------
INSERT INTO `wms_material` VALUES (568, 17, '商品1', '制1', 'sp1', '', '个', NULL, NULL, NULL, NULL, '1', '0', '0', 2, '0', '', NULL, '2023-01-28 00:10:15');
INSERT INTO `wms_material` VALUES (569, 17, '商品2', '', 'sp2', '', '只', NULL, NULL, NULL, NULL, '1', '0', '0', 2, '0', '', NULL, '2023-01-28 00:10:18');
INSERT INTO `wms_material` VALUES (570, 17, '商品3', '', 'sp3', '', '个', NULL, NULL, NULL, NULL, '1', '0', '0', 2, '0', '', NULL, '2023-01-28 00:10:23');
INSERT INTO `wms_material` VALUES (577, NULL, '商品8', '', 'sp8', '', '', NULL, 15, NULL, NULL, '1', '0', '0', 63, '0', '', NULL, '2023-01-27 20:09:10');
INSERT INTO `wms_material` VALUES (579, 21, '商品17', '', 'sp17', '', '', NULL, 15, NULL, NULL, '1', '0', '0', 63, '0', '', NULL, '2023-01-27 20:09:10');
INSERT INTO `wms_material` VALUES (586, 17, '序列号商品测试', '', 'xlh123', '', '个', NULL, NULL, NULL, NULL, '1', '1', '0', 63, '0', '', NULL, '2023-01-27 20:09:10');
INSERT INTO `wms_material` VALUES (587, 17, '商品test1', '南通中远', '', 'test1', '个', NULL, NULL, NULL, NULL, '1', '0', '0', 63, '0', '', NULL, '2023-01-27 20:09:10');
INSERT INTO `wms_material` VALUES (588, 21, '商品200', 'fafda', 'weqwe', '300ml', '个', NULL, NULL, NULL, NULL, '1', '0', '0', 63, '0', 'aaaabbbbb', NULL, '2023-01-27 20:09:10');
INSERT INTO `wms_material` VALUES (619, NULL, '衣服', NULL, NULL, NULL, '件', '', NULL, NULL, NULL, '1', '0', '0', 63, '0', NULL, NULL, '2023-01-27 20:09:10');

-- ----------------------------
-- Table structure for wms_material_attribute
-- ----------------------------
DROP TABLE IF EXISTS `wms_material_attribute`;
CREATE TABLE `wms_material_attribute`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `material_id` bigint NULL DEFAULT NULL COMMENT '商品id',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '属性名',
  `tenant_id` bigint NULL DEFAULT NULL COMMENT '租户id',
  `del_flag` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '产品属性表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wms_material_attribute
-- ----------------------------
INSERT INTO `wms_material_attribute` VALUES (1, NULL, '多颜色', 63, '0', NULL, NULL);
INSERT INTO `wms_material_attribute` VALUES (2, NULL, '多尺寸', 63, '0', NULL, NULL);
INSERT INTO `wms_material_attribute` VALUES (3, NULL, '自定义1', 63, '0', NULL, NULL);
INSERT INTO `wms_material_attribute` VALUES (4, NULL, '自定义2', 63, '0', NULL, NULL);
INSERT INTO `wms_material_attribute` VALUES (5, NULL, '自定义3', 63, '0', NULL, NULL);

-- ----------------------------
-- Table structure for wms_material_attribute_sub
-- ----------------------------
DROP TABLE IF EXISTS `wms_material_attribute_sub`;
CREATE TABLE `wms_material_attribute_sub`  (
  `id` bigint NOT NULL COMMENT '商品id',
  `attribute_id` bigint NOT NULL COMMENT '归属',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '属性名',
  `tenant_id` bigint NULL DEFAULT NULL COMMENT '租户id',
  `del_flag` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '产品属性表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wms_material_attribute_sub
-- ----------------------------

-- ----------------------------
-- Table structure for wms_material_attribute_template
-- ----------------------------
DROP TABLE IF EXISTS `wms_material_attribute_template`;
CREATE TABLE `wms_material_attribute_template`  (
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '属性名',
  `values` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '属性值',
  `tenant_id` bigint NOT NULL COMMENT '租户id',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`name`, `tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '产品属性表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wms_material_attribute_template
-- ----------------------------

-- ----------------------------
-- Table structure for wms_material_category
-- ----------------------------
DROP TABLE IF EXISTS `wms_material_category`;
CREATE TABLE `wms_material_category`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `ancestors` varbinary(128) NULL DEFAULT NULL COMMENT '祖级列表',
  `parent_id` bigint NULL DEFAULT NULL COMMENT '上级id',
  `sort` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示顺序',
  `serial_no` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编号',
  `remark` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `tenant_id` bigint NULL DEFAULT NULL COMMENT '租户id',
  `del_flag` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK3EE7F725237A77D8`(`parent_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '产品类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wms_material_category
-- ----------------------------
INSERT INTO `wms_material_category` VALUES (17, '目录1', NULL, NULL, '11', 'wae12', 'eee', NULL, '2021-02-17 15:11:35', 63, '0');
INSERT INTO `wms_material_category` VALUES (21, '目录2', NULL, 17, '22', 'ada112', 'ddd', NULL, '2020-07-20 23:08:44', 63, '0');

-- ----------------------------
-- Table structure for wms_material_current_stock
-- ----------------------------
DROP TABLE IF EXISTS `wms_material_current_stock`;
CREATE TABLE `wms_material_current_stock`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `warehouse_id` bigint NULL DEFAULT NULL COMMENT '仓库id',
  `material_id` bigint NULL DEFAULT NULL COMMENT '产品id',
  `material_extend_id` bigint NULL DEFAULT NULL COMMENT '产品拓展id',
  `batch` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '批次号',
  `current_number` decimal(16, 2) NULL DEFAULT NULL COMMENT '当前库存数量',
  `tenant_id` bigint NULL DEFAULT NULL COMMENT '租户id',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `del_flag` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '产品当前库存' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of wms_material_current_stock
-- ----------------------------
INSERT INTO `wms_material_current_stock` VALUES (19, 14, 588, NULL, NULL, 7.00, 63, NULL, NULL, '0');
INSERT INTO `wms_material_current_stock` VALUES (20, 14, 568, NULL, NULL, 2.00, 63, NULL, NULL, '0');
INSERT INTO `wms_material_current_stock` VALUES (21, 15, 568, NULL, NULL, 1.00, 63, NULL, NULL, '0');
INSERT INTO `wms_material_current_stock` VALUES (22, 14, 570, NULL, NULL, 8.00, 63, NULL, NULL, '0');
INSERT INTO `wms_material_current_stock` VALUES (23, 14, 619, NULL, NULL, 1.00, 63, NULL, NULL, '0');
INSERT INTO `wms_material_current_stock` VALUES (24, 15, 619, NULL, NULL, 0.00, 63, NULL, NULL, '0');
INSERT INTO `wms_material_current_stock` VALUES (25, 17, 619, NULL, NULL, 0.00, 63, NULL, NULL, '0');

-- ----------------------------
-- Table structure for wms_material_extend
-- ----------------------------
DROP TABLE IF EXISTS `wms_material_extend`;
CREATE TABLE `wms_material_extend`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `material_id` bigint NULL DEFAULT NULL COMMENT '商品id',
  `bar_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品条码',
  `commodity_unit` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品单位',
  `sku` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '多属性',
  `purchase_decimal` decimal(16, 2) NULL DEFAULT NULL COMMENT '采购价格',
  `commodity_decimal` decimal(16, 2) NULL DEFAULT NULL COMMENT '零售价格',
  `wholesale_decimal` decimal(16, 2) NULL DEFAULT NULL COMMENT '销售价格',
  `low_decimal` decimal(16, 2) NULL DEFAULT NULL COMMENT '最低售价',
  `default_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否为默认单位，1是，0否',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人编码',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人编码',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间戳',
  `tenant_id` bigint NULL DEFAULT NULL COMMENT '租户id',
  `del_Flag` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '产品价格扩展' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of wms_material_extend
-- ----------------------------
INSERT INTO `wms_material_extend` VALUES (1, 587, '1000', '个', NULL, 11.00, 22.00, 22.00, 22.00, '1', 'jsh', '2020-02-20 23:22:03', 'jsh', NULL, 63, '0');
INSERT INTO `wms_material_extend` VALUES (2, 568, '1001', '个', NULL, 11.00, 15.00, 15.00, 15.00, '1', 'jsh', '2020-02-20 23:44:57', 'jsh', NULL, 63, '0');
INSERT INTO `wms_material_extend` VALUES (3, 569, '1002', '只', NULL, 10.00, 15.00, 15.00, 13.00, '1', 'jsh', '2020-02-20 23:45:15', 'jsh', NULL, 63, '0');
INSERT INTO `wms_material_extend` VALUES (4, 570, '1003', '个', NULL, 8.00, 15.00, 14.00, 13.00, '1', 'jsh', '2020-02-20 23:45:37', 'jsh', NULL, 63, '0');
INSERT INTO `wms_material_extend` VALUES (5, 577, '1004', '个', NULL, 10.00, 20.00, 20.00, 20.00, '1', 'jsh', '2020-02-20 23:46:36', 'jsh', NULL, 63, '0');
INSERT INTO `wms_material_extend` VALUES (6, 577, '1005', '箱', NULL, 120.00, 240.00, 240.00, 240.00, '0', 'jsh', '2020-02-20 23:46:36', 'jsh', NULL, 63, '0');
INSERT INTO `wms_material_extend` VALUES (7, 579, '1006', '个', NULL, 20.00, 30.00, 30.00, 30.00, '1', 'jsh', '2020-02-20 23:47:04', 'jsh', NULL, 63, '0');
INSERT INTO `wms_material_extend` VALUES (8, 579, '1007', '箱', NULL, 240.00, 360.00, 360.00, 360.00, '0', 'jsh', '2020-02-20 23:47:04', 'jsh', NULL, 63, '0');
INSERT INTO `wms_material_extend` VALUES (9, 586, '1008', '个', NULL, 12.00, 15.00, 15.00, 15.00, '1', 'jsh', '2020-02-20 23:47:23', 'jsh', NULL, 63, '0');
INSERT INTO `wms_material_extend` VALUES (10, 588, '1009', '个', NULL, 11.00, 22.00, 22.00, 22.00, '1', 'jsh', '2020-07-21 00:58:15', 'jsh', NULL, 63, '0');
INSERT INTO `wms_material_extend` VALUES (36, 619, '1014', '件', '橙色,M', 12.00, 15.00, 14.00, NULL, '1', 'jsh', '2021-07-28 01:00:20', 'jsh', NULL, 63, '0');
INSERT INTO `wms_material_extend` VALUES (37, 619, '1015', '件', '橙色,L', 12.00, 15.00, 14.00, NULL, '0', 'jsh', '2021-07-28 01:00:20', 'jsh', NULL, 63, '0');
INSERT INTO `wms_material_extend` VALUES (38, 619, '1016', '件', '绿色,M', 12.00, 15.00, 14.00, NULL, '0', 'jsh', '2021-07-28 01:00:20', 'jsh', NULL, 63, '0');
INSERT INTO `wms_material_extend` VALUES (39, 619, '1017', '件', '绿色,L', 12.00, 15.00, 14.00, NULL, '0', 'jsh', '2021-07-28 01:00:20', 'jsh', NULL, 63, '0');

-- ----------------------------
-- Table structure for wms_order_head
-- ----------------------------
DROP TABLE IF EXISTS `wms_order_head`;
CREATE TABLE `wms_order_head`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '出入库类别',
  `source_type` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '来源类别',
  `source_num` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '来源单据号',
  `default_num` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '初始票据号',
  `bill_num` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '票据号',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `oper_time` datetime NULL DEFAULT NULL COMMENT '出入库时间',
  `organ_id` bigint NULL DEFAULT NULL COMMENT '供应商id or 者客户id',
  `user_id` bigint NULL DEFAULT NULL COMMENT '操作者',
  `approver_id` bigint NULL DEFAULT NULL COMMENT '审批人',
  `operate_id` bigint NULL DEFAULT NULL COMMENT '出入库操作人',
  `fms_head_id` bigint NULL DEFAULT NULL COMMENT '账户id',
  `pay_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '付款类型(现金、记账等)',
  `bill_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据类型',
  `remark` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `file_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件名称',
  `sales_man` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '业务员（可以多个）',
  `account_id_list` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '多账户ID列表',
  `account_money_list` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '多账户金额列表',
  `discount` decimal(24, 6) NULL DEFAULT NULL COMMENT '优惠率',
  `discount_money` decimal(24, 6) NULL DEFAULT NULL COMMENT '优惠金额',
  `discount_last_money` decimal(24, 6) NULL DEFAULT NULL COMMENT '优惠后金额',
  `other_money` decimal(24, 6) NULL DEFAULT NULL COMMENT '销售或采购费用合计',
  `deposit` decimal(24, 6) NULL DEFAULT NULL COMMENT '订金',
  `status` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态，0未审核、1已审核、2完成采购|销售、3部分采购|销售',
  `purchase_status` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '采购状态，0未采购、2完成采购、3部分采购',
  `link_number` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联订单号',
  `tenant_id` int NULL DEFAULT NULL COMMENT '租户id',
  `del_flag` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK2A80F214B610FC06`(`organ_id`) USING BTREE,
  INDEX `FK2A80F214AAE50527`(`fms_head_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单据主表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wms_order_head
-- ----------------------------
INSERT INTO `wms_order_head` VALUES (258, '其它', NULL, NULL, 'CGDD00000000630', 'CGDD00000000630', '2021-06-02 00:21:54', '2021-06-02 00:21:44', 57, 63, NULL, NULL, NULL, '现付', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', '0', NULL, 63, '0');
INSERT INTO `wms_order_head` VALUES (259, '入库', NULL, NULL, 'CGRK00000000631', 'CGRK00000000631', '2021-06-02 00:22:23', '2021-06-02 00:22:05', 57, 63, NULL, NULL, 17, '现付', NULL, NULL, NULL, NULL, '', '', 0.000000, 0.000000, 110.000000, 0.000000, NULL, '0', '0', 'CGDD00000000630', 63, '0');
INSERT INTO `wms_order_head` VALUES (260, '出库', NULL, NULL, 'CGTH00000000632', 'CGTH00000000632', '2021-06-02 00:22:35', '2021-06-02 00:22:26', 57, 63, NULL, NULL, 17, '现付', NULL, NULL, NULL, NULL, NULL, NULL, 0.000000, 0.000000, 22.000000, 0.000000, NULL, '0', '0', NULL, 63, '0');
INSERT INTO `wms_order_head` VALUES (261, '其它', NULL, NULL, 'XSDD00000000633', 'XSDD00000000633', '2021-06-02 00:22:48', '2021-06-02 00:22:39', 58, 63, NULL, NULL, NULL, '现付', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', '0', NULL, 63, '0');
INSERT INTO `wms_order_head` VALUES (262, '出库', NULL, NULL, 'XSCK00000000634', 'XSCK00000000634', '2021-06-02 00:23:03', '2021-06-02 00:22:54', 58, 63, NULL, NULL, 17, '现付', NULL, NULL, NULL, '', '', '', 0.000000, 0.000000, 44.000000, 0.000000, NULL, '0', '0', 'XSDD00000000633', 63, '0');
INSERT INTO `wms_order_head` VALUES (263, '入库', NULL, NULL, 'XSTH00000000635', 'XSTH00000000635', '2021-06-02 00:23:12', '2021-06-02 00:23:05', 71, 63, NULL, NULL, 17, '现付', NULL, NULL, NULL, '', '', '', 0.000000, 0.000000, 22.000000, 0.000000, NULL, '0', '0', NULL, 63, '0');
INSERT INTO `wms_order_head` VALUES (264, '出库', NULL, NULL, 'LSCK00000000636', 'LSCK00000000636', '2021-06-02 00:23:21', '2021-06-02 00:23:14', 60, 63, NULL, NULL, 17, '现付', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, 63, '0');
INSERT INTO `wms_order_head` VALUES (265, '入库', NULL, NULL, 'LSTH00000000637', 'LSTH00000000637', '2021-06-02 00:23:29', '2021-06-02 00:23:23', 60, 63, NULL, NULL, 17, '现付', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, 63, '0');
INSERT INTO `wms_order_head` VALUES (266, '入库', NULL, NULL, 'QTRK00000000638', 'QTRK00000000638', '2021-06-02 00:23:48', '2021-06-02 00:23:36', 57, 63, NULL, NULL, NULL, '现付', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', NULL, 63, '0');
INSERT INTO `wms_order_head` VALUES (267, '出库', NULL, NULL, 'QTCK00000000639', 'QTCK00000000639', '2021-06-02 00:23:59', '2021-06-02 00:23:50', 58, 63, NULL, NULL, NULL, '现付', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, 63, '0');
INSERT INTO `wms_order_head` VALUES (268, '出库', NULL, NULL, 'DBCK00000000640', 'DBCK00000000640', '2021-06-02 00:24:09', '2021-06-02 00:24:00', NULL, 63, NULL, NULL, NULL, '现付', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, 63, '0');
INSERT INTO `wms_order_head` VALUES (269, '其它', NULL, NULL, 'ZZD00000000641', 'ZZD00000000641', '2021-06-02 00:24:29', '2021-06-02 00:24:11', NULL, 63, NULL, NULL, NULL, '现付', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, 63, '0');
INSERT INTO `wms_order_head` VALUES (270, '其它', NULL, NULL, 'CXD00000000642', 'CXD00000000642', '2021-06-02 00:24:45', '2021-06-02 00:24:32', NULL, 63, NULL, NULL, NULL, '现付', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, 63, '0');
INSERT INTO `wms_order_head` VALUES (271, '入库', NULL, NULL, 'CGRK00000000651', 'CGRK00000000651', '2021-07-06 23:45:20', '2021-07-06 23:44:45', 57, 63, NULL, NULL, 17, '现付', NULL, NULL, NULL, NULL, '', '', 0.000000, 0.000000, 80.000000, 0.000000, NULL, '0', '0', NULL, 63, '0');
INSERT INTO `wms_order_head` VALUES (272, '出库', NULL, NULL, 'XSCK00000000652', 'XSCK00000000652', '2021-07-06 23:46:07', '2021-07-06 23:45:24', 58, 63, NULL, NULL, 17, '现付', NULL, NULL, NULL, '', '', '', 0.000000, 0.000000, 28.000000, 0.000000, NULL, '0', '0', NULL, 63, '0');
INSERT INTO `wms_order_head` VALUES (273, '入库', NULL, NULL, 'CGRK00000000658', 'CGRK00000000658', '2021-07-28 00:58:12', '2021-07-28 00:58:02', 57, 63, NULL, NULL, 17, '现付', NULL, NULL, NULL, NULL, '', '', 0.000000, 0.000000, 60.000000, 0.000000, NULL, '0', '0', NULL, 63, '0');
INSERT INTO `wms_order_head` VALUES (274, '出库', NULL, NULL, 'LSCK00000000662', 'LSCK00000000662', '2023-01-11 10:48:07', '2023-01-11 10:46:23', NULL, 63, NULL, NULL, 17, '现付', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, NULL, '0');
INSERT INTO `wms_order_head` VALUES (275, '其它', NULL, NULL, 'CGDD00000000664', 'CGDD00000000664', '2023-01-11 11:15:56', '2023-01-11 11:11:05', 74, 63, NULL, NULL, 17, '现付', NULL, NULL, '', NULL, '', '', 3.000000, 159.000000, 5141.000000, NULL, NULL, '1', '0', NULL, NULL, '0');

-- ----------------------------
-- Table structure for wms_order_item
-- ----------------------------
DROP TABLE IF EXISTS `wms_order_item`;
CREATE TABLE `wms_order_item`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `header_id` bigint NOT NULL COMMENT '表头Id',
  `material_id` bigint NOT NULL COMMENT '商品Id',
  `material_extend_id` bigint NULL DEFAULT NULL COMMENT '商品扩展id',
  `material_unit` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品计量单位',
  `sku` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '多属性',
  `oper_number` decimal(24, 6) NULL DEFAULT NULL COMMENT '数量',
  `basic_number` decimal(24, 6) NULL DEFAULT NULL COMMENT '基础数量，如kg、瓶',
  `unit_price` decimal(24, 6) NULL DEFAULT NULL COMMENT '单价',
  `purchase_unit_price` decimal(24, 6) NULL DEFAULT NULL COMMENT '采购单价',
  `tax_unit_price` decimal(24, 6) NULL DEFAULT NULL COMMENT '含税单价',
  `all_price` decimal(24, 6) NULL DEFAULT NULL COMMENT '金额',
  `remark` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `warehouse_id` bigint NULL DEFAULT NULL COMMENT '仓库ID',
  `another_warehouse_id` bigint NULL DEFAULT NULL COMMENT '调拨仓库',
  `tax_rate` decimal(24, 6) NULL DEFAULT NULL COMMENT '税率',
  `tax_money` decimal(24, 6) NULL DEFAULT NULL COMMENT '税额',
  `tax_last_money` decimal(24, 6) NULL DEFAULT NULL COMMENT '价税合计',
  `material_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品类型',
  `sn_list` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '序列号列表',
  `batch` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '批号',
  `expiration_date` datetime NULL DEFAULT NULL COMMENT '有效日期',
  `link_id` bigint NULL DEFAULT NULL COMMENT '关联明细id',
  `tenant_id` int NULL DEFAULT NULL COMMENT '租户id',
  `del_flag` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK2A819F475D61CCF7`(`material_id`) USING BTREE,
  INDEX `FK2A819F474BB6190E`(`header_id`) USING BTREE,
  INDEX `FK2A819F479485B3F5`(`warehouse_id`) USING BTREE,
  INDEX `FK2A819F47729F5392`(`another_warehouse_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单据子表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wms_order_item
-- ----------------------------
INSERT INTO `wms_order_item` VALUES (312, 258, 588, 10, '个', NULL, 10.000000, 10.000000, 11.000000, NULL, NULL, 110.000000, NULL, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 63, '0');
INSERT INTO `wms_order_item` VALUES (313, 259, 588, 10, '个', NULL, 10.000000, 10.000000, 11.000000, NULL, NULL, 110.000000, NULL, 14, NULL, NULL, 0.000000, 110.000000, NULL, NULL, NULL, NULL, NULL, 63, '0');
INSERT INTO `wms_order_item` VALUES (314, 260, 588, 10, '个', NULL, 2.000000, 2.000000, 11.000000, NULL, 11.000000, 22.000000, NULL, 14, NULL, 0.000000, 0.000000, 22.000000, NULL, NULL, NULL, NULL, NULL, 63, '0');
INSERT INTO `wms_order_item` VALUES (315, 261, 588, 10, '个', NULL, 2.000000, 2.000000, 22.000000, NULL, NULL, 44.000000, NULL, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 63, '0');
INSERT INTO `wms_order_item` VALUES (316, 262, 588, 10, '个', NULL, 2.000000, 2.000000, 22.000000, NULL, NULL, 44.000000, NULL, 14, NULL, NULL, 0.000000, 44.000000, NULL, NULL, NULL, NULL, NULL, 63, '0');
INSERT INTO `wms_order_item` VALUES (317, 263, 588, 10, '个', NULL, 1.000000, 1.000000, 22.000000, NULL, 22.000000, 22.000000, NULL, 14, NULL, 0.000000, 0.000000, 22.000000, NULL, NULL, NULL, NULL, NULL, 63, '0');
INSERT INTO `wms_order_item` VALUES (318, 264, 588, 10, '个', NULL, 1.000000, 1.000000, 22.000000, NULL, NULL, 22.000000, NULL, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 63, '0');
INSERT INTO `wms_order_item` VALUES (319, 265, 588, 10, '个', NULL, 1.000000, 1.000000, 22.000000, NULL, NULL, 22.000000, NULL, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 63, '0');
INSERT INTO `wms_order_item` VALUES (320, 266, 568, 2, '个', NULL, 5.000000, 5.000000, 11.000000, NULL, NULL, 55.000000, NULL, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 63, '0');
INSERT INTO `wms_order_item` VALUES (321, 267, 568, 2, '个', NULL, 2.000000, 2.000000, 15.000000, NULL, NULL, 30.000000, NULL, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 63, '0');
INSERT INTO `wms_order_item` VALUES (322, 268, 568, 2, '个', NULL, 1.000000, 1.000000, 11.000000, NULL, NULL, 11.000000, NULL, 14, 15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 63, '0');
INSERT INTO `wms_order_item` VALUES (323, 269, 588, 10, '个', NULL, 1.000000, 1.000000, 0.000000, NULL, NULL, 0.000000, NULL, 14, NULL, NULL, NULL, NULL, '组合件', NULL, NULL, NULL, NULL, 63, '0');
INSERT INTO `wms_order_item` VALUES (324, 269, 568, 2, '个', NULL, 1.000000, 1.000000, 0.000000, NULL, NULL, 0.000000, NULL, 14, NULL, NULL, NULL, NULL, '普通子件', NULL, NULL, NULL, NULL, 63, '0');
INSERT INTO `wms_order_item` VALUES (325, 270, 588, 10, '个', NULL, 1.000000, 1.000000, 0.000000, NULL, NULL, 0.000000, NULL, 14, NULL, NULL, NULL, NULL, '组合件', NULL, NULL, NULL, NULL, 63, '0');
INSERT INTO `wms_order_item` VALUES (326, 270, 568, 2, '个', NULL, 1.000000, 1.000000, 0.000000, NULL, NULL, 0.000000, NULL, 14, NULL, NULL, NULL, NULL, '普通子件', NULL, NULL, NULL, NULL, 63, '0');
INSERT INTO `wms_order_item` VALUES (327, 271, 570, 4, '个', NULL, 10.000000, 10.000000, 8.000000, NULL, 8.000000, 80.000000, NULL, 14, NULL, 0.000000, 0.000000, 80.000000, NULL, NULL, NULL, NULL, NULL, 63, '0');
INSERT INTO `wms_order_item` VALUES (328, 272, 570, 4, '个', NULL, 2.000000, 2.000000, 14.000000, NULL, 14.000000, 28.000000, NULL, 14, NULL, 0.000000, 0.000000, 28.000000, NULL, NULL, NULL, NULL, NULL, 63, '0');
INSERT INTO `wms_order_item` VALUES (330, 273, 619, 37, '件', '橙色,L', 5.000000, 5.000000, 12.000000, NULL, 12.000000, 60.000000, NULL, 14, NULL, 0.000000, 0.000000, 60.000000, NULL, NULL, NULL, NULL, NULL, 63, '0');
INSERT INTO `wms_order_item` VALUES (331, 274, 619, 36, '件', '橙色,M', 4.000000, 4.000000, 15.000000, 12.000000, NULL, 60.000000, NULL, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0');
INSERT INTO `wms_order_item` VALUES (332, 275, 619, 36, '件', '橙色,M', 100.000000, 100.000000, 25.000000, NULL, NULL, 2500.000000, NULL, NULL, NULL, 6.000000, 150.000000, 2650.000000, NULL, NULL, NULL, NULL, NULL, NULL, '0');
INSERT INTO `wms_order_item` VALUES (333, 275, 619, 37, '件', '橙色,L', 100.000000, 100.000000, 25.000000, NULL, NULL, 2500.000000, NULL, NULL, NULL, 6.000000, 150.000000, 2650.000000, NULL, NULL, NULL, NULL, NULL, NULL, '0');

-- ----------------------------
-- Table structure for wms_order_type
-- ----------------------------
DROP TABLE IF EXISTS `wms_order_type`;
CREATE TABLE `wms_order_type`  (
  `id` bigint NOT NULL COMMENT 'id',
  `inout` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '出入库',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名称',
  `enable` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '启用',
  `tenant_id` bigint NULL DEFAULT NULL COMMENT '租户',
  `update_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '出入库类别表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wms_order_type
-- ----------------------------

-- ----------------------------
-- Table structure for wms_supplier
-- ----------------------------
DROP TABLE IF EXISTS `wms_supplier`;
CREATE TABLE `wms_supplier`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `contacts` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系人',
  `mobile` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电子邮箱',
  `enabled` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '启用',
  `advance_in` decimal(16, 2) NULL DEFAULT 0.00 COMMENT '预收款',
  `begin_need_get` decimal(16, 2) NULL DEFAULT NULL COMMENT '期初应收',
  `begin_need_pay` decimal(16, 2) NULL DEFAULT NULL COMMENT '期初应付',
  `all_need_get` decimal(16, 2) NULL DEFAULT NULL COMMENT '累计应收',
  `all_need_pay` decimal(16, 2) NULL DEFAULT NULL COMMENT '累计应付',
  `fax` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '传真',
  `address` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地址',
  `tax_num` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '纳税人识别号',
  `bank_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '开户行',
  `account_number` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '账号',
  `tax_rate` decimal(16, 2) NULL DEFAULT NULL COMMENT '税率',
  `sort` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '排序',
  `tenant_id` int NULL DEFAULT NULL COMMENT '租户id',
  `del_flag` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注',
  `update_by` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '供应商/客户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wms_supplier
-- ----------------------------
INSERT INTO `wms_supplier` VALUES (57, '1', '供应商1', '小军', '12345678', '', 'Y', 0.00, 0.00, 0.00, 0.00, 4.00, '', '地址1', '', '', '', 12.00, NULL, 63, '0', '', NULL, '2023-01-27 17:10:41');
INSERT INTO `wms_supplier` VALUES (58, '1', '客户1', '小李', '12345678', '', '1', 0.00, 0.00, 0.00, -100.00, NULL, '', '', '', '', '', 12.00, NULL, 63, '0', '', NULL, '2023-01-27 17:10:53');
INSERT INTO `wms_supplier` VALUES (59, '客户', '客户2', '小陈', '', '', '1', 0.00, 0.00, 0.00, 0.00, NULL, '', '', '', '', '', NULL, NULL, 63, '0', '', NULL, NULL);
INSERT INTO `wms_supplier` VALUES (60, '会员', '12312666', '小曹', '', '', '1', 970.00, 0.00, 0.00, NULL, NULL, '', '', '', '', '', NULL, NULL, 63, '0', '', NULL, NULL);
INSERT INTO `wms_supplier` VALUES (68, '供应商', '供应商3', '晓丽', '12345678', '', '1', 0.00, 0.00, 0.00, 0.00, -35.00, '', 'aaaa', '1341324', '', '', 13.00, NULL, 63, '0', 'fasdfadf', NULL, NULL);
INSERT INTO `wms_supplier` VALUES (71, '客户', '客户3', '小周', '', '', '1', 0.00, 0.00, 0.00, 0.00, NULL, '', '', '', '', '', NULL, NULL, 63, '0', '', NULL, NULL);
INSERT INTO `wms_supplier` VALUES (74, '供应商', '供应商5', '小季', '77779999', '', '1', 0.00, 0.00, 5.00, 0.00, 5.00, '', '', '', '', '', 3.00, NULL, 63, '0', '', NULL, NULL);
INSERT INTO `wms_supplier` VALUES (90, '会员', '123124123', '啊啊', '130', '130@qq.com', '1', 0.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8', NULL, '0', 'aa', NULL, NULL);
INSERT INTO `wms_supplier` VALUES (91, '会员', '测试', '测试', NULL, NULL, '1', 0.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for wms_warehouse
-- ----------------------------
DROP TABLE IF EXISTS `wms_warehouse`;
CREATE TABLE `wms_warehouse`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '仓库名称',
  `address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '仓库地址',
  `cost` decimal(16, 2) NULL DEFAULT NULL COMMENT '仓储费',
  `truckage` decimal(16, 2) NULL DEFAULT NULL COMMENT '搬运费',
  `type` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `sort` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '排序',
  `user_id` bigint NULL DEFAULT NULL COMMENT '负责人',
  `enabled` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '启用',
  `del_Flag` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标记',
  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否默认',
  `tenant_id` bigint NULL DEFAULT NULL COMMENT '归属',
  `remark` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '仓库表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wms_warehouse
-- ----------------------------
INSERT INTO `wms_warehouse` VALUES (14, '仓库1', 'dizhi', 12.00, 12.00, '1', '1', 131, '0', '0', '1', 63, '描述', NULL, '2023-01-27 19:02:07');
INSERT INTO `wms_warehouse` VALUES (15, '仓库2', '地址100', 555.00, 666.00, '0', '2', 131, '1', '0', '0', 63, 'dfdf', NULL, NULL);
INSERT INTO `wms_warehouse` VALUES (17, '仓库3', '123123', 123.00, 123.00, '0', '3', 131, '1', '0', '0', 63, '123', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
