#### 朝夕同行

#### 介绍

    基于 若依 前后分离VUE版本 二次开发
    先去把若依系统简单环境部署了解
    朝夕同行

#### 软件架构
    JDK >= 1.8 (推荐1.8版本)
    Mysql >= 8.28（推荐大于8）
    Redis >= 5.0
    Maven >= 3.6
    Node = 14.21（就用14的版本最好）

#### 后端部署
    1.通过cmd 打开自己的工作目录
    2.git执行下载包
    git clone https://gitee.com/andy0551/zxtx.git 
    
    3.在数据库中新建数据库
    4.导入/sql目录下的.sql文件
    5用idea打开项目
    6.修改自己的maven中的仓库配置
    7.maven下载所有依赖包
    8.修改application.yml 中的数据库用户名和密码（对应新建的库名）
    9.启动服务
#### 前端部署
    1.安装nodejs 最好是14.21.2版本
    https://nodejs.org/dist/latest-v14.x/node-v14.21.2-win-x64.zip
    2.进入项目目录
    cd mn-ui-pc
    3.安装依赖
        npm install
    4.建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
        npm install --registry=https://registry.npmmirror.com
    5.启动服务
    npm run dev
     ## 发布
     # 构建测试环境
     npm run build:stage
    
    # 构建生产环境
    npm run build:prod
    ```

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
