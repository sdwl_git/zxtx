package com.mn.framework.web.service;

import com.mn.common.core.domain.model.LoginUser;
import com.mn.common.enums.UserStatus;
import com.mn.common.exception.ServiceException;
import com.mn.common.utils.DateUtils;
import com.mn.common.utils.StringUtils;
import com.mn.system.domain.SysTenant;
import com.mn.system.domain.SysUser;
import com.mn.system.service.ISysTenantService;
import com.mn.system.service.ISysUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * 用户验证处理
 *
 * @author mn
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private static final Logger log = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    @Autowired
    private ISysUserService userService;

    @Autowired
    private SysPermissionService permissionService;

    @Autowired
    private ISysTenantService tenantService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUser user = userService.selectUserByUserName(username);
        if (StringUtils.isNull(user)) {
            log.info("登录用户：{} 不存在.", username);
            throw new ServiceException("登录用户：" + username + " 不存在");
        } else if (UserStatus.DELETED.getCode().equals(user.getDelFlag())) {
            log.info("登录用户：{} 已被删除.", username);
            throw new ServiceException("对不起，您的账号：" + username + " 已被删除");
        } else if (UserStatus.DISABLE.getCode().equals(user.getStatus())) {
            log.info("登录用户：{} 已被停用.", username);
            throw new ServiceException("对不起，您的账号：" + username + " 已停用");
        }

        //获取租户  放在这里好像不合适，应该是在验证密码的时候就做租户管控，以后在改吧。
        SysTenant tenant = tenantService.selectSysTenantByTenantId(user.getTenantId());
        //还有多少天过期
        int tenantExpireDay = DateUtils.differentDaysByMillisecond(tenant.getExpireTime(),DateUtils.getNowDate());
        if (tenantExpireDay < 0){
            log.info("用户机构：{} 已到期.", tenant.getName());
            throw new ServiceException("登录用户机构：" + tenant.getName() + " 已到期");
        }
        //设置tenantName
        user.setParams("tenantName",tenant.getName());
        return createLoginUser(user);
    }

    public UserDetails createLoginUser(SysUser user) {
        return new LoginUser(user, permissionService.getMenuPermission(user));
    }
}
