package com.mn.system.service.impl;

import com.mn.common.utils.DateUtils;
import com.mn.system.domain.SysTenant;
import com.mn.system.mapper.SysTenantMapper;
import com.mn.system.service.ISysTenantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 租户Service业务层处理
 *
 * @author mn
 * @date 2023-01-21
 */
@Service
public class SysTenantServiceImpl implements ISysTenantService {
    @Autowired
    private SysTenantMapper sysTenantMapper;

    /**
     * 查询租户
     *
     * @param tenantId 租户主键
     * @return 租户
     */
    @Override
    public SysTenant selectSysTenantByTenantId(Long tenantId) {
        return sysTenantMapper.selectSysTenantByTenantId(tenantId);
    }

    /**
     * 查询租户列表
     *
     * @param sysTenant 租户
     * @return 租户
     */
    @Override
    public List<SysTenant> selectSysTenantList(SysTenant sysTenant) {
        return sysTenantMapper.selectSysTenantList(sysTenant);
    }

    /**
     * 新增租户
     *
     * @param sysTenant 租户
     * @return 结果
     */
    @Override
    public int insertSysTenant(SysTenant sysTenant) {
        sysTenant.setCreateTime(DateUtils.getNowDate());
        return sysTenantMapper.insertSysTenant(sysTenant);
    }

    /**
     * 修改租户
     *
     * @param sysTenant 租户
     * @return 结果
     */
    @Override
    public int updateSysTenant(SysTenant sysTenant) {
        return sysTenantMapper.updateSysTenant(sysTenant);
    }

    /**
     * 批量删除租户
     *
     * @param tenantIds 需要删除的租户主键
     * @return 结果
     */
    @Override
    public int deleteSysTenantByTenantIds(Long[] tenantIds) {
        return sysTenantMapper.deleteSysTenantByTenantIds(tenantIds);
    }

    /**
     * 删除租户信息
     *
     * @param tenantId 租户主键
     * @return 结果
     */
    @Override
    public int deleteSysTenantByTenantId(Long tenantId) {
        return sysTenantMapper.deleteSysTenantByTenantId(tenantId);
    }
}
