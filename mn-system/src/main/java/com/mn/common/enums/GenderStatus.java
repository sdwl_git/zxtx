package com.mn.common.enums;

/**
 * 性别
 *
 * @author mn
 */
public enum GenderStatus {
    /**
     * 男
     */
    MAN,

    /**
     * 女
     */
    WOMAN,
}
