package com.mn.common.enums;

/**
 * 数据源
 *
 * @author mn
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
