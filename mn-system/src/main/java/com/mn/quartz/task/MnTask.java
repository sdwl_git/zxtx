package com.mn.quartz.task;

import com.mn.common.utils.StringUtils;
import com.mn.quartz.service.ISysJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 定时任务调度测试
 *
 * @author mn
 */
@Component("mnTask")
public class MnTask {

    @Autowired
    private ISysJobService jobService;

    public void mnMultipleParams(String s, Boolean b, Long l, Double d, Integer i) {
        System.out.println(StringUtils.format("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i));
    }
    public void mnParams(String params) {
        System.out.println("执行有参方法：" + params);
    }

    public void mnNoParams() {
        System.out.println("执行无参方法");
    }



}
