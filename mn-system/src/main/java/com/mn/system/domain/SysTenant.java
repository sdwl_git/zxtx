package com.mn.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.mn.common.annotation.Excel;
import com.mn.common.core.domain.BaseEntity;

/**
 * 租户对象 sys_tenant
 *
 * @author mn
 * @date 2023-01-21
 */
public class SysTenant extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long tenantId;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 用户数量限制 */
    @Excel(name = "用户数量限制")
    private Long userNumLimit;

    /** 启用 0-禁用  1-启用 */
    @Excel(name = "启用 0-禁用  1-启用")
    private Integer enabled;

    /** 到期时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "到期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date expireTime;

    public void setTenantId(Long tenantId)
    {
        this.tenantId = tenantId;
    }

    public Long getTenantId()
    {
        return tenantId;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setUserNumLimit(Long userNumLimit)
    {
        this.userNumLimit = userNumLimit;
    }

    public Long getUserNumLimit()
    {
        return userNumLimit;
    }
    public void setEnabled(Integer enabled)
    {
        this.enabled = enabled;
    }

    public Integer getEnabled()
    {
        return enabled;
    }
    public void setExpireTime(Date expireTime)
    {
        this.expireTime = expireTime;
    }

    public Date getExpireTime()
    {
        return expireTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("tenantId", getTenantId())
            .append("name", getName())
            .append("userNumLimit", getUserNumLimit())
            .append("enabled", getEnabled())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("expireTime", getExpireTime())
            .append("remark", getRemark())
            .toString();
    }
}
