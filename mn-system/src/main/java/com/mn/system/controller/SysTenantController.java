package com.mn.system.controller;

import com.mn.common.annotation.Log;
import com.mn.common.core.controller.BaseController;
import com.mn.common.core.domain.AjaxResult;
import com.mn.common.core.domain.model.LoginUser;
import com.mn.common.core.page.TableDataInfo;
import com.mn.common.enums.BusinessType;
import com.mn.common.utils.SecurityUtils;
import com.mn.common.utils.poi.ExcelUtil;
import com.mn.framework.web.service.TokenService;
import com.mn.system.domain.SysTenant;
import com.mn.system.domain.SysUser;
import com.mn.system.service.ISysTenantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 租户Controller
 *
 * @author mn
 * @date 2023-01-21
 */
@RestController
@RequestMapping("/system/tenant")
public class SysTenantController extends BaseController {
    @Autowired
    private ISysTenantService sysTenantService;

    @Autowired
    private TokenService tokenService;

    /**
     * 查询租户列表
     */
    @PreAuthorize("@ss.hasPermi('system:tenant:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysTenant sysTenant) {
        startPage();
        List<SysTenant> list = sysTenantService.selectSysTenantList(sysTenant);
        return getDataTable(list);
    }

    /**
     * 导出租户列表
     */
    @PreAuthorize("@ss.hasPermi('system:tenant:export')")
    @Log(title = "租户", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysTenant sysTenant) {
        List<SysTenant> list = sysTenantService.selectSysTenantList(sysTenant);
        ExcelUtil<SysTenant> util = new ExcelUtil<SysTenant>(SysTenant.class);
        util.exportExcel(response, list, "租户数据");
    }

    /**
     * 获取租户详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tenant:query')")
    @GetMapping(value = "/{tenantId}")
    public AjaxResult getInfo(@PathVariable("tenantId") Long tenantId) {
        return AjaxResult.success(sysTenantService.selectSysTenantByTenantId(tenantId));
    }

    /**
     * 切换租户的id
     */
    @PreAuthorize("@ss.hasPermi('system:tenant:query')")
    @GetMapping(value = "/changeTenant/{tenantId}")
    public AjaxResult changeTenantId(@PathVariable("tenantId") Long tenantId) {

        Long userId = SecurityUtils.getUserId();
        if (!SecurityUtils.isAdmin(userId)) {
            AjaxResult.error("只有超级管理员可以切换租户");
        }

        LoginUser loginUser = SecurityUtils.getLoginUser();
        loginUser.setTenantId(tenantId);
        tokenService.setLoginUser(loginUser);

        return AjaxResult.success("返回成功");
    }

    /**
     * 新增租户
     */
    @PreAuthorize("@ss.hasPermi('system:tenant:add')")
    @Log(title = "租户", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysTenant sysTenant) {
        return toAjax(sysTenantService.insertSysTenant(sysTenant));
    }

    /**
     * 修改租户
     */
    @PreAuthorize("@ss.hasPermi('system:tenant:edit')")
    @Log(title = "租户", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysTenant sysTenant) {
        return toAjax(sysTenantService.updateSysTenant(sysTenant));
    }

    /**
     * 删除租户
     */
    @PreAuthorize("@ss.hasPermi('system:tenant:remove')")
    @Log(title = "租户", businessType = BusinessType.DELETE)
    @DeleteMapping("/{tenantIds}")
    public AjaxResult remove(@PathVariable Long[] tenantIds) {
        return toAjax(sysTenantService.deleteSysTenantByTenantIds(tenantIds));
    }
}
