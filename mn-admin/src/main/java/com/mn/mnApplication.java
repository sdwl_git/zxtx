package com.mn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动程序
 *
 * @author mn
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class mnApplication {
    public static void main(String[] args) {
        // System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(mnApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  项目启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                "-----------------------\n" +
                "           _        \n" +
                "          | |       \n" +
                " ______  _| |___  __\n" +
                "|_  /\\ \\/ / __\\ \\/ /\n" +
                " / /  >  <| |_ >  < \n" +
                "/___|/_/\\_\\\\__/_/\\_\\\n");
    }
}
