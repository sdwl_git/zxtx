package com.mn.common.enums;

/**
 * 操作状态
 *
 * @author mn
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
